<?php

namespace frontend\controllers;

use common\models\TaxInvoiceEvent;
use common\models\TaxInvoiceEventSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * TaxInvoiceEventController implements the CRUD actions for TaxInvoiceEvent model.
 */
class TaxInvoiceEventController extends Controller
{
	/**
	 * Lists all TaxInvoiceEvent models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new TaxInvoiceEventSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single TaxInvoiceEvent model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($ID	int	NO)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($ID	int	NO),
		]);
	}

	/**
	 * Creates a new TaxInvoiceEvent model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new TaxInvoiceEvent;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing TaxInvoiceEvent model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($ID	int	NO)
	{
		$model = $this->findModel($ID	int	NO);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing TaxInvoiceEvent model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($ID	int	NO)
	{
		$this->findModel($ID	int	NO)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the TaxInvoiceEvent model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return TaxInvoiceEvent the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($ID	int	NO)
	{
		if (($model = TaxInvoiceEvent::findOne($ID	int	NO)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
