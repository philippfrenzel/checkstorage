<?php

namespace frontend\controllers;

use common\models\TaxInvoice;
use common\models\TaxInvoiceSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * TaxInvoiceController implements the CRUD actions for TaxInvoice model.
 */
class TaxInvoiceController extends Controller
{
	/**
	 * Lists all TaxInvoice models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new TaxInvoiceSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single TaxInvoice model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($InvoiceID)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($InvoiceID),
		]);
	}

	/**
	 * Creates a new TaxInvoice model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new TaxInvoice;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing TaxInvoice model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($InvoiceID)
	{
		$model = $this->findModel($InvoiceID);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing TaxInvoice model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($InvoiceID)
	{
		$this->findModel($InvoiceID)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the TaxInvoice model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return TaxInvoice the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($InvoiceID)
	{
		if (($model = TaxInvoice::findOne($InvoiceID)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
