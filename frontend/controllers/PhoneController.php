<?php

namespace frontend\controllers;

use common\models\Phone;
use frontend\models\PhoneSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * PhoneController implements the CRUD actions for Phone model.
 */
class PhoneController extends Controller
{
	/**
	 * Lists all Phone models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new PhoneSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single Phone model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($PhoneID)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($PhoneID),
		]);
	}

	/**
	 * Creates a new Phone model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Phone;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing Phone model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($PhoneID)
	{
		$model = $this->findModel($PhoneID);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Phone model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($PhoneID)
	{
		$this->findModel($PhoneID)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the Phone model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Phone the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($PhoneID)
	{
		if (($model = Phone::findOne($PhoneID)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
