<?php

namespace frontend\controllers;

use common\models\UnitType;
use frontend\models\UnitTypeSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * UnitTypeController implements the CRUD actions for UnitType model.
 */
class UnitTypeController extends Controller
{
	/**
	 * Lists all UnitType models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new UnitTypeSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single UnitType model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($UnitTypeID)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($UnitTypeID),
		]);
	}

	/**
	 * Creates a new UnitType model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new UnitType;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing UnitType model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($UnitTypeID)
	{
		$model = $this->findModel($UnitTypeID);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing UnitType model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($UnitTypeID)
	{
		$this->findModel($UnitTypeID)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the UnitType model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return UnitType the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($UnitTypeID)
	{
		if (($model = UnitType::findOne($UnitTypeID)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
