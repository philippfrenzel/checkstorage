<?php

namespace frontend\controllers;

use common\models\Unit;
use frontend\models\UnitSearch;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * UnitController implements the CRUD actions for Unit model.
 */
class UnitController extends AppController
{
	/**
	 * Lists all Unit models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new UnitSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single Unit model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($UnitID)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($UnitID),
		]);
	}

	/**
	 * Creates a new Unit model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Unit;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing Unit model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($UnitID)
	{
		$model = $this->findModel($UnitID);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Unit model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($UnitID)
	{
		$this->findModel($UnitID)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the Unit model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Unit the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($UnitID)
	{
		if (($model = Unit::findOne($UnitID)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
