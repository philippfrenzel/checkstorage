<?php

namespace frontend\controllers;

use common\models\Party;
use frontend\models\PartySearch;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * PartyController implements the CRUD actions for Party model.
 */
class PartyController extends AppController
{
	/**
	 * Lists all Party models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new PartySearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single Party model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($PartyID)
	{
        Url::remember();
        //change the layout
        $this->layout = '/main';
        
        $model = $this->findModel($PartyID);	
		\Yii::$app->session->setFlash('success', Html::encode($model->Alert));
        
        return $this->render('partyview', [
			'model' => $model,
		]);
	}

	/**
	 * Creates a new Party model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Party;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing Party model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($PartyID)
	{
		$model = $this->findModel($PartyID);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Party model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($PartyID)
	{
		$this->findModel($PartyID)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the Party model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Party the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($PartyID)
	{
		if (($model = Party::findOne($PartyID)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
