<?php

namespace frontend\controllers;

use common\models\Contract;
use frontend\models\ContractSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * ContractController implements the CRUD actions for Contract model.
 */
class ContractController extends Controller
{
	/**
	 * Lists all Contract models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ContractSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single Contract model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($ContractID)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($ContractID),
		]);
	}

	/**
	 * Creates a new Contract model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Contract;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing Contract model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($ContractID)
	{
		$model = $this->findModel($ContractID);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Contract model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($ContractID)
	{
		$this->findModel($ContractID)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the Contract model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Contract the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($ContractID)
	{
		if (($model = Contract::findOne($ContractID)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
