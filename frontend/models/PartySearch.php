<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Party;

/**
 * PartySearch represents the model behind the search form about Party.
 */
class PartySearch extends Model
{
	public $PartyID;
	public $DLData;
	public $DLNumber;
	public $ID1;
	public $ID2;
	public $UnitSortOrder;
	public $GrpActRef;
	public $DLRegionAbbr;
	public $Identification2;
	public $Identification1;
	public $PartyName;
	public $Soundex;
	public $Alert;
	public $PartyParam;
	public $UnitList;
	public $Note;
	public $Created;
	public $LockTime;
	public $LockUser;
	public $PTD;
	public $created_at;
	public $updated_at;
	public $deleted_at;
	public $PartyTypeID;
	public $OccupationID;

	//added relations
	public $ContractEffective;

	public function rules()
	{
		return [
			[['PartyID', 'UnitSortOrder', 'GrpActRef', 'LockUser', 'created_at', 'updated_at', 'deleted_at', 'PartyTypeID', 'OccupationID'], 'integer'],
			[['DLData', 'DLNumber', 'ID1', 'ID2', 'DLRegionAbbr', 'Identification2', 'Identification1', 'PartyName', 'Soundex', 'Alert', 'PartyParam', 'UnitList', 'Note', 'Created', 'LockTime', 'PTD'], 'safe'],
			[['ContractEffective'],'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'PartyID' => 'Party ID',
			'DLData' => 'Dldata',
			'DLNumber' => 'Dlnumber',
			'ID1' => 'Id1',
			'ID2' => 'Id2',
			'UnitSortOrder' => 'Unit Sort Order',
			'GrpActRef' => 'Grp Act Ref',
			'DLRegionAbbr' => 'Dlregion Abbr',
			'Identification2' => 'Identification2',
			'Identification1' => 'Identification1',
			'PartyName' => 'Party Name',
			'Soundex' => 'Soundex',
			'Alert' => 'Alert',
			'PartyParam' => 'Party Param',
			'UnitList' => 'Unit List',
			'Note' => 'Note',
			'Created' => 'Created',
			'LockTime' => 'Lock Time',
			'LockUser' => 'Lock User',
			'PTD' => 'Ptd',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
			'PartyTypeID' => 'Party Type ID',
			'OccupationID' => 'Occupation ID',
		];
	}

	/**
	 * [search description]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public function search($params)
	{
		$query = Party::find()->with(['contract','addresses']);

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'PartyID' => $this->PartyID,
            'UnitSortOrder' => $this->UnitSortOrder,
            'GrpActRef' => $this->GrpActRef,
            'Created' => $this->Created,
            'LockTime' => $this->LockTime,
            'LockUser' => $this->LockUser,
            'PTD' => $this->PTD,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'PartyTypeID' => $this->PartyTypeID,
            'OccupationID' => $this->OccupationID,
        ]);

		$query->andFilterWhere(['like', 'DLData', $this->DLData])
            ->andFilterWhere(['like', 'DLNumber', $this->DLNumber])
            ->andFilterWhere(['like', 'ID1', $this->ID1])
            ->andFilterWhere(['like', 'ID2', $this->ID2])
            ->andFilterWhere(['like', 'DLRegionAbbr', $this->DLRegionAbbr])
            ->andFilterWhere(['like', 'Identification2', $this->Identification2])
            ->andFilterWhere(['like', 'Identification1', $this->Identification1])
            ->andFilterWhere(['like', 'PartyName', $this->PartyName])
            ->andFilterWhere(['like', 'Soundex', $this->Soundex])
            ->andFilterWhere(['like', 'Alert', $this->Alert])
            ->andFilterWhere(['like', 'PartyParam', $this->PartyParam])
            ->andFilterWhere(['like', 'UnitList', $this->UnitList])
            ->andFilterWhere(['like', 'Note', $this->Note]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
