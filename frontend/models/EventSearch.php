<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Event;

/**
 * EventSearch represents the model behind the search form about Event.
 */
class EventSearch extends Model
{
	public $EventID;
	public $ActionID;
	public $Param;
	public $UserPartyID;
	public $Created;
	public $JournalDate;
	public $EAmount;
	public $ETax;
	public $ESnapshot;
	public $CID;
	public $datFrom;
	public $datTo;
	public $Description;
	public $created_at;
	public $updated_at;
	public $deleted_at;

	//related fields
	public $PartyID;
	public $ActionType;

	public function rules()
	{
		return [
			[['EventID', 'ActionID', 'UserPartyID', 'CID', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
			[['Param', 'Created', 'JournalDate', 'ESnapshot', 'datFrom', 'datTo', 'Description'], 'safe'],
			[['EAmount', 'ETax'], 'number'],
			[['PartyID','ActionType'],'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'EventID' => 'Event ID',
			'ActionID' => 'Action ID',
			'Param' => 'Param',
			'UserPartyID' => 'User Party ID',
			'Created' => 'Created',
			'JournalDate' => 'Journal Date',
			'EAmount' => 'Eamount',
			'ETax' => 'Etax',
			'ESnapshot' => 'Esnapshot',
			'CID' => 'Cid',
			'datFrom' => 'Dat From',
			'datTo' => 'Dat To',
			'Description' => 'Description',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
		];
	}

	public function search($params)
	{
		$query = Event::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'EventID' => $this->EventID,
            'ActionID' => $this->ActionID,
            'UserPartyID' => $this->UserPartyID,
            'Created' => $this->Created,
            'JournalDate' => $this->JournalDate,
            'EAmount' => $this->EAmount,
            'ETax' => $this->ETax,
            'CID' => $this->CID,
            'datFrom' => $this->datFrom,
            'datTo' => $this->datTo,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

		$query->andFilterWhere(['like', 'Param', $this->Param])
            ->andFilterWhere(['like', 'ESnapshot', $this->ESnapshot])
            ->andFilterWhere(['like', 'Description', $this->Description]);

		return $dataProvider;
	}

	public function searchRelated($params)
	{
		$query = Event::find()->joinWith(['action','action.actionType']);
		
		$query->andFilterWhere(['=', '{{%Action}}.PartyID', $this->PartyID]);

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'EventID' => $this->EventID,
            'ActionID' => $this->ActionID,
            'UserPartyID' => $this->UserPartyID,
            'Created' => $this->Created,
            'JournalDate' => $this->JournalDate,
            'EAmount' => $this->EAmount,
            'ETax' => $this->ETax,
            'CID' => $this->CID,
            'datFrom' => $this->datFrom,
            'datTo' => $this->datTo,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

		$query->andFilterWhere(['like', 'Param', $this->Param])
            ->andFilterWhere(['like', 'ESnapshot', $this->ESnapshot])
            ->andFilterWhere(['like', 'Description', $this->Description])
            ->andFilterWhere(['like', '{{%ActionType}}.ActionType', $this->ActionType]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
