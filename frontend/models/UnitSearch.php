<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Unit;

/**
 * UnitSearch represents the model behind the search form about Unit.
 */
class UnitSearch extends Model
{
	public $UnitID;
	public $Unit;
	public $StatusNow;
	public $SortOrder;
	public $UnitNumber;
	public $CatalogSet;
	public $CatalogDesc;
	public $created_at;
	public $updated_at;
	public $deleted_at;
	public $SiteID;
	public $LocationID;
	public $OffSiteAddressID;
	public $UnitTypeID;

	public function rules()
	{
		return [
			[['UnitID', 'SortOrder', 'created_at', 'updated_at', 'deleted_at', 'SiteID', 'LocationID', 'OffSiteAddressID', 'UnitTypeID'], 'integer'],
			[['Unit', 'StatusNow', 'UnitNumber', 'CatalogSet', 'CatalogDesc'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'UnitID' => 'Unit ID',
			'Unit' => 'Unit',
			'StatusNow' => 'Status Now',
			'SortOrder' => 'Sort Order',
			'UnitNumber' => 'Unit Number',
			'CatalogSet' => 'Catalog Set',
			'CatalogDesc' => 'Catalog Desc',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
			'SiteID' => 'Site ID',
			'LocationID' => 'Location ID',
			'OffSiteAddressID' => 'Off Site Address ID',
			'UnitTypeID' => 'Unit Type ID',
		];
	}

	public function search($params)
	{
		$query = Unit::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'UnitID' => $this->UnitID,
            'SortOrder' => $this->SortOrder,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'SiteID' => $this->SiteID,
            'LocationID' => $this->LocationID,
            'OffSiteAddressID' => $this->OffSiteAddressID,
            'UnitTypeID' => $this->UnitTypeID,
        ]);

		$query->andFilterWhere(['like', 'Unit', $this->Unit])
            ->andFilterWhere(['like', 'StatusNow', $this->StatusNow])
            ->andFilterWhere(['like', 'UnitNumber', $this->UnitNumber])
            ->andFilterWhere(['like', 'CatalogSet', $this->CatalogSet])
            ->andFilterWhere(['like', 'CatalogDesc', $this->CatalogDesc]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
