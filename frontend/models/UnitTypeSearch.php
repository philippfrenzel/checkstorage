<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UnitType;

/**
 * UnitTypeSearch represents the model behind the search form about UnitType.
 */
class UnitTypeSearch extends Model
{
	public $UnitTypeID;
	public $Icon;
	public $QueryName;
	public $TableName;
	public $UnitType;
	public $Notes;
	public $SortOrder;
	public $created_at;
	public $updated_at;
	public $deleted_at;

	public function rules()
	{
		return [
			[['UnitTypeID', 'Icon', 'SortOrder', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
			[['QueryName', 'TableName', 'UnitType', 'Notes'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'UnitTypeID' => 'Unit Type ID',
			'Icon' => 'Icon',
			'QueryName' => 'Query Name',
			'TableName' => 'Table Name',
			'UnitType' => 'Unit Type',
			'Notes' => 'Notes',
			'SortOrder' => 'Sort Order',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
		];
	}

	public function search($params)
	{
		$query = UnitType::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'UnitTypeID' => $this->UnitTypeID,
            'Icon' => $this->Icon,
            'SortOrder' => $this->SortOrder,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

		$query->andFilterWhere(['like', 'QueryName', $this->QueryName])
            ->andFilterWhere(['like', 'TableName', $this->TableName])
            ->andFilterWhere(['like', 'UnitType', $this->UnitType])
            ->andFilterWhere(['like', 'Notes', $this->Notes]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
