<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Address;

/**
 * AddressSearch represents the model behind the search form about Address.
 */
class AddressSearch extends Model
{
	public $AddressID;
	public $Code;
	public $City;
	public $Street;
	public $IsPrimary;
	public $created_at;
	public $updated_at;
	public $deleted_at;
	public $AddressTypeID;
	public $PartyID;
	public $RegionID;

	public function rules()
	{
		return [
			[['AddressID', 'IsPrimary', 'created_at', 'updated_at', 'deleted_at', 'AddressTypeID', 'PartyID', 'RegionID'], 'integer'],
			[['Code', 'City', 'Street'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'AddressID' => 'Address ID',
			'Code' => 'Code',
			'City' => 'City',
			'Street' => 'Street',
			'IsPrimary' => 'Is Primary',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
			'AddressTypeID' => 'Address Type ID',
			'PartyID' => 'Party ID',
			'RegionID' => 'Region ID',
		];
	}

	public function search($params)
	{
		$query = Address::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'AddressID' => $this->AddressID,
            'IsPrimary' => $this->IsPrimary,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'AddressTypeID' => $this->AddressTypeID,
            'PartyID' => $this->PartyID,
            'RegionID' => $this->RegionID,
        ]);

		$query->andFilterWhere(['like', 'Code', $this->Code])
            ->andFilterWhere(['like', 'City', $this->City])
            ->andFilterWhere(['like', 'Street', $this->Street]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
