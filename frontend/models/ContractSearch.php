<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Contract;

/**
 * ContractSearch represents the model behind the search form about Contract.
 */
class ContractSearch extends Model
{
	public $ContractID;
	public $CycleLength;
	public $UnitSortOrder;
	public $ContractNumber;
	public $Param;
	public $PTSizeNeeded;
	public $UnitList;
	public $PTReason;
	public $AmtDue;
	public $Balance;
	public $Deposit;
	public $Effective;
	public $IsInProcessor;
	public $LastRebuildPrepay;
	public $MarketDistance;
	public $MinimumDuration;
	public $OpenCredits;
	public $Pau;
	public $PTD;
	public $PTDateMovein;
	public $PTDateNeeded;
	public $Rent;
	public $UnitIns;
	public $UnitOther;
	public $UnitRent;
	public $UnitTax;
	public $VacateDate;
	public $created_at;
	public $updated_at;
	public $deleted_at;
	public $UserID;
	public $PartyID;
	public $MarketReasonID;
	public $MarketSourceID;
	public $ContractTypeID;

	public function rules()
	{
		return [
			[['ContractID', 'UnitSortOrder', 'IsInProcessor', 'created_at', 'updated_at', 'deleted_at', 'UserID', 'PartyID', 'MarketReasonID', 'MarketSourceID', 'ContractTypeID'], 'integer'],
			[['CycleLength', 'ContractNumber', 'Param', 'PTSizeNeeded', 'UnitList', 'PTReason', 'Effective', 'LastRebuildPrepay', 'MinimumDuration', 'Pau', 'PTD', 'PTDateMovein', 'PTDateNeeded', 'VacateDate'], 'safe'],
			[['AmtDue', 'Balance', 'Deposit', 'MarketDistance', 'OpenCredits', 'Rent', 'UnitIns', 'UnitOther', 'UnitRent', 'UnitTax'], 'number'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'ContractID' => 'Contract ID',
			'CycleLength' => 'Cycle Length',
			'UnitSortOrder' => 'Unit Sort Order',
			'ContractNumber' => 'Contract Number',
			'Param' => 'Param',
			'PTSizeNeeded' => 'Ptsize Needed',
			'UnitList' => 'Unit List',
			'PTReason' => 'Ptreason',
			'AmtDue' => 'Amt Due',
			'Balance' => 'Balance',
			'Deposit' => 'Deposit',
			'Effective' => 'Effective',
			'IsInProcessor' => 'Is In Processor',
			'LastRebuildPrepay' => 'Last Rebuild Prepay',
			'MarketDistance' => 'Market Distance',
			'MinimumDuration' => 'Minimum Duration',
			'OpenCredits' => 'Open Credits',
			'Pau' => 'Pau',
			'PTD' => 'Ptd',
			'PTDateMovein' => 'Ptdate Movein',
			'PTDateNeeded' => 'Ptdate Needed',
			'Rent' => 'Rent',
			'UnitIns' => 'Unit Ins',
			'UnitOther' => 'Unit Other',
			'UnitRent' => 'Unit Rent',
			'UnitTax' => 'Unit Tax',
			'VacateDate' => 'Vacate Date',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
			'UserID' => 'User ID',
			'PartyID' => 'Party ID',
			'MarketReasonID' => 'Market Reason ID',
			'MarketSourceID' => 'Market Source ID',
			'ContractTypeID' => 'Contract Type ID',
		];
	}

	public function search($params)
	{
		$query = Contract::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'ContractID' => $this->ContractID,
            'UnitSortOrder' => $this->UnitSortOrder,
            'AmtDue' => $this->AmtDue,
            'Balance' => $this->Balance,
            'Deposit' => $this->Deposit,
            'Effective' => $this->Effective,
            'IsInProcessor' => $this->IsInProcessor,
            'LastRebuildPrepay' => $this->LastRebuildPrepay,
            'MarketDistance' => $this->MarketDistance,
            'MinimumDuration' => $this->MinimumDuration,
            'OpenCredits' => $this->OpenCredits,
            'Pau' => $this->Pau,
            'PTD' => $this->PTD,
            'PTDateMovein' => $this->PTDateMovein,
            'PTDateNeeded' => $this->PTDateNeeded,
            'Rent' => $this->Rent,
            'UnitIns' => $this->UnitIns,
            'UnitOther' => $this->UnitOther,
            'UnitRent' => $this->UnitRent,
            'UnitTax' => $this->UnitTax,
            'VacateDate' => $this->VacateDate,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'UserID' => $this->UserID,
            'PartyID' => $this->PartyID,
            'MarketReasonID' => $this->MarketReasonID,
            'MarketSourceID' => $this->MarketSourceID,
            'ContractTypeID' => $this->ContractTypeID,
        ]);

		$query->andFilterWhere(['like', 'CycleLength', $this->CycleLength])
            ->andFilterWhere(['like', 'ContractNumber', $this->ContractNumber])
            ->andFilterWhere(['like', 'Param', $this->Param])
            ->andFilterWhere(['like', 'PTSizeNeeded', $this->PTSizeNeeded])
            ->andFilterWhere(['like', 'UnitList', $this->UnitList])
            ->andFilterWhere(['like', 'PTReason', $this->PTReason]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
