<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UnitSelfStor;

/**
 * UnitSelfStorSearch represents the model behind the search form about UnitSelfStor.
 */
class UnitSelfStorSearch extends Model
{
	public $UnitID;
	public $Overlock;
	public $WalkOrder;
	public $UnitOfMeasure;
	public $Note;
	public $UnitRating;
	public $PopupNote;
	public $RateDaily;
	public $RateMonthly;
	public $RateWeekly;
	public $UnitHeight;
	public $UnitLength;
	public $UnitWidth;
	public $created_at;
	public $updated_at;
	public $deleted_at;
	public $DRateID;
	public $FeatureCodeID;
	public $MRateID;
	public $WRateID;
	public $SizeCodeID;

	public $UnitNumber;
	public $StatusNow;
	public $Location;
	public $Party;
	public $UnitType;
	public $PartyID;

	public function rules()
	{
		return [
			[['UnitID', 'created_at', 'updated_at', 'deleted_at', 'DRateID', 'FeatureCodeID', 'MRateID', 'WRateID', 'SizeCodeID'], 'integer'],
			[['Overlock', 'WalkOrder', 'UnitOfMeasure', 'Note', 'UnitRating', 'PopupNote'], 'safe'],
			[['RateDaily', 'RateMonthly', 'RateWeekly', 'UnitHeight', 'UnitLength', 'UnitWidth'], 'number'],
			[['UnitNumber','StatusNow','Location','Party','UnitType'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'UnitID' => 'Unit ID',
			'Overlock' => 'Overlock',
			'WalkOrder' => 'Walk Order',
			'UnitOfMeasure' => 'Unit Of Measure',
			'Note' => 'Note',
			'UnitRating' => 'Unit Rating',
			'PopupNote' => 'Popup Note',
			'RateDaily' => 'Rate Daily',
			'RateMonthly' => 'Rate Monthly',
			'RateWeekly' => 'Rate Weekly',
			'UnitHeight' => 'Unit Height',
			'UnitLength' => 'Unit Length',
			'UnitWidth' => 'Unit Width',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
			'DRateID' => 'Drate ID',
			'FeatureCodeID' => 'Feature Code ID',
			'MRateID' => 'Mrate ID',
			'WRateID' => 'Wrate ID',
			'SizeCodeID' => 'Size Code ID',
			'Party' => 'Customer',
		];
	}

	public function search($params)
	{
		$query = UnitSelfStor::find();
		$query->joinWith(['unit','unit.location','unit.unitType','contract.party']);

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$dataProvider->sort->attributes['UnitNumber'] = [
	        // The tables are the ones our relation are configured to
	        // in my case they are prefixed with "tbl_"
	        'asc' => ['{{%Unit}}.UnitNumber' => SORT_ASC],
	        'desc' => ['{{%Unit}}.UnitNumber' => SORT_DESC],
	    ];

	    $dataProvider->sort->attributes['UnitType'] = [
	        // The tables are the ones our relation are configured to
	        // in my case they are prefixed with "tbl_"
	        'asc' => ['{{%UnitType}}.UnitType' => SORT_ASC],
	        'desc' => ['{{%UnitType}}.UnitType' => SORT_DESC],
	    ];

	    $dataProvider->sort->attributes['StatusNow'] = [
	        // The tables are the ones our relation are configured to
	        // in my case they are prefixed with "tbl_"
	        'asc' => ['{{%Unit}}.StatusNow' => SORT_ASC],
	        'desc' => ['{{%Unit}}.StatusNow' => SORT_DESC],
	    ];

	    $dataProvider->sort->attributes['Location'] = [
	        // The tables are the ones our relation are configured to
	        // in my case they are prefixed with "tbl_"
	        'asc' => ['{{%Location}}.Location' => SORT_ASC],
	        'desc' => ['{{%Location}}.Location' => SORT_DESC],
	    ];

	    $dataProvider->sort->attributes['Party'] = [
	        // The tables are the ones our relation are configured to
	        // in my case they are prefixed with "tbl_"
	        'asc' => ['{{%Party}}.PartyName' => SORT_ASC],
	        'desc' => ['{{%Party}}.PartyName' => SORT_DESC],
	    ];

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}



		$query->andFilterWhere([
            'UnitID' => $this->UnitID,
            'RateDaily' => $this->RateDaily,
            'RateMonthly' => $this->RateMonthly,
            'RateWeekly' => $this->RateWeekly,
            'UnitHeight' => $this->UnitHeight,
            'UnitLength' => $this->UnitLength,
            'UnitWidth' => $this->UnitWidth,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'DRateID' => $this->DRateID,
            'FeatureCodeID' => $this->FeatureCodeID,
            'MRateID' => $this->MRateID,
            'WRateID' => $this->WRateID,
            'SizeCodeID' => $this->SizeCodeID,
        ]);

		$query->andFilterWhere(['like', 'Overlock', $this->Overlock])
            ->andFilterWhere(['like', 'WalkOrder', $this->WalkOrder])
            ->andFilterWhere(['like', 'UnitOfMeasure', $this->UnitOfMeasure])
            ->andFilterWhere(['like', 'Note', $this->Note])
            ->andFilterWhere(['like', 'UnitRating', $this->UnitRating])
            ->andFilterWhere(['like', 'PopupNote', $this->PopupNote])
            ->andFilterWhere(['like', '{{%Unit}}.UnitNumber', $this->UnitNumber])
            ->andFilterWhere(['like', '{{%UnitType}}.UnitType', $this->UnitType])
            ->andFilterWhere(['like', '{{%Unit}}.StatusNow', $this->StatusNow])
            ->andFilterWhere(['like', '{{%Location}}.LocationID', $this->Location])
            ->andFilterWhere(['like', '{{%Party}}.PartyName', $this->Party]);

		return $dataProvider;
	}

	public function searchParty($params)
	{
		$query = UnitSelfStor::find();
		$query->joinWith(['unit','unit.location','unit.unitType','contract.party']);

		$query->andFilterWhere([
            '{{%Party}}.PartyID' => $this->PartyID,
        ]);

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$dataProvider->sort->attributes['UnitNumber'] = [
	        // The tables are the ones our relation are configured to
	        // in my case they are prefixed with "tbl_"
	        'asc' => ['{{%Unit}}.UnitNumber' => SORT_ASC],
	        'desc' => ['{{%Unit}}.UnitNumber' => SORT_DESC],
	    ];

	    $dataProvider->sort->attributes['UnitType'] = [
	        // The tables are the ones our relation are configured to
	        // in my case they are prefixed with "tbl_"
	        'asc' => ['{{%UnitType}}.UnitType' => SORT_ASC],
	        'desc' => ['{{%UnitType}}.UnitType' => SORT_DESC],
	    ];

	    $dataProvider->sort->attributes['StatusNow'] = [
	        // The tables are the ones our relation are configured to
	        // in my case they are prefixed with "tbl_"
	        'asc' => ['{{%Unit}}.StatusNow' => SORT_ASC],
	        'desc' => ['{{%Unit}}.StatusNow' => SORT_DESC],
	    ];

	    $dataProvider->sort->attributes['Location'] = [
	        // The tables are the ones our relation are configured to
	        // in my case they are prefixed with "tbl_"
	        'asc' => ['{{%Location}}.Location' => SORT_ASC],
	        'desc' => ['{{%Location}}.Location' => SORT_DESC],
	    ];

	    $dataProvider->sort->attributes['Party'] = [
	        // The tables are the ones our relation are configured to
	        // in my case they are prefixed with "tbl_"
	        'asc' => ['{{%Party}}.PartyName' => SORT_ASC],
	        'desc' => ['{{%Party}}.PartyName' => SORT_DESC],
	    ];

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}



		$query->andFilterWhere([
            'UnitID' => $this->UnitID,
            'RateDaily' => $this->RateDaily,
            'RateMonthly' => $this->RateMonthly,
            'RateWeekly' => $this->RateWeekly,
            'UnitHeight' => $this->UnitHeight,
            'UnitLength' => $this->UnitLength,
            'UnitWidth' => $this->UnitWidth,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'DRateID' => $this->DRateID,
            'FeatureCodeID' => $this->FeatureCodeID,
            'MRateID' => $this->MRateID,
            'WRateID' => $this->WRateID,
            'SizeCodeID' => $this->SizeCodeID,
        ]);

		$query->andFilterWhere(['like', 'Overlock', $this->Overlock])
            ->andFilterWhere(['like', 'WalkOrder', $this->WalkOrder])
            ->andFilterWhere(['like', 'UnitOfMeasure', $this->UnitOfMeasure])
            ->andFilterWhere(['like', 'Note', $this->Note])
            ->andFilterWhere(['like', 'UnitRating', $this->UnitRating])
            ->andFilterWhere(['like', 'PopupNote', $this->PopupNote])
            ->andFilterWhere(['like', '{{%Unit}}.UnitNumber', $this->UnitNumber])
            ->andFilterWhere(['like', '{{%UnitType}}.UnitType', $this->UnitType])
            ->andFilterWhere(['like', '{{%Unit}}.StatusNow', $this->StatusNow])
            ->andFilterWhere(['like', '{{%Location}}.LocationID', $this->Location])
            ->andFilterWhere(['like', '{{%Party}}.PartyName', $this->Party]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
