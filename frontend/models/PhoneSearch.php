<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Phone;

/**
 * PhoneSearch represents the model behind the search form about Phone.
 */
class PhoneSearch extends Model
{
	public $PhoneID;
	public $PartyID;
	public $PhoneTypeID;
	public $PhoneNumber;
	public $created_at;
	public $updated_at;
	public $deleted_at;

	//custom fields
	public $PhoneType;

	public function rules()
	{
		return [
			[['PhoneID', 'PartyID', 'PhoneTypeID', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
			[['PhoneNumber'], 'safe'],
			[['PhoneType'],'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'PhoneID' => 'Phone ID',
			'PartyID' => 'Party ID',
			'PhoneTypeID' => 'Phone Type ID',
			'PhoneNumber' => 'Phone Number',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
		];
	}

	public function search($params)
	{
		$query = Phone::find()->forParty($this->PartyID)->joinWith(['phoneType']);
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$dataProvider->sort->attributes['PhoneType'] = [
	        // The tables are the ones our relation are configured to
	        // in my case they are prefixed with "tbl_"
	        'asc' => ['{{%PhoneType}}.PhoneType' => SORT_ASC],
	        'desc' => ['{{%PhoneType}}.PhoneType' => SORT_DESC],
	    ];

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'PhoneID' => $this->PhoneID,
            'PartyID' => $this->PartyID,
            'PhoneTypeID' => $this->PhoneTypeID,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

		$query->andFilterWhere(['like', 'PhoneNumber', $this->PhoneNumber])
			  ->andFilterWhere(['like', '{{%PhoneType}}.PhoneType', $this->PhoneType]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
