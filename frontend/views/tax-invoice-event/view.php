<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var common\models\TaxInvoiceEvent $model
*/

$this->title = 'Tax Invoice Event View ' . $model->ID	int	NO . '';
$this->params['breadcrumbs'][] = ['label' => 'Tax Invoice Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->ID	int	NO, 'url' => ['view', 'ID	int	NO' => $model->ID	int	NO]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="tax-invoice-event-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'ID	int	NO' => $model->ID	int	NO],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Tax Invoice Event', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->ID	int	NO ?>    </h3>


    <?php $this->beginBlock('common\models\TaxInvoiceEvent'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    			'InvoiceID',
			'EventID',
			'Description',
			'Unit',
			'Amount',
			'VAT',
			'DueDate',
			'VATCode',
			'ID	int	NO',
			'GLID',
			'TaxGroupID',
			'JournalDate',
			'EventParam',
			'UserPartyID',
			'ActionID',
			'UnitID',
			'PaymentEventID',
			'fReversed',
			'created_at',
			'updated_at',
			'deleted_at',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'ID	int	NO' => $model->ID	int	NO],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> TaxInvoiceEvent',
    'content' => $this->blocks['common\models\TaxInvoiceEvent'],
    'active'  => true,
], ]
                 ]
    );
    ?></div>
