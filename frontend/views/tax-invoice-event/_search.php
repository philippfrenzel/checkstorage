<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\TaxInvoiceEventSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="tax-invoice-event-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'InvoiceID') ?>

		<?= $form->field($model, 'EventID') ?>

		<?= $form->field($model, 'Description') ?>

		<?= $form->field($model, 'Unit') ?>

		<?= $form->field($model, 'Amount') ?>

		<?php // echo $form->field($model, 'VAT') ?>

		<?php // echo $form->field($model, 'DueDate') ?>

		<?php // echo $form->field($model, 'VATCode') ?>

		<?php // echo $form->field($model, 'ID	int	NO') ?>

		<?php // echo $form->field($model, 'GLID') ?>

		<?php // echo $form->field($model, 'TaxGroupID') ?>

		<?php // echo $form->field($model, 'JournalDate') ?>

		<?php // echo $form->field($model, 'EventParam') ?>

		<?php // echo $form->field($model, 'UserPartyID') ?>

		<?php // echo $form->field($model, 'ActionID') ?>

		<?php // echo $form->field($model, 'UnitID') ?>

		<?php // echo $form->field($model, 'PaymentEventID') ?>

		<?php // echo $form->field($model, 'fReversed') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

		<?php // echo $form->field($model, 'deleted_at') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
