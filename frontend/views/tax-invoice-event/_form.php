<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\TaxInvoiceEvent $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="tax-invoice-event-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            
			<?= $form->field($model, 'InvoiceID')->textInput() ?>
			<?= $form->field($model, 'EventID')->textInput() ?>
			<?= $form->field($model, 'GLID')->textInput() ?>
			<?= $form->field($model, 'TaxGroupID')->textInput() ?>
			<?= $form->field($model, 'UserPartyID')->textInput() ?>
			<?= $form->field($model, 'ActionID')->textInput() ?>
			<?= $form->field($model, 'UnitID')->textInput() ?>
			<?= $form->field($model, 'PaymentEventID')->textInput() ?>
			<?= $form->field($model, 'fReversed')->textInput() ?>
			<?= $form->field($model, 'created_at')->textInput() ?>
			<?= $form->field($model, 'updated_at')->textInput() ?>
			<?= $form->field($model, 'deleted_at')->textInput() ?>
			<?= $form->field($model, 'Amount')->textInput() ?>
			<?= $form->field($model, 'VAT')->textInput() ?>
			<?= $form->field($model, 'DueDate')->textInput() ?>
			<?= $form->field($model, 'JournalDate')->textInput() ?>
			<?= $form->field($model, 'Description')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'Unit')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'VATCode')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'EventParam')->textInput(['maxlength' => 255]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    \yii\bootstrap\Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ [
    'label'   => 'TaxInvoiceEvent',
    'content' => $this->blocks['main'],
    'active'  => true,
], ]
                 ]
    );
    ?>
        <hr/>

        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? 'Create' : 'Save'), ['class' => $model->isNewRecord ?
        'btn btn-primary' : 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
