<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\TaxInvoiceEvent $model
 */

$this->title = 'Tax Invoice Event Update ' . $model->ID	int	NO . '';
$this->params['breadcrumbs'][] = ['label' => 'Tax Invoice Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->ID	int	NO, 'url' => ['view', 'ID	int	NO' => $model->ID	int	NO]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="tax-invoice-event-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'ID	int	NO' => $model->ID	int	NO], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
