<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var common\models\TaxInvoice $model
*/

$this->title = 'Tax Invoice View ' . $model->InvoiceID . '';
$this->params['breadcrumbs'][] = ['label' => 'Tax Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->InvoiceID, 'url' => ['view', 'InvoiceID' => $model->InvoiceID]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="tax-invoice-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'InvoiceID' => $model->InvoiceID],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Tax Invoice', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->InvoiceID ?>    </h3>


    <?php $this->beginBlock('common\models\TaxInvoice'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    			'InvoiceID',
			'InvoiceNumber',
			'InvoiceDate',
			'DueDate',
			'PartyID',
			'ContractID',
			'Note',
			'Comment',
			'Param',
			'Export',
			'InvoiceNumeric',
			'created_at',
			'updated_at',
			'deleted_at',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'InvoiceID' => $model->InvoiceID],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
<?php $this->beginBlock('TaxInvoiceEvents'); ?>
<p class='pull-right'>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-list"></span> List All Tax Invoice Events',
            ['tax-invoice-event/index'],
            ['class'=>'btn text-muted btn-xs']
        ) ?>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-plus"></span> New Tax Invoice Event',
            ['tax-invoice-event/create', 'TaxInvoiceEvent'=>['InvoiceID'=>$model->InvoiceID]],
            ['class'=>'btn btn-success btn-xs']
        ) ?>
</p><div class='clearfix'></div>
<?php $this->endBlock() ?>


    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> TaxInvoice',
    'content' => $this->blocks['common\models\TaxInvoice'],
    'active'  => true,
],[
    'label'   => '<small><span class="glyphicon glyphicon-paperclip"></span> Tax Invoice Events</small>',
    'content' => $this->blocks['TaxInvoiceEvents'],
    'active'  => false,
], ]
                 ]
    );
    ?></div>
