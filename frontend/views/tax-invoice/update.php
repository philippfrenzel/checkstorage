<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\TaxInvoice $model
 */

$this->title = 'Tax Invoice Update ' . $model->InvoiceID . '';
$this->params['breadcrumbs'][] = ['label' => 'Tax Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->InvoiceID, 'url' => ['view', 'InvoiceID' => $model->InvoiceID]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="tax-invoice-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'InvoiceID' => $model->InvoiceID], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
