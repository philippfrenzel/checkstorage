<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\TaxInvoice $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="tax-invoice-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            
			<?= $form->field($model, 'InvoiceDate')->textInput() ?>
			<?= $form->field($model, 'DueDate')->textInput() ?>
			<?= $form->field($model, 'PartyID')->textInput() ?>
			<?= $form->field($model, 'ContractID')->textInput() ?>
			<?= $form->field($model, 'Export')->textInput() ?>
			<?= $form->field($model, 'InvoiceNumeric')->textInput() ?>
			<?= $form->field($model, 'created_at')->textInput() ?>
			<?= $form->field($model, 'updated_at')->textInput() ?>
			<?= $form->field($model, 'deleted_at')->textInput() ?>
			<?= $form->field($model, 'InvoiceNumber')->textInput(['maxlength' => 50]) ?>
			<?= $form->field($model, 'Note')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'Comment')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'Param')->textInput(['maxlength' => 255]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    \yii\bootstrap\Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ [
    'label'   => 'TaxInvoice',
    'content' => $this->blocks['main'],
    'active'  => true,
], ]
                 ]
    );
    ?>
        <hr/>

        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? 'Create' : 'Save'), ['class' => $model->isNewRecord ?
        'btn btn-primary' : 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
