<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\TaxInvoiceSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="tax-invoice-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'InvoiceID') ?>

		<?= $form->field($model, 'InvoiceNumber') ?>

		<?= $form->field($model, 'InvoiceDate') ?>

		<?= $form->field($model, 'DueDate') ?>

		<?= $form->field($model, 'PartyID') ?>

		<?php // echo $form->field($model, 'ContractID') ?>

		<?php // echo $form->field($model, 'Note') ?>

		<?php // echo $form->field($model, 'Comment') ?>

		<?php // echo $form->field($model, 'Param') ?>

		<?php // echo $form->field($model, 'Export') ?>

		<?php // echo $form->field($model, 'InvoiceNumeric') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

		<?php // echo $form->field($model, 'deleted_at') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
