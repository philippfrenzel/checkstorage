<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\Contract $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="contract-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            
			<?= $form->field($model, 'UnitSortOrder')->textInput() ?>
			<?= $form->field($model, 'IsInProcessor')->textInput() ?>
			<?= $form->field($model, 'created_at')->textInput() ?>
			<?= $form->field($model, 'updated_at')->textInput() ?>
			<?= $form->field($model, 'deleted_at')->textInput() ?>
			<?= $form->field($model, 'UserID')->textInput() ?>
			<?= $form->field($model, 'PartyID')->textInput() ?>
			<?= $form->field($model, 'MarketReasonID')->textInput() ?>
			<?= $form->field($model, 'MarketSourceID')->textInput() ?>
			<?= $form->field($model, 'ContractTypeID')->textInput() ?>
			<?= $form->field($model, 'UnitList')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'PTReason')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'AmtDue')->textInput(['maxlength' => 19]) ?>
			<?= $form->field($model, 'Balance')->textInput(['maxlength' => 19]) ?>
			<?= $form->field($model, 'Deposit')->textInput(['maxlength' => 19]) ?>
			<?= $form->field($model, 'MarketDistance')->textInput() ?>
			<?= $form->field($model, 'OpenCredits')->textInput(['maxlength' => 19]) ?>
			<?= $form->field($model, 'Rent')->textInput(['maxlength' => 19]) ?>
			<?= $form->field($model, 'UnitIns')->textInput(['maxlength' => 19]) ?>
			<?= $form->field($model, 'UnitOther')->textInput(['maxlength' => 19]) ?>
			<?= $form->field($model, 'UnitRent')->textInput(['maxlength' => 19]) ?>
			<?= $form->field($model, 'UnitTax')->textInput(['maxlength' => 19]) ?>
			<?= $form->field($model, 'Effective')->textInput() ?>
			<?= $form->field($model, 'LastRebuildPrepay')->textInput() ?>
			<?= $form->field($model, 'MinimumDuration')->textInput() ?>
			<?= $form->field($model, 'Pau')->textInput() ?>
			<?= $form->field($model, 'PTD')->textInput() ?>
			<?= $form->field($model, 'PTDateMovein')->textInput() ?>
			<?= $form->field($model, 'PTDateNeeded')->textInput() ?>
			<?= $form->field($model, 'VacateDate')->textInput() ?>
			<?= $form->field($model, 'CycleLength')->textInput(['maxlength' => 5]) ?>
			<?= $form->field($model, 'ContractNumber')->textInput(['maxlength' => 50]) ?>
			<?= $form->field($model, 'Param')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'PTSizeNeeded')->textInput(['maxlength' => 255]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    \yii\bootstrap\Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ [
    'label'   => 'Contract',
    'content' => $this->blocks['main'],
    'active'  => true,
], ]
                 ]
    );
    ?>
        <hr/>

        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? 'Create' : 'Save'), ['class' => $model->isNewRecord ?
        'btn btn-primary' : 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
