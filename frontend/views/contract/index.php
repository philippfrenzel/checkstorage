<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
* @var yii\web\View $this
* @var yii\data\ActiveDataProvider $dataProvider
* @var frontend\models\ContractSearch $searchModel
*/

$this->title = 'Contracts';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="contract-index">

    <?php //     echo $this->render('_search', ['model' =>$searchModel]);
    ?>

    <div class="clearfix">
        <p class="pull-left">
            <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Contract', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <div class="pull-right">


                                                                                                                                        
            <?php 
            echo \yii\bootstrap\ButtonDropdown::widget(
                [
                    'id'       => 'giiant-relations',
                    'encodeLabel' => false,
                    'label'    => '<span class="glyphicon glyphicon-paperclip"></span> Relations',
                    'dropdown' => [
                        'options'      => [
                            'class' => 'dropdown-menu-right'
                        ],
                        'encodeLabels' => false,
                        'items'        => [
    [
        'label' => '<i class="glyphicon glyphicon-arrow-left"> Contract Type</i>',
        'url' => [
            'contract-type/index',
        ],
    ],
    [
        'label' => '<i class="glyphicon glyphicon-arrow-left"> Market Reason</i>',
        'url' => [
            'market-reason/index',
        ],
    ],
    [
        'label' => '<i class="glyphicon glyphicon-arrow-left"> Market Source</i>',
        'url' => [
            'market-source/index',
        ],
    ],
    [
        'label' => '<i class="glyphicon glyphicon-arrow-left"> Party</i>',
        'url' => [
            'party/index',
        ],
    ],
]                    ],
                ]
            );
            ?>        </div>
    </div>

            <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
        
			//'ContractID',
            'Effective',
            'Pau',
			'CycleLength',
			'UnitSortOrder',
			'ContractNumber',
			'Param',
			'PTSizeNeeded',
			'UnitList:ntext',
			/*'PTReason:ntext'*/
			/*'AmtDue'*/
			/*'Balance'*/
			/*'Deposit'*/
			/**/
			/*'IsInProcessor'*/
			/*'LastRebuildPrepay'*/
			/*'MarketDistance'*/
			/*'MinimumDuration'*/
			/*'OpenCredits'*/
			/**/
			/*'PTD'*/
			/*'PTDateMovein'*/
			/*'PTDateNeeded'*/
			/*'Rent'*/
			/*'UnitIns'*/
			/*'UnitOther'*/
			/*'UnitRent'*/
			/*'UnitTax'*/
			/*'VacateDate'*/
			/*'created_at'*/
			/*'updated_at'*/
			/*'deleted_at'*/
			/*'UserID'*/
			'PartyID',
			/*'MarketReasonID'*/
			/*'MarketSourceID'*/
			/*'ContractTypeID'*/
            [
                'class' => 'yii\grid\ActionColumn',
                'urlCreator' => function($action, $model, $key, $index) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
                    $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                    return \yii\helpers\Url::toRoute($params);
                },
                'contentOptions' => ['nowrap'=>'nowrap']
            ],
        ],
    ]); ?>
    
</div>
