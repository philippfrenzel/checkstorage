<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Contract $model
 */

$this->title = 'Contract Update ' . $model->ContractID . '';
$this->params['breadcrumbs'][] = ['label' => 'Contracts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->ContractID, 'url' => ['view', 'ContractID' => $model->ContractID]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="contract-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'ContractID' => $model->ContractID], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
