<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var frontend\models\ContractSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="contract-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'ContractID') ?>

		<?= $form->field($model, 'CycleLength') ?>

		<?= $form->field($model, 'UnitSortOrder') ?>

		<?= $form->field($model, 'ContractNumber') ?>

		<?= $form->field($model, 'Param') ?>

		<?php // echo $form->field($model, 'PTSizeNeeded') ?>

		<?php // echo $form->field($model, 'UnitList') ?>

		<?php // echo $form->field($model, 'PTReason') ?>

		<?php // echo $form->field($model, 'AmtDue') ?>

		<?php // echo $form->field($model, 'Balance') ?>

		<?php // echo $form->field($model, 'Deposit') ?>

		<?php // echo $form->field($model, 'Effective') ?>

		<?php // echo $form->field($model, 'IsInProcessor') ?>

		<?php // echo $form->field($model, 'LastRebuildPrepay') ?>

		<?php // echo $form->field($model, 'MarketDistance') ?>

		<?php // echo $form->field($model, 'MinimumDuration') ?>

		<?php // echo $form->field($model, 'OpenCredits') ?>

		<?php // echo $form->field($model, 'Pau') ?>

		<?php // echo $form->field($model, 'PTD') ?>

		<?php // echo $form->field($model, 'PTDateMovein') ?>

		<?php // echo $form->field($model, 'PTDateNeeded') ?>

		<?php // echo $form->field($model, 'Rent') ?>

		<?php // echo $form->field($model, 'UnitIns') ?>

		<?php // echo $form->field($model, 'UnitOther') ?>

		<?php // echo $form->field($model, 'UnitRent') ?>

		<?php // echo $form->field($model, 'UnitTax') ?>

		<?php // echo $form->field($model, 'VacateDate') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

		<?php // echo $form->field($model, 'deleted_at') ?>

		<?php // echo $form->field($model, 'UserID') ?>

		<?php // echo $form->field($model, 'PartyID') ?>

		<?php // echo $form->field($model, 'MarketReasonID') ?>

		<?php // echo $form->field($model, 'MarketSourceID') ?>

		<?php // echo $form->field($model, 'ContractTypeID') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
