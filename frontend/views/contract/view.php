<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var common\models\Contract $model
*/

$this->title = 'Contract View ' . $model->ContractID . '';
$this->params['breadcrumbs'][] = ['label' => 'Contracts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->ContractID, 'url' => ['view', 'ContractID' => $model->ContractID]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="contract-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'ContractID' => $model->ContractID],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Contract', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->ContractID ?>    </h3>


    <?php $this->beginBlock('common\models\Contract'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    			'ContractID',
			'CycleLength',
			'UnitSortOrder',
			'ContractNumber',
			'Param',
			'PTSizeNeeded',
			'UnitList:ntext',
			'PTReason:ntext',
			'AmtDue',
			'Balance',
			'Deposit',
			'Effective',
			'IsInProcessor',
			'LastRebuildPrepay',
			'MarketDistance',
			'MinimumDuration',
			'OpenCredits',
			'Pau',
			'PTD',
			'PTDateMovein',
			'PTDateNeeded',
			'Rent',
			'UnitIns',
			'UnitOther',
			'UnitRent',
			'UnitTax',
			'VacateDate',
			'created_at',
			'updated_at',
			'deleted_at',
			'UserID',
			'PartyID',
			'MarketReasonID',
			'MarketSourceID',
			'ContractTypeID',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'ContractID' => $model->ContractID],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> Contract',
    'content' => $this->blocks['common\models\Contract'],
    'active'  => true,
], ]
                 ]
    );
    ?></div>
