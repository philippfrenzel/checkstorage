<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use kartik\widgets\Growl;
//use philippfrenzel\yii2tooltipster\yii2tooltipster;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php
/*echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tipster',
        'theme' => '.tooltipster-default',
    ]
]);*/
?>

<?php $this->beginBody() ?>

<div id="sb-site">

    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => '
                    Check Storage &nbsp;',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar navbar-fixed-top',
                ],
            ]);


            $menuItems = [                
                ['label' => '<i class="fa fa-home"></i> Home', 'url' => ['/site/index']],
                ['label' => '<i class="fa fa-info"></i> About', 'url' => ['/site/about']],
                ['label' => '<i class="fa fa-inbox"></i> Contact', 'url' => ['/site/contact']],
            ];

            if (Yii::$app->hasModule('user')) {
                if (Yii::$app->user->isGuest) {
                    $menuItems[] = ['label' => '<i class="fa fa-sign-in"></i> Signup', 'url' => ['/user/registration/register']];
                    $menuItems[] = ['label' => '<i class="fa fa-sign-in"></i> Login', 'url' => ['/user/security/login']];
                } else {
                    $menuItems[] = [
                        'label'       => 'Logout (' . Yii::$app->user->identity->username . ')',
                        'url'         => ['/user/security/logout'],
                        'linkOptions' => ['data-method' => 'post']
                    ];
                }
            }

            ?>
            
            <div class="navbar-nav nav sb-toggle-left navbar-left">
                        <div class="navicon-line"></div>
                        <div class="navicon-line"></div>
                        <div class="navicon-line"></div>
                        <div class="navicon-line"></div>
                        <div class="navicon-line"></div>
                        <div class="navicon-line"></div>
                        <div class="navicon-line"></div>
            </div> &nbsp; 

            <?php

            echo Nav::widget([
                'encodeLabels' => false,
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items'   => $menuItems
            ]);
            NavBar::end();
        ?>

        <div class="container">
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <?= Growl::widget([
                  'type'  => Growl::TYPE_INFO,
                  'icon'  => 'glyphicon glyphicon-ok-sign',
                  'title' => 'Hinweis',
                  'showSeparator' => true,
                  'body'  => Yii::$app->session->getFlash('success')
                ]);?>
            <?php elseif (Yii::$app->session->hasFlash('error')): ?>
                <?= Growl::widget([
                  'type'  => Growl::TYPE_WARNING,
                  'icon'  => 'glyphicon glyphicon-ok-sign',
                  'title' => 'Alert',
                  'showSeparator' => true,
                  'body'  => Yii::$app->session->getFlash('error')
                ]);?>
            <?php endif; ?>

            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

            <?= $content ?>
        </div>
    </div>    
    
    <div class="clear-fix"></div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; Frenzel GmbH <?= date('Y') ?></p>
            <p class="pull-right"> <i class="fa fa-mail"></i> Philipp@Frenzel.net</p>
        </div>
    </footer>

</div>

<?php
    echo philippfrenzel\yii2slidebars\yii2slidebars::widget([]);
?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
