<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<div class="row">
   <div class="col-md-3">

<?php if(!\Yii::$app->user->isGuest): ?>

<?php endif; ?>	

    <div class="pg-sidebar">
      <?php       
        if(isset($this->blocks['sidebar']))
        {   
          echo $this->blocks['sidebar'];
        } 
      ?>
    </div>      
  </div>
  <div class="col-md-9">
    <?= $content; ?>
  </div>
</div>
<?php $this->endContent(); ?>
