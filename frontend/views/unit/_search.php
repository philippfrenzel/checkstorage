<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var frontend\models\UnitSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="unit-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'UnitID') ?>

		<?= $form->field($model, 'Unit') ?>

		<?= $form->field($model, 'StatusNow') ?>

		<?= $form->field($model, 'SortOrder') ?>

		<?= $form->field($model, 'UnitNumber') ?>

		<?php // echo $form->field($model, 'CatalogSet') ?>

		<?php // echo $form->field($model, 'CatalogDesc') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

		<?php // echo $form->field($model, 'deleted_at') ?>

		<?php // echo $form->field($model, 'SiteID') ?>

		<?php // echo $form->field($model, 'LocationID') ?>

		<?php // echo $form->field($model, 'OffSiteAddressID') ?>

		<?php // echo $form->field($model, 'UnitTypeID') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
