<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\Unit $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="unit-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            
			<?= $form->field($model, 'StatusNow')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'CatalogSet')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'CatalogDesc')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'SortOrder')->textInput() ?>
			<?= $form->field($model, 'created_at')->textInput() ?>
			<?= $form->field($model, 'updated_at')->textInput() ?>
			<?= $form->field($model, 'deleted_at')->textInput() ?>
			<?= $form->field($model, 'SiteID')->textInput() ?>
			<?= $form->field($model, 'LocationID')->textInput() ?>
			<?= $form->field($model, 'OffSiteAddressID')->textInput() ?>
			<?= $form->field($model, 'UnitTypeID')->textInput() ?>
			<?= $form->field($model, 'Unit')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'UnitNumber')->textInput(['maxlength' => 255]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    \yii\bootstrap\Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ [
    'label'   => 'Unit',
    'content' => $this->blocks['main'],
    'active'  => true,
], ]
                 ]
    );
    ?>
        <hr/>

        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? 'Create' : 'Save'), ['class' => $model->isNewRecord ?
        'btn btn-primary' : 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
