<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Unit $model
 */

$this->title = 'Unit Update ' . $model->UnitID . '';
$this->params['breadcrumbs'][] = ['label' => 'Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->UnitID, 'url' => ['view', 'UnitID' => $model->UnitID]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="unit-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'UnitID' => $model->UnitID], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
