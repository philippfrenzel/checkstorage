<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var common\models\Unit $model
*/

$this->title = 'Unit View ' . $model->UnitID . '';
$this->params['breadcrumbs'][] = ['label' => 'Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->UnitID, 'url' => ['view', 'UnitID' => $model->UnitID]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="unit-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'UnitID' => $model->UnitID],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Unit', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->UnitID ?>    </h3>


    <?php $this->beginBlock('common\models\Unit'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    		'unit.UnitNumber',
			'CatalogSet:ntext',
			'CatalogDesc:ntext',
			'created_at',
			'updated_at',
			'deleted_at',
			'SiteID',
			'LocationID',
			'OffSiteAddressID',
			'UnitTypeID',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'UnitID' => $model->UnitID],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> Unit',
    'content' => $this->blocks['common\models\Unit'],
    'active'  => true,
], ]
                 ]
    );
    ?></div>
