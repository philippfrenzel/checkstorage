<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/**
* @var yii\web\View $this
* @var yii\data\ActiveDataProvider $dataProvider
* @var frontend\models\UnitSearch $searchModel
*/

$this->title = 'Units';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="unit-index">

    <?php //     echo $this->render('_search', ['model' =>$searchModel]);
    ?>

    <div class="clearfix">
        <p class="pull-left">
            <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Unit', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <div class="pull-right">


                                                                                                            
            <?php 
            echo \yii\bootstrap\ButtonDropdown::widget(
                [
                    'id'       => 'giiant-relations',
                    'encodeLabel' => false,
                    'label'    => '<span class="glyphicon glyphicon-paperclip"></span> Relations',
                    'dropdown' => [
                        'options'      => [
                            'class' => 'dropdown-menu-right'
                        ],
                        'encodeLabels' => false,
                        'items'        => [
    [
        'label' => '<i class="glyphicon glyphicon-arrow-left"> Location</i>',
        'url' => [
            'location/index',
        ],
    ],
    [
        'label' => '<i class="glyphicon glyphicon-arrow-left"> Unit Type</i>',
        'url' => [
            'unit-type/index',
        ],
    ],
    [
        'label' => '<i class="glyphicon glyphicon-arrow-left"> Unit Self Stor</i>',
        'url' => [
            'unit-self-stor/index',
        ],
    ],
]                    ],
                ]
            );
            ?>        </div>
    </div>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'showPageSummary' => true,
        'hover' => true,
        'floatHeader' => true,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true
        ],
        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="fa fa-archive"></i> Artikel</h3>',
        ],
        'columns' => [
        
			'UnitID',
			'Unit',
			'StatusNow:ntext',
			'SortOrder',
			'UnitNumber',
			'CatalogSet:ntext',
			'CatalogDesc:ntext',
			/*'created_at'*/
			/*'updated_at'*/
			/*'deleted_at'*/
			/*'SiteID'*/
			/*'LocationID'*/
			/*'OffSiteAddressID'*/
			/*'UnitTypeID'*/
            [
                'class' => 'kartik\grid\ActionColumn',
                'urlCreator' => function($action, $model, $key, $index) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
                    $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                    return \yii\helpers\Url::toRoute($params);
                },
                'contentOptions' => ['nowrap'=>'nowrap']
            ],
        ],
    ]); ?>
    
</div>
