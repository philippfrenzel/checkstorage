<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\Event $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="event-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            
			<?= $form->field($model, 'ActionID')->textInput() ?>
			<?= $form->field($model, 'created_at')->textInput() ?>
			<?= $form->field($model, 'updated_at')->textInput() ?>
			<?= $form->field($model, 'UserPartyID')->textInput() ?>
			<?= $form->field($model, 'CID')->textInput() ?>
			<?= $form->field($model, 'deleted_at')->textInput() ?>
			<?= $form->field($model, 'Created')->textInput() ?>
			<?= $form->field($model, 'JournalDate')->textInput() ?>
			<?= $form->field($model, 'datFrom')->textInput() ?>
			<?= $form->field($model, 'datTo')->textInput() ?>
			<?= $form->field($model, 'EAmount')->textInput() ?>
			<?= $form->field($model, 'ETax')->textInput() ?>
			<?= $form->field($model, 'Param')->textInput(['maxlength' => 1000]) ?>
			<?= $form->field($model, 'ESnapshot')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'Description')->textInput(['maxlength' => 255]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    \yii\bootstrap\Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ [
    'label'   => 'Event',
    'content' => $this->blocks['main'],
    'active'  => true,
], ]
                 ]
    );
    ?>
        <hr/>

        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? 'Create' : 'Save'), ['class' => $model->isNewRecord ?
        'btn btn-primary' : 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
