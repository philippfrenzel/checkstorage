<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var common\models\Event $model
*/

$this->title = 'Event View ' . $model->EventID . '';
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->EventID, 'url' => ['view', 'EventID' => $model->EventID]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="event-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'EventID' => $model->EventID],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Event', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->EventID ?>    </h3>


    <?php $this->beginBlock('common\models\Event'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    			'EventID',
			'ActionID',
			'Param',
			'UserPartyID',
			'Created',
			'JournalDate',
			'EAmount',
			'ETax',
			'ESnapshot',
			'CID',
			'datFrom',
			'datTo',
			'Description',
			'created_at',
			'updated_at',
			'deleted_at',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'EventID' => $model->EventID],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> Event',
    'content' => $this->blocks['common\models\Event'],
    'active'  => true,
], ]
                 ]
    );
    ?></div>
