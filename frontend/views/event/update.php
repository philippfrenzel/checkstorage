<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Event $model
 */

$this->title = 'Event Update ' . $model->EventID . '';
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->EventID, 'url' => ['view', 'EventID' => $model->EventID]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="event-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'EventID' => $model->EventID], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
