<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Occupation $model
 */

$this->title = 'Occupation Update ' . $model->OccupationID . '';
$this->params['breadcrumbs'][] = ['label' => 'Occupations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->OccupationID, 'url' => ['view', 'OccupationID' => $model->OccupationID]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="occupation-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'OccupationID' => $model->OccupationID], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
