<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Phone $model
 */

$this->title = 'Phone Update ' . $model->PhoneID . '';
$this->params['breadcrumbs'][] = ['label' => 'Phones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->PhoneID, 'url' => ['view', 'PhoneID' => $model->PhoneID]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="phone-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'PhoneID' => $model->PhoneID], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
