<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var common\models\Phone $model
*/

$this->title = 'Phone View ' . $model->PhoneID . '';
$this->params['breadcrumbs'][] = ['label' => 'Phones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->PhoneID, 'url' => ['view', 'PhoneID' => $model->PhoneID]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="phone-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'PhoneID' => $model->PhoneID],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Phone', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->PhoneID ?>    </h3>


    <?php $this->beginBlock('common\models\Phone'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    			'PhoneID',
			'PartyID',
			'PhoneTypeID',
			'PhoneNumber',
			'created_at',
			'updated_at',
			'deleted_at',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'PhoneID' => $model->PhoneID],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> Phone',
    'content' => $this->blocks['common\models\Phone'],
    'active'  => true,
], ]
                 ]
    );
    ?></div>
