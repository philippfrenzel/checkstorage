<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var common\models\Party $model
*/

$this->title = 'Party View ' . $model->PartyID . '';
$this->params['breadcrumbs'][] = ['label' => 'Parties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->PartyID, 'url' => ['view', 'PartyID' => $model->PartyID]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="party-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'PartyID' => $model->PartyID],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Party', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->PartyID ?>    </h3>


    <?php $this->beginBlock('common\models\Party'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    			'PartyID',
			'DLData',
			'DLNumber',
			'ID1',
			'ID2',
			'UnitSortOrder',
			'GrpActRef',
			'DLRegionAbbr',
			'Identification2',
			'Identification1',
			'PartyName',
			'Soundex',
			'Alert:ntext',
			'PartyParam',
			'UnitList',
			'Note:ntext',
			'Created',
			'LockTime',
			'LockUser',
			'PTD',
			'created_at',
			'updated_at',
			'deleted_at',
			'PartyTypeID',
			'OccupationID',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'PartyID' => $model->PartyID],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
<?php $this->beginBlock('Addresses'); ?>
<p class='pull-right'>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-list"></span> List All Addresses',
            ['address/index'],
            ['class'=>'btn text-muted btn-xs']
        ) ?>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-plus"></span> New Address',
            ['address/create', 'Address'=>['PartyID'=>$model->PartyID]],
            ['class'=>'btn btn-success btn-xs']
        ) ?>
</p><div class='clearfix'></div>
<?php $this->endBlock() ?>


    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> Party',
    'content' => $this->blocks['common\models\Party'],
    'active'  => true,
],[
    'label'   => '<small><span class="glyphicon glyphicon-paperclip"></span> Addresses</small>',
    'content' => $this->blocks['Addresses'],
    'active'  => false,
], ]
                 ]
    );
    ?></div>
