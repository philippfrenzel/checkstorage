<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\Party $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="party-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            
			<?= $form->field($model, 'DLNumber')->textInput() ?>
			<?= $form->field($model, 'ID1')->textInput() ?>
			<?= $form->field($model, 'ID2')->textInput() ?>
			<?= $form->field($model, 'Alert')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'Note')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'UnitSortOrder')->textInput() ?>
			<?= $form->field($model, 'GrpActRef')->textInput() ?>
			<?= $form->field($model, 'LockUser')->textInput() ?>
			<?= $form->field($model, 'created_at')->textInput() ?>
			<?= $form->field($model, 'updated_at')->textInput() ?>
			<?= $form->field($model, 'deleted_at')->textInput() ?>
			<?= $form->field($model, 'PartyTypeID')->textInput() ?>
			<?= $form->field($model, 'OccupationID')->textInput() ?>
			<?= $form->field($model, 'Created')->textInput() ?>
			<?= $form->field($model, 'LockTime')->textInput() ?>
			<?= $form->field($model, 'PTD')->textInput() ?>
			<?= $form->field($model, 'DLData')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'DLRegionAbbr')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'Identification2')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'Identification1')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'PartyName')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'Soundex')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'UnitList')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'PartyParam')->textInput(['maxlength' => 1000]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    \yii\bootstrap\Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ [
    'label'   => 'Party',
    'content' => $this->blocks['main'],
    'active'  => true,
], ]
                 ]
    );
    ?>
        <hr/>

        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? 'Create' : 'Save'), ['class' => $model->isNewRecord ?
        'btn btn-primary' : 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
