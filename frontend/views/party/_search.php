<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var frontend\models\PartySearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="party-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'PartyID') ?>

		<?= $form->field($model, 'DLData') ?>

		<?= $form->field($model, 'DLNumber') ?>

		<?= $form->field($model, 'ID1') ?>

		<?= $form->field($model, 'ID2') ?>

		<?php // echo $form->field($model, 'UnitSortOrder') ?>

		<?php // echo $form->field($model, 'GrpActRef') ?>

		<?php // echo $form->field($model, 'DLRegionAbbr') ?>

		<?php // echo $form->field($model, 'Identification2') ?>

		<?php // echo $form->field($model, 'Identification1') ?>

		<?php // echo $form->field($model, 'PartyName') ?>

		<?php // echo $form->field($model, 'Soundex') ?>

		<?php // echo $form->field($model, 'Alert') ?>

		<?php // echo $form->field($model, 'PartyParam') ?>

		<?php // echo $form->field($model, 'UnitList') ?>

		<?php // echo $form->field($model, 'Note') ?>

		<?php // echo $form->field($model, 'Created') ?>

		<?php // echo $form->field($model, 'LockTime') ?>

		<?php // echo $form->field($model, 'LockUser') ?>

		<?php // echo $form->field($model, 'PTD') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

		<?php // echo $form->field($model, 'deleted_at') ?>

		<?php // echo $form->field($model, 'PartyTypeID') ?>

		<?php // echo $form->field($model, 'OccupationID') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
