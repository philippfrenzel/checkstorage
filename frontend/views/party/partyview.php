<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var common\models\Party $model
*/

$this->title = 'Party View ' . $model->PartyID . '';
$this->params['breadcrumbs'][] = ['label' => 'Parties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->PartyID, 'url' => ['view', 'PartyID' => $model->PartyID]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="party-view">

    <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    <div class="row">
        <div class="col-md-4">
            <h4>Kunde</h4>
             <h3> <?= Html::encode($model->PartyName) ?> </h3>
             <div class="pull-left">
                <i class="fa fa-road fa-2x"></i>
             </div>
             <address>
                &nbsp;<?= Html::encode($model->address->Street) ?> <br>
                &nbsp;<?= Html::encode($model->address->Code) ?> <?= Html::encode($model->address->City) ?>
            </address>
        </div>
        <div class="col-md-4">
            <h4>Kontakt</h4>
            <?php
                echo common\widgets\phone\DisplayWidget::widget([ 'PartyID' => $model->PartyID ]);
            ?> 
        </div>
        <div class="col-md-4">
            <h4>Abteile</h4>
            <?php
                echo common\widgets\unit\DisplayRelatedWidget::widget([ 'PartyID' => $model->PartyID ]);
            ?>
        </div>
    </div>    

   
   <?php $this->beginBlock('common\models\Action'); ?>

        <?php
            echo common\widgets\action\DisplayRelatedWidget::widget([ 'PartyID' => $model->PartyID ]);
        ?>

   <?php $this->endBlock(); ?>

   <?php $this->beginBlock('common\models\TaxInvoice'); ?>

        <?php
            echo common\widgets\taxinvoice\DisplayRelatedWidget::widget([ 'PartyID' => $model->PartyID ]);
        ?>

   <?php $this->endBlock(); ?>


    <?php $this->beginBlock('common\models\Party'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
            'PartyName',
    		'PartyID',
			//'DLData',
			//'DLNumber',
			//'ID1',
			//'ID2',
			'UnitSortOrder',
			//'GrpActRef',
			//'DLRegionAbbr',
			//'Identification2',
			//'Identification1',
			
			//'Soundex',
			'Alert:ntext',
			//'PartyParam',
			//'UnitList',
			'Note:ntext',
			//'Created',
			//'LockTime',
			//'LockUser',
			//'PTD',
			//'created_at',
			//'updated_at',
			//'deleted_at',
			//'PartyTypeID',
			//'OccupationID',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'PartyID' => $model->PartyID],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
<?php $this->beginBlock('Addresses'); ?>
<p class='pull-right'>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-list"></span> List All Addresses',
            ['address/index'],
            ['class'=>'btn text-muted btn-xs']
        ) ?>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-plus"></span> New Address',
            ['address/create', 'Address'=>['PartyID'=>$model->PartyID]],
            ['class'=>'btn btn-success btn-xs']
        ) ?>
</p><div class='clearfix'></div>
<?php $this->endBlock() ?>


    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                    'id' => 'relation-tabs',
                    'encodeLabels' => false,
                    'items' => [ 
                        [
                            'label'   => '<i class="fa fa-list"></i> Ledger',
                            'content' => $this->blocks['common\models\Action'],
                            'active'  => true,
                        ],
                        [
                            'label'   => '<i class="fa fa-list"></i> Invoices',
                            'content' => $this->blocks['common\models\TaxInvoice'],
                        ]
                    ]
                 ]
    );
    ?>

</div>
