<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Party $model
 */

$this->title = 'Party Update ' . $model->PartyID . '';
$this->params['breadcrumbs'][] = ['label' => 'Parties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->PartyID, 'url' => ['view', 'PartyID' => $model->PartyID]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="party-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'PartyID' => $model->PartyID], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
