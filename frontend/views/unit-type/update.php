<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\UnitType $model
 */

$this->title = 'Unit Type Update ' . $model->UnitTypeID . '';
$this->params['breadcrumbs'][] = ['label' => 'Unit Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->UnitTypeID, 'url' => ['view', 'UnitTypeID' => $model->UnitTypeID]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="unit-type-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'UnitTypeID' => $model->UnitTypeID], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
