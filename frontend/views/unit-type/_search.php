<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var frontend\models\UnitTypeSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="unit-type-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'UnitTypeID') ?>

		<?= $form->field($model, 'Icon') ?>

		<?= $form->field($model, 'QueryName') ?>

		<?= $form->field($model, 'TableName') ?>

		<?= $form->field($model, 'UnitType') ?>

		<?php // echo $form->field($model, 'Notes') ?>

		<?php // echo $form->field($model, 'SortOrder') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

		<?php // echo $form->field($model, 'deleted_at') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
