<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\UnitSelfStor $model
 */

$this->title = 'Unit Self Stor Update ' . $model->UnitID . '';
$this->params['breadcrumbs'][] = ['label' => 'Unit Self Stors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->UnitID, 'url' => ['view', 'UnitID' => $model->UnitID]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="unit-self-stor-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'UnitID' => $model->UnitID], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
