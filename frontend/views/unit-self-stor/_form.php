<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\UnitSelfStor $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="unit-self-stor-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            
			<?= $form->field($model, 'Note')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'UnitRating')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'PopupNote')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'RateDaily')->textInput() ?>
			<?= $form->field($model, 'RateMonthly')->textInput() ?>
			<?= $form->field($model, 'RateWeekly')->textInput() ?>
			<?= $form->field($model, 'UnitHeight')->textInput() ?>
			<?= $form->field($model, 'UnitLength')->textInput() ?>
			<?= $form->field($model, 'UnitWidth')->textInput() ?>
			<?= $form->field($model, 'created_at')->textInput() ?>
			<?= $form->field($model, 'updated_at')->textInput() ?>
			<?= $form->field($model, 'DRateID')->textInput() ?>
			<?= $form->field($model, 'FeatureCodeID')->textInput() ?>
			<?= $form->field($model, 'MRateID')->textInput() ?>
			<?= $form->field($model, 'WRateID')->textInput() ?>
			<?= $form->field($model, 'SizeCodeID')->textInput() ?>
			<?= $form->field($model, 'deleted_at')->textInput() ?>
			<?= $form->field($model, 'Overlock')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'WalkOrder')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'UnitOfMeasure')->textInput(['maxlength' => 255]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    \yii\bootstrap\Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ [
    'label'   => 'UnitSelfStor',
    'content' => $this->blocks['main'],
    'active'  => true,
], ]
                 ]
    );
    ?>
        <hr/>

        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? 'Create' : 'Save'), ['class' => $model->isNewRecord ?
        'btn btn-primary' : 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
