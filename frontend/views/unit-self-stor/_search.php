<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var frontend\models\UnitSelfStorSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="unit-self-stor-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'UnitID') ?>

		<?= $form->field($model, 'Overlock') ?>

		<?= $form->field($model, 'WalkOrder') ?>

		<?= $form->field($model, 'UnitOfMeasure') ?>

		<?= $form->field($model, 'Note') ?>

		<?php // echo $form->field($model, 'UnitRating') ?>

		<?php // echo $form->field($model, 'PopupNote') ?>

		<?php // echo $form->field($model, 'RateDaily') ?>

		<?php // echo $form->field($model, 'RateMonthly') ?>

		<?php // echo $form->field($model, 'RateWeekly') ?>

		<?php // echo $form->field($model, 'UnitHeight') ?>

		<?php // echo $form->field($model, 'UnitLength') ?>

		<?php // echo $form->field($model, 'UnitWidth') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

		<?php // echo $form->field($model, 'deleted_at') ?>

		<?php // echo $form->field($model, 'DRateID') ?>

		<?php // echo $form->field($model, 'FeatureCodeID') ?>

		<?php // echo $form->field($model, 'MRateID') ?>

		<?php // echo $form->field($model, 'WRateID') ?>

		<?php // echo $form->field($model, 'SizeCodeID') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
