<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\UnitSelfStor $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => 'Unit Self Stors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-self-stor-create">

    <p class="pull-left">
        <?= Html::a('Cancel', \yii\helpers\Url::previous(), ['class' => 'btn btn-default']) ?>
    </p>
    <div class="clearfix"></div>

    <?php echo $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>
