<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/**
* @var yii\web\View $this
* @var yii\data\ActiveDataProvider $dataProvider
* @var frontend\models\UnitSelfStorSearch $searchModel
*/

$this->title = 'Unit Self Stors';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="unit-self-stor-index">

    <?php //     echo $this->render('_search', ['model' =>$searchModel]);
    ?>

    <div class="clearfix">
        <p class="pull-left">
            <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Unit Self Stor', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <div class="pull-right">


                                                                                
            <?php 
            echo \yii\bootstrap\ButtonDropdown::widget(
                [
                    'id'       => 'giiant-relations',
                    'encodeLabel' => false,
                    'label'    => '<span class="glyphicon glyphicon-paperclip"></span> Relations',
                    'dropdown' => [
                        'options'      => [
                            'class' => 'dropdown-menu-right'
                        ],
                        'encodeLabels' => false,
                        'items'        => [
    [
        'label' => '<i class="glyphicon glyphicon-arrow-left"> Feature Code</i>',
        'url' => [
            'feature-code/index',
        ],
    ],
    [
        'label' => '<i class="glyphicon glyphicon-arrow-left"> Unit</i>',
        'url' => [
            'unit/index',
        ],
    ],
]                    ],
                ]
            );
            ?>        </div>
    </div>

            
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'showPageSummary' => true,
        'hover' => true,
        'floatHeader' => true,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true
        ],
        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="fa fa-archive"></i> Abteile</h3>',
        ],
        'columns' => [
            'unit.UnitNumber',
			'unit.location.Location',
            'unit.StatusNow',
            'UnitHeight:decimal',
            'UnitLength:decimal',
            'UnitWidth:decimal',
            //'UnitOfMeasure',
            //'Overlock',
			//'WalkOrder',
			'Note:ntext',
			//'UnitRating:ntext',
			'PopupNote:ntext',
			'RateDaily:decimal',
			'RateMonthly:decimal',
			'RateWeekly',
			/*'created_at'*/
			/*'updated_at'*/
			/*'deleted_at'*/
			/*'DRateID'*/
			/*'FeatureCodeID'*/
			/*'MRateID'*/
			/*'WRateID'*/
			/*'SizeCodeID'*/
            [
                'class' => 'kartik\grid\ActionColumn',
                'urlCreator' => function($action, $model, $key, $index) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
                    $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                    return \yii\helpers\Url::toRoute($params);
                },
                'contentOptions' => ['nowrap'=>'nowrap']
            ],
        ],
    ]); ?>
    
</div>
