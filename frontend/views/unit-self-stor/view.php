<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var common\models\UnitSelfStor $model
*/

$this->title = 'Unit Self Stor View ' . $model->UnitID . '';
$this->params['breadcrumbs'][] = ['label' => 'Unit Self Stors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->UnitID, 'url' => ['view', 'UnitID' => $model->UnitID]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="unit-self-stor-view">

    </p><div class='clearfix'></div> 
    
    <h3><?= \Yii::t('app','Unit Number'); ?>: <?= $model->unit->UnitNumber ?> </h3>


    <?php $this->beginBlock('common\models\UnitSelfStor'); ?>

        <h1>Allgemein</h1>

    <?php $this->endBlock(); ?>


    
    <?=
    \yii\bootstrap\Tabs::widget(
        [
            'id' => 'relation-tabs',
            'encodeLabels' => false,
            'items' => 
            [ 
                [
                    'label'   => '<i class="fa fa-info"></i> ' . \Yii::t('app','General'),
                    'content' => $this->render('tabIndex/tab1',['model'=>$model]),
                    'active'  => true,
                ],
                [
                    'label'   => '<i class="fa fa-money"></i> ' . \Yii::t('app','Rates'),
                    'content' => $this->render('tabIndex/tab2',['model'=>$model]),
                ],
                [
                    'label'   => '<i class="fa fa-info"></i> ' . \Yii::t('app','Security'),
                    'content' => $this->blocks['common\models\UnitSelfStor']
                ],
                [
                    'label'   => '<i class="fa fa-info"></i> ' . \Yii::t('app','Availability'),
                    'content' => $this->blocks['common\models\UnitSelfStor']
                ],
                [
                    'label'   => '<i class="fa fa-info"></i> ' . \Yii::t('app','History'),
                    'content' => $this->blocks['common\models\UnitSelfStor']
                ],
                [
                    'label'   => '<i class="fa fa-money"></i> ' . \Yii::t('app','Features'),
                    'content' => $this->blocks['common\models\UnitSelfStor']
                ], 
            ]
        ]
    );
    ?>
</div>
