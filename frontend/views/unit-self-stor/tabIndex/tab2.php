<?php
use yii\helpers\Html;
use dosamigos\chartjs\Chart;

/* @var $this yii\web\View */

?>

<div>

<p>&nbsp;</p>

<div class="row">
	<div class="col-md-2">
		<div class="pull-right">
			<i class="fa fa-dashboard fa-4x"></i>
		</div>
	</div>
	<div class="col-md-8">
<?= Chart::widget([
    'type' => 'Line',
    'options' => [
        'height' => 250,
        'width' => 600
    ],
    'data' => [
        'labels' => $model->unit->getRateChartLabels(),
        'datasets' => [
            [
                'fillColor' => "rgba(220,220,220,0.5)",
                'strokeColor' => "rgba(220,220,220,1)",
                'pointColor' => "rgba(220,220,220,1)",
                'pointStrokeColor' => "#fff",
                'data' => $model->unit->getRateChartValues()                
            ]
        ]
    ]
]);
?>
	</div>
</div>

<div class="row">
	<div class="col-md-2">
		<div class="pull-right">
			<i class="fa fa-money fa-4x"></i>
		</div>
	</div>
	<div class="col-md-8">
		<?php
	        echo common\widgets\rate\DisplayRelatedWidget::widget(['model'=>$model]);
	    ?>
	</div>
</div>

</div>
