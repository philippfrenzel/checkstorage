<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

?>

<div>

<p>&nbsp;</p>

<div class="row">
	<div class="col-md-2">
		<div class="pull-right">
			<i class="fa fa-building fa-4x"></i>
		</div>
	</div>
	<div class="col-md-8">
		<?php
	        echo common\widgets\unit\DisplaySingleWidget::widget(['model'=>$model]);
	    ?>
	</div>
</div>

<div class="row">
	<div class="col-md-2">
		<div class="pull-right">
			<i class="fa fa-user fa-4x"></i>
		</div>
	</div>
	<div class="col-md-8">
		<?php
	        echo common\widgets\party\DisplaySingleWidget::widget(['model'=>$model->contract->party]);
	    ?>
	</div>
</div>

</div>
