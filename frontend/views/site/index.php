<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\widgets\SideNav;
use kartik\tabs\TabsX;

/* @var $this yii\web\View */
$this->title .= 'Home';
?>


<?php yii\widgets\Block::begin(array('id'=>'sb-left')); ?>

<h3 style="color:#fff">&nbsp;<i class="fa fa-list"></i> SideMenu</h3>

<div class="well">
  <div class="row">
    <div class="col-md-2">
      <i class="fa fa-user fa-3x"></i>
    </div>
    <div class="col-md-10">
  <address>
    Angemeldet als: <br>
    <strong><?= !\Yii::$app->user->isGuest?strtoupper(\Yii::$app->user->identity->username):''; ?></strong><br>   
  </address>    
    </div>
  </div>
  <div class="row">
    <div class="col-md-2">
      <abbr title="EMail">Mail:</abbr> 
    </div>
    <div class="col-md-10">
      <?= !\Yii::$app->user->isGuest?\Yii::$app->user->identity->email:''; ?>
    </div>
  </div>
</div>  
    
  <?php 

    $sideMenu = array();
    $sideMenu[] = array('label'=>'<i class="fa fa-home"></i> '.Yii::t('app','Startseite'),'url'=>Url::to(array('/site/index')));
    //The side menu for the orga management
    $oeMenu[] = array('label'=>'<i class="fa fa-group"></i> '.Yii::t('app','Units'),'url'=>Url::to(array('/unit/index')));
    $oeMenu[] = array('label'=>'<i class="fa fa-group"></i> '.Yii::t('app','Units Storage'),'url'=>Url::to(array('/unit-self-stor/index')));
    $oeMenu[] = array('label'=>'<i class="fa fa-group"></i> '.Yii::t('app','Partys'),'url'=>Url::to(array('/party/index')));
    $oeMenu[] = array('label'=>'<i class="fa fa-group"></i> '.Yii::t('app','Address'),'url'=>Url::to(array('/address/index')));
    $oeMenu[] = array('label'=>'<i class="fa fa-group"></i> '.Yii::t('app','Contracts'),'url'=>Url::to(array('/contract/index')));
    $sideMenu[] = array('label'=>'<i class="fa fa-sitemap"></i> '.Yii::t('app','CRUDs'),'url'=>Url::to(array('/site/index')),'items'=>$oeMenu);    

    $sideMenu[] = array('label'=>'<i class="fa fa-home"></i> '.Yii::t('app','DEV GII'),'url'=>Url::to(array('/gii')));

    echo SideNav::widget([
      'encodeLabels' => false,
      'type' => SideNav::TYPE_INFO,
      'heading' => '<i class="fa fa-reorder"></i> '.Yii::t('app','Benutzermenu'),
      'items' => $sideMenu
    ]);

    if(!\Yii::$app->user->isGuest){
      //if(\Yii::$app->user->identity->isAdmin()){  
        $adminMenu = array();
        $adminMenu[] = array('label'=>'<i class="fa fa-group"></i> ' . Yii::t('app','Units'),'url'=>Url::to(array('/unit/index')));
        $adminMenu[] = array('label'=>'<i class="fa fa-sitemap"></i> ' . Yii::t('app','Party'),'url'=>Url::to(array('/party/index')));
        $adminMenu[] = array('label'=>'<i class="fa fa-plus"></i> ' . Yii::t('app','Address'),'url'=>Url::to(array('/address/index')));
        
        echo SideNav::widget([
          'encodeLabels' => false,
          'type' => SideNav::TYPE_DANGER,
          'heading' => '<i class="fa fa-reorder"></i> '.Yii::t('app','Administrator'),
          'items' => $adminMenu
        ]);
      //}
    }

  ?>

<?php yii\widgets\Block::end(); ?>

<?php 
  echo TabsX::widget([
      'items' => [
          [
              'label' => '<i class="glyphicon glyphicon-user"></i> <b>Kunden</b><br>KPI <span class="badge">0</span>',
              'content' => $this->render('tabIndex/tab1'),
              'options' => ['id' => 'tabtwo']            
          ],
          [
              'label' => '<i class="glyphicon glyphicon-list"></i> <b>Abteile</b><br>KPI <span class="badge">0</span>',
              'content' => $this->render('tabIndex/tab2'),
              'options' => ['id' => 'tabone']            
          ],
      ],
      'position'=>TabsX::POS_ABOVE,
      'bordered'=>true,
      'encodeLabels'=>false
  ]);
?>
