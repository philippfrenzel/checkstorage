<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var common\models\Address $model
*/

$this->title = 'Address View ' . $model->AddressID . '';
$this->params['breadcrumbs'][] = ['label' => 'Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->AddressID, 'url' => ['view', 'AddressID' => $model->AddressID]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="address-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'AddressID' => $model->AddressID],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Address', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->AddressID ?>    </h3>


    <?php $this->beginBlock('common\models\Address'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    			'AddressID',
			'Code',
			'City',
			'Street',
			'IsPrimary',
			'created_at',
			'updated_at',
			'deleted_at',
			'AddressTypeID',
			'PartyID',
			'RegionID',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'AddressID' => $model->AddressID],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> Address',
    'content' => $this->blocks['common\models\Address'],
    'active'  => true,
], ]
                 ]
    );
    ?></div>
