<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Address $model
 */

$this->title = 'Address Update ' . $model->AddressID . '';
$this->params['breadcrumbs'][] = ['label' => 'Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->AddressID, 'url' => ['view', 'AddressID' => $model->AddressID]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="address-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'AddressID' => $model->AddressID], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
