<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\AddressType $model
 */

$this->title = 'Address Type Update ' . $model->AddressTypeID . '';
$this->params['breadcrumbs'][] = ['label' => 'Address Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->AddressTypeID, 'url' => ['view', 'AddressTypeID' => $model->AddressTypeID]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="address-type-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'AddressTypeID' => $model->AddressTypeID], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
