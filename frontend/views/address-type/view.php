<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var common\models\AddressType $model
*/

$this->title = 'Address Type View ' . $model->AddressTypeID . '';
$this->params['breadcrumbs'][] = ['label' => 'Address Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->AddressTypeID, 'url' => ['view', 'AddressTypeID' => $model->AddressTypeID]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="address-type-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'AddressTypeID' => $model->AddressTypeID],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Address Type', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->AddressTypeID ?>    </h3>


    <?php $this->beginBlock('common\models\AddressType'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    			'AddressTypeID',
			'AddressType:ntext',
			'MailToPriority',
			'UserDefined',
			'created_at',
			'updated_at',
			'deleted_at',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'AddressTypeID' => $model->AddressTypeID],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
<?php $this->beginBlock('Addresses'); ?>
<p class='pull-right'>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-list"></span> List All Addresses',
            ['address/index'],
            ['class'=>'btn text-muted btn-xs']
        ) ?>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-plus"></span> New Address',
            ['address/create', 'Address'=>['AddressTypeID'=>$model->AddressTypeID]],
            ['class'=>'btn btn-success btn-xs']
        ) ?>
</p><div class='clearfix'></div>
<?php $this->endBlock() ?>


    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> AddressType',
    'content' => $this->blocks['common\models\AddressType'],
    'active'  => true,
],[
    'label'   => '<small><span class="glyphicon glyphicon-paperclip"></span> Addresses</small>',
    'content' => $this->blocks['Addresses'],
    'active'  => false,
], ]
                 ]
    );
    ?></div>
