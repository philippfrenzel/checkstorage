<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Contract".
 * @author Philipp Frenzel <philipp@frenzel.net>
 */
class Contract extends \common\models\base\ContractBase
{

	/**
	 * This relation will load the Units from within UnitList and then relates them ot the unit table
	 * @todo this is by now not working correct, as it's still missing to "explode" the relation or change the syntax to be with "in"
     * @return \yii\db\ActiveQuery
     */
    public function getUnits()
    {
        return $this->hasMany(\common\models\Unit::className(), ['UnitNumber' => 'UnitList']);
    }
}
