<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Action".
 */
class Action extends \common\models\base\ActionBase
{

	/** 
     * @todo this logic is super unstable!!! Needs to be fixed as soon as possible!
     * @return \yii\db\ActiveQuery 
     */ 
    public function getUnit() 
    { 
       return $this->hasOne(\common\models\Unit::className(), ['UnitID' => 'UnitID']); 
    }

    /** 
     * @todo this logic is super unstable!!! Needs to be fixed as soon as possible!
     * @return \yii\db\ActiveQuery 
     */ 
    public function getParty() 
    { 
       return $this->hasOne(\common\models\Party::className(), ['PartyID' => 'PartyID']); 
    }

}
