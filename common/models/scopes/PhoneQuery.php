<?php

namespace common\models\scopes;

use Yii;
use yii\db\ActiveQuery;

/**
 * The scopes for the polizzen table
 * @copy Philipp Frenzel <philipp@frenzel.net>
 * @version 0.2
 */

class PhoneQuery extends ActiveQuery
{

    /**
     * return only the none deleted tasks
     * @return [type] [description]
     */
    public function active()
    {
        $this->andWhere(['deleted_at' => NULL]);
        return $this;
    }

    /**
     * adds the filter to the primary address
     * @return [type] [description]
     */
    public function isPrimary()
    {
    	$this->andWhere(['IsPrimary' => 1]);
        return $this;
    }

    /**
     * this returns all the records, wich are related to a defined party
     * @param  INTEGER $PartyID [description]
     * @return [type]          [description]
     */
    public function forParty($PartyID = NULL)
    {
        $this->andWhere(['PartyID' => $PartyID]);
        return $this;
    }

}
