<?php

namespace common\models\scopes;

use Yii;
use yii\db\ActiveQuery;

/**
 * The scopes for the polizzen table
 * @copy Philipp Frenzel <philipp@frenzel.net>
 * @version 0.2
 */

class UnitRateHistoryQuery extends ActiveQuery
{

    /**
     * return only the none deleted tasks
     * @return [type] [description]
     */
    public function active()
    {
        $this->andWhere(['deleted_at' => NULL]);
        return $this;
    }

    /**
     * this returns all the records, wich are related to a defined party
     * @param  INTEGER $UnitID [description]
     * @return [type]          [description]
     */
    public function forUnit($UnitID = NULL)
    {
        $this->andWhere(['{{%Unit}}.UnitID' => $UnitID]);
        return $this;
    }

}
