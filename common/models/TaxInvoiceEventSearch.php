<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TaxInvoiceEvent;

/**
 * TaxInvoiceEventSearch represents the model behind the search form about TaxInvoiceEvent.
 */
class TaxInvoiceEventSearch extends Model
{
	public $InvoiceID;
	public $EventID;
	public $Description;
	public $Unit;
	public $Amount;
	public $VAT;
	public $DueDate;
	public $VATCode;
	public $ID	int	NO;
	public $GLID;
	public $TaxGroupID;
	public $JournalDate;
	public $EventParam;
	public $UserPartyID;
	public $ActionID;
	public $UnitID;
	public $PaymentEventID;
	public $fReversed;
	public $created_at;
	public $updated_at;
	public $deleted_at;

	public function rules()
	{
		return [
			[['InvoiceID', 'EventID', 'ID	int	NO', 'GLID', 'TaxGroupID', 'UserPartyID', 'ActionID', 'UnitID', 'PaymentEventID', 'fReversed', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
			[['Description', 'Unit', 'DueDate', 'VATCode', 'JournalDate', 'EventParam'], 'safe'],
			[['Amount', 'VAT'], 'number'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'InvoiceID' => 'Invoice ID',
			'EventID' => 'Event ID',
			'Description' => 'Description',
			'Unit' => 'Unit',
			'Amount' => 'Amount',
			'VAT' => 'Vat',
			'DueDate' => 'Due Date',
			'VATCode' => 'Vatcode',
			'ID	int	NO' => 'Id	Int	 No',
			'GLID' => 'Glid',
			'TaxGroupID' => 'Tax Group ID',
			'JournalDate' => 'Journal Date',
			'EventParam' => 'Event Param',
			'UserPartyID' => 'User Party ID',
			'ActionID' => 'Action ID',
			'UnitID' => 'Unit ID',
			'PaymentEventID' => 'Payment Event ID',
			'fReversed' => 'F Reversed',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
		];
	}

	public function search($params)
	{
		$query = TaxInvoiceEvent::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'InvoiceID' => $this->InvoiceID,
            'EventID' => $this->EventID,
            'Amount' => $this->Amount,
            'VAT' => $this->VAT,
            'DueDate' => $this->DueDate,
            'ID	int	NO' => $this->ID	int	NO,
            'GLID' => $this->GLID,
            'TaxGroupID' => $this->TaxGroupID,
            'JournalDate' => $this->JournalDate,
            'UserPartyID' => $this->UserPartyID,
            'ActionID' => $this->ActionID,
            'UnitID' => $this->UnitID,
            'PaymentEventID' => $this->PaymentEventID,
            'fReversed' => $this->fReversed,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

		$query->andFilterWhere(['like', 'Description', $this->Description])
            ->andFilterWhere(['like', 'Unit', $this->Unit])
            ->andFilterWhere(['like', 'VATCode', $this->VATCode])
            ->andFilterWhere(['like', 'EventParam', $this->EventParam]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
