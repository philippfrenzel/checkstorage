<?php

namespace common\models;

use Yii;
use common\models\scopes\PartyQuery;

/**
 * This is the model class for table "Party".
 */
class Party extends \common\models\base\PartyBase
{
	/**
     * @inheritdoc
     * @return PolizzenQuery
     */
    public static function find()
    {
        return new PartyQuery(get_called_class());
    }

	/** 
     * @todo this logic is super unstable!!! Needs to be fixed as soon as possible!
     * @return \yii\db\ActiveQuery 
     */ 
    public function getContract() 
    { 
       return $this->hasOne(\common\models\Contract::className(), ['PartyID' => 'PartyID' , 'UnitList' => 'UnitList']); 
    }

    /**
     * should return the main address only
     * @return [type] [description]
     */
    public function getAddress()
    {
    	if(count($this->addresses) > 0)
    	{
	    	foreach ($this->addresses AS $address)
	    	{
	    		if($address['IsPrimary'])
	    		{
	    			return $address;
	    		}
	    	}
	    }
    	return NULL;
    }

}
