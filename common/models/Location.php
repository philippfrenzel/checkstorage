<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Location".
 */
class Location extends \common\models\base\LocationBase
{
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

	/**
     * Returns all possible lists to choose from as an associative array
     *
     * @return array The array of lists
     */
    public static function getListOptions()
    {
        $returnme = array();
        $returnme = ['0'=>'NONE AVAILABLE! Gibts net!'];
        return ArrayHelper::merge(ArrayHelper::map(self::findBySQL('SELECT DISTINCT LocationID, Location FROM {{%Location}} ORDER BY Location ASC')->all(),'LocationID','Location'),$returnme);
    }

}
