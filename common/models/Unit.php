<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Unit".
 * @author Philipp Frenzel <philipp@frenzel.net>
 */
class Unit extends \common\models\base\UnitBase
{

	/**
	 * this will build the labels for the price curve
	 * @return [type] [description]
	 */
	public function getRateChartLabels()
	{
		$labels = [];
		$rates = $this->unitRateHistories;
    	foreach($rates AS $rate)
    	{
    		$labels[] = substr($rate->Start,0,11);
    	}
		return $labels;
	}

	public function getRateChartValues()
	{
		$values = [];
		$rates = $this->unitRateHistories;
    	foreach($rates AS $rate)
    	{
    		$values[] = $rate->rate->PassThru;
    	}
		return $values;
	}

}
