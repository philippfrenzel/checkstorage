<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TaxInvoice;

/**
 * TaxInvoiceSearch represents the model behind the search form about TaxInvoice.
 */
class TaxInvoiceSearch extends Model
{
	public $InvoiceID;
	public $InvoiceNumber;
	public $InvoiceDate;
	public $DueDate;
	public $PartyID;
	public $ContractID;
	public $Note;
	public $Comment;
	public $Param;
	public $Export;
	public $InvoiceNumeric;
	public $created_at;
	public $updated_at;
	public $deleted_at;

	public function rules()
	{
		return [
			[['InvoiceID', 'PartyID', 'ContractID', 'Export', 'InvoiceNumeric', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
			[['InvoiceNumber', 'InvoiceDate', 'DueDate', 'Note', 'Comment', 'Param'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'InvoiceID' => 'Invoice ID',
			'InvoiceNumber' => 'Invoice Number',
			'InvoiceDate' => 'Invoice Date',
			'DueDate' => 'Due Date',
			'PartyID' => 'Party ID',
			'ContractID' => 'Contract ID',
			'Note' => 'Note',
			'Comment' => 'Comment',
			'Param' => 'Param',
			'Export' => 'Export',
			'InvoiceNumeric' => 'Invoice Numeric',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
		];
	}

	public function search($params)
	{
		$query = TaxInvoice::find();

		$query->andFilterWhere([
            'PartyID' => $this->PartyID
        ]);

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'InvoiceID' => $this->InvoiceID,
            'InvoiceDate' => $this->InvoiceDate,
            'DueDate' => $this->DueDate,
            'ContractID' => $this->ContractID,
            'Export' => $this->Export,
            'InvoiceNumeric' => $this->InvoiceNumeric,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

		$query->andFilterWhere(['like', 'InvoiceNumber', $this->InvoiceNumber])
            ->andFilterWhere(['like', 'Note', $this->Note])
            ->andFilterWhere(['like', 'Comment', $this->Comment])
            ->andFilterWhere(['like', 'Param', $this->Param]);

		return $dataProvider;
	}

	public function searchRelated($params)
	{
		$query = TaxInvoice::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'InvoiceID' => $this->InvoiceID,
            'InvoiceDate' => $this->InvoiceDate,
            'DueDate' => $this->DueDate,
            'PartyID' => $this->PartyID,
            'ContractID' => $this->ContractID,
            'Export' => $this->Export,
            'InvoiceNumeric' => $this->InvoiceNumeric,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

		$query->andFilterWhere(['like', 'InvoiceNumber', $this->InvoiceNumber])
            ->andFilterWhere(['like', 'Note', $this->Note])
            ->andFilterWhere(['like', 'Comment', $this->Comment])
            ->andFilterWhere(['like', 'Param', $this->Param]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
