<?php

namespace common\models;

use Yii;

use common\models\scopes\UnitRateHistoryQuery;

/**
 * This is the model class for table "UnitRateHistory".
 * @author Philipp Frenzel <philipp@frenzel.net>
 */
class UnitRateHistory extends \common\models\base\UnitRateHistoryBase
{

	/**
     * @inheritdoc
     * @return UnitRateHistoryQuery
     */
    public static function find()
    {
        return new UnitRateHistoryQuery(get_called_class());
    }

}
