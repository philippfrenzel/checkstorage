<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "Occupation".
 *
 * @property integer $OccupationID
 * @property string $Occupation
 * @property integer $UserDefined
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property Party[] $parties
 */
class OccupationBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Occupation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Occupation'], 'string'],
            [['UserDefined', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['created_at', 'updated_at'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'OccupationID' => 'Occupation ID',
            'Occupation' => 'Occupation',
            'UserDefined' => 'User Defined',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParties()
    {
        return $this->hasMany(\common\models\Party::className(), ['OccupationID' => 'OccupationID']);
    }
}
