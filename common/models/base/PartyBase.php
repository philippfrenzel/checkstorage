<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "Party".
 *
 * @property integer $PartyID
 * @property string $DLData
 * @property resource $DLNumber
 * @property resource $ID1
 * @property resource $ID2
 * @property integer $UnitSortOrder
 * @property integer $GrpActRef
 * @property string $DLRegionAbbr
 * @property string $Identification2
 * @property string $Identification1
 * @property string $PartyName
 * @property string $Soundex
 * @property string $Alert
 * @property string $PartyParam
 * @property string $UnitList
 * @property string $Note
 * @property string $Created
 * @property string $LockTime
 * @property integer $LockUser
 * @property string $PTD
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property integer $PartyTypeID
 * @property integer $OccupationID
 *
 * @property Address[] $addresses
 * @property Contract[] $contracts
 * @property Occupation $occupation
 * @property PartyType $partyType
 */
class PartyBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Party';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DLNumber', 'ID1', 'ID2', 'Alert', 'Note'], 'string'],
            [['UnitSortOrder', 'GrpActRef', 'LockUser', 'created_at', 'updated_at', 'deleted_at', 'PartyTypeID', 'OccupationID'], 'integer'],
            [['Created', 'LockTime', 'PTD'], 'safe'],
            [['created_at', 'updated_at'], 'required'],
            [['DLData', 'DLRegionAbbr', 'Identification2', 'Identification1', 'PartyName', 'Soundex', 'UnitList'], 'string', 'max' => 255],
            [['PartyParam'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'PartyID' => Yii::t('app', 'Party ID'),
            'DLData' => Yii::t('app', 'Dldata'),
            'DLNumber' => Yii::t('app', 'Dlnumber'),
            'ID1' => Yii::t('app', 'Id1'),
            'ID2' => Yii::t('app', 'Id2'),
            'UnitSortOrder' => Yii::t('app', 'Unit Sort Order'),
            'GrpActRef' => Yii::t('app', 'Grp Act Ref'),
            'DLRegionAbbr' => Yii::t('app', 'Dlregion Abbr'),
            'Identification2' => Yii::t('app', 'Identification2'),
            'Identification1' => Yii::t('app', 'Identification1'),
            'PartyName' => Yii::t('app', 'Party Name'),
            'Soundex' => Yii::t('app', 'Soundex'),
            'Alert' => Yii::t('app', 'Alert'),
            'PartyParam' => Yii::t('app', 'Party Param'),
            'UnitList' => Yii::t('app', 'Unit List'),
            'Note' => Yii::t('app', 'Note'),
            'Created' => Yii::t('app', 'Created'),
            'LockTime' => Yii::t('app', 'Lock Time'),
            'LockUser' => Yii::t('app', 'Lock User'),
            'PTD' => Yii::t('app', 'Ptd'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'PartyTypeID' => Yii::t('app', 'Party Type ID'),
            'OccupationID' => Yii::t('app', 'Occupation ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(\common\models\Address::className(), ['PartyID' => 'PartyID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOccupation()
    {
        return $this->hasOne(\common\models\Occupation::className(), ['OccupationID' => 'OccupationID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartyType()
    {
        return $this->hasOne(\common\models\PartyType::className(), ['PartyTypeID' => 'PartyTypeID']);
    }

    /** 
    * @return \yii\db\ActiveQuery 
    */ 
    public function getContracts() 
    { 
       return $this->hasMany(\common\models\Contract::className(), ['PartyID' => 'PartyID']); 
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getPhones()
    {
        return $this->hasMany(\common\models\Phone::className(), ['PartyID' => 'PartyID']);
    } 

}
