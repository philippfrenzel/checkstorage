<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "UnitRateHistory".
 *
 * @property integer $UnitID
 * @property integer $RateID
 * @property string $Start
 * @property string $Stop
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property Unit $unit
 * @property Rate $rate
 */
class UnitRateHistoryBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%UnitRateHistory}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UnitID', 'RateID', 'Start', 'created_at', 'updated_at'], 'required'],
            [['UnitID', 'RateID', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['Start', 'Stop'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'UnitID' => Yii::t('app', 'Unit ID'),
            'RateID' => Yii::t('app', 'Rate ID'),
            'Start' => Yii::t('app', 'Start'),
            'Stop' => Yii::t('app', 'Stop'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * gets the related Unit
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(\common\models\Unit::className(), ['UnitID' => 'UnitID']);
    }

    /**
     * gets the related Rate
     * @return \yii\db\ActiveQuery
     */
    public function getRate()
    {
        return $this->hasOne(\common\models\Rate::className(), ['RateID' => 'RateID']);
    }
}
