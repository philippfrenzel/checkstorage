<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "Contract".
 *
 * @property integer $ContractID
 * @property string $CycleLength
 * @property integer $UnitSortOrder
 * @property string $ContractNumber
 * @property string $Param
 * @property string $PTSizeNeeded
 * @property string $UnitList
 * @property string $PTReason
 * @property string $AmtDue
 * @property string $Balance
 * @property string $Deposit
 * @property string $Effective
 * @property integer $IsInProcessor
 * @property string $LastRebuildPrepay
 * @property double $MarketDistance
 * @property string $MinimumDuration
 * @property string $OpenCredits
 * @property string $Pau
 * @property string $PTD
 * @property string $PTDateMovein
 * @property string $PTDateNeeded
 * @property string $Rent
 * @property string $UnitIns
 * @property string $UnitOther
 * @property string $UnitRent
 * @property string $UnitTax
 * @property string $VacateDate
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property integer $UserID
 * @property integer $PartyID
 * @property integer $MarketReasonID
 * @property integer $MarketSourceID
 * @property integer $ContractTypeID
 *
 * @property ContractType $contractType
 * @property MarketReason $marketReason
 * @property MarketSource $marketSource
 * @property Party $party
 */
class ContractBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Contract';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UnitSortOrder', 'IsInProcessor', 'created_at', 'updated_at', 'deleted_at', 'UserID', 'PartyID', 'MarketReasonID', 'MarketSourceID', 'ContractTypeID'], 'integer'],
            [['UnitList', 'PTReason'], 'string'],
            [['AmtDue', 'Balance', 'Deposit', 'MarketDistance', 'OpenCredits', 'Rent', 'UnitIns', 'UnitOther', 'UnitRent', 'UnitTax'], 'number'],
            [['Effective', 'LastRebuildPrepay', 'MinimumDuration', 'Pau', 'PTD', 'PTDateMovein', 'PTDateNeeded', 'VacateDate'], 'safe'],
            [['created_at', 'updated_at', 'PartyID'], 'required'],
            [['CycleLength'], 'string', 'max' => 5],
            [['ContractNumber'], 'string', 'max' => 50],
            [['Param', 'PTSizeNeeded'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ContractID' => Yii::t('app', 'Contract ID'),
            'CycleLength' => Yii::t('app', 'Cycle Length'),
            'UnitSortOrder' => Yii::t('app', 'Unit Sort Order'),
            'ContractNumber' => Yii::t('app', 'Contract Number'),
            'Param' => Yii::t('app', 'Param'),
            'PTSizeNeeded' => Yii::t('app', 'Ptsize Needed'),
            'UnitList' => Yii::t('app', 'Unit List'),
            'PTReason' => Yii::t('app', 'Ptreason'),
            'AmtDue' => Yii::t('app', 'Amt Due'),
            'Balance' => Yii::t('app', 'Balance'),
            'Deposit' => Yii::t('app', 'Deposit'),
            'Effective' => Yii::t('app', 'Effective'),
            'IsInProcessor' => Yii::t('app', 'Is In Processor'),
            'LastRebuildPrepay' => Yii::t('app', 'Last Rebuild Prepay'),
            'MarketDistance' => Yii::t('app', 'Market Distance'),
            'MinimumDuration' => Yii::t('app', 'Minimum Duration'),
            'OpenCredits' => Yii::t('app', 'Open Credits'),
            'Pau' => Yii::t('app', 'Pau'),
            'PTD' => Yii::t('app', 'Ptd'),
            'PTDateMovein' => Yii::t('app', 'Ptdate Movein'),
            'PTDateNeeded' => Yii::t('app', 'Ptdate Needed'),
            'Rent' => Yii::t('app', 'Rent'),
            'UnitIns' => Yii::t('app', 'Unit Ins'),
            'UnitOther' => Yii::t('app', 'Unit Other'),
            'UnitRent' => Yii::t('app', 'Unit Rent'),
            'UnitTax' => Yii::t('app', 'Unit Tax'),
            'VacateDate' => Yii::t('app', 'Vacate Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'UserID' => Yii::t('app', 'User ID'),
            'PartyID' => Yii::t('app', 'Party ID'),
            'MarketReasonID' => Yii::t('app', 'Market Reason ID'),
            'MarketSourceID' => Yii::t('app', 'Market Source ID'),
            'ContractTypeID' => Yii::t('app', 'Contract Type ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractType()
    {
        return $this->hasOne(\common\models\ContractType::className(), ['ContractTypeID' => 'ContractTypeID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarketReason()
    {
        return $this->hasOne(\common\models\MarketReason::className(), ['MarketReasonID' => 'MarketReasonID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarketSource()
    {
        return $this->hasOne(\common\models\MarketSource::className(), ['MarketSourceID' => 'MarketSourceID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParty()
    {
        return $this->hasOne(\common\models\Party::className(), ['PartyID' => 'PartyID']);
    }
}
