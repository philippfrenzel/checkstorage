<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "TaxInvoice".
 *
 * @property integer $InvoiceID
 * @property string $InvoiceNumber
 * @property string $InvoiceDate
 * @property string $DueDate
 * @property integer $PartyID
 * @property integer $ContractID
 * @property string $Note
 * @property string $Comment
 * @property string $Param
 * @property integer $Export
 * @property integer $InvoiceNumeric
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property TaxInvoiceEvent[] $taxInvoiceEvents
 */
class TaxInvoiceBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TaxInvoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['InvoiceDate', 'DueDate'], 'safe'],
            [['PartyID', 'ContractID', 'Export', 'InvoiceNumeric', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['InvoiceNumeric', 'created_at', 'updated_at'], 'required'],
            [['InvoiceNumber'], 'string', 'max' => 50],
            [['Note', 'Comment', 'Param'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'InvoiceID' => Yii::t('app', 'Invoice ID'),
            'InvoiceNumber' => Yii::t('app', 'Invoice Number'),
            'InvoiceDate' => Yii::t('app', 'Invoice Date'),
            'DueDate' => Yii::t('app', 'Due Date'),
            'PartyID' => Yii::t('app', 'Party ID'),
            'ContractID' => Yii::t('app', 'Contract ID'),
            'Note' => Yii::t('app', 'Note'),
            'Comment' => Yii::t('app', 'Comment'),
            'Param' => Yii::t('app', 'Param'),
            'Export' => Yii::t('app', 'Export'),
            'InvoiceNumeric' => Yii::t('app', 'Invoice Numeric'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxInvoiceEvents()
    {
        return $this->hasMany(\common\models\TaxInvoiceEvent::className(), ['InvoiceID' => 'InvoiceID']);
    }
}
