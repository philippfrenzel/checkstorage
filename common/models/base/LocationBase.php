<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "Location".
 *
 * @property integer $LocationID
 * @property string $Location
 * @property integer $SortOrder
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property integer $ParentID
 * @property integer $LocationTypeID
 *
 * @property Location $parent
 * @property Location[] $locations
 * @property Unit[] $units
 */
class LocationBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%Location}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SortOrder', 'created_at', 'updated_at', 'deleted_at', 'ParentID', 'LocationTypeID'], 'integer'],
            [['created_at', 'updated_at', 'LocationTypeID'], 'required'],
            [['Location'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'LocationID' => Yii::t('app', 'Location ID'),
            'Location' => Yii::t('app', 'Location'),
            'SortOrder' => Yii::t('app', 'Sort Order'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'ParentID' => Yii::t('app', 'Parent ID'),
            'LocationTypeID' => Yii::t('app', 'Location Type ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(\common\models\Location::className(), ['LocationID' => 'ParentID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocations()
    {
        return $this->hasMany(\common\models\Location::className(), ['ParentID' => 'LocationID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnits()
    {
        return $this->hasMany(\common\models\Unit::className(), ['LocationID' => 'LocationID']);
    }
}
