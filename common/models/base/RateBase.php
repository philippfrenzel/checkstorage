<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "Rate".
 *
 * @property integer $RateID
 * @property string $UnitOfMeasure
 * @property integer $CR
 * @property integer $DR
 * @property integer $PassThru
 * @property integer $Quantity
 * @property integer $RateTypeID
 * @property integer $TaxGroupID
 * @property double $UnitPrice
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 */
class RateBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Rate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CR', 'DR', 'PassThru', 'Quantity', 'RateTypeID', 'TaxGroupID', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['UnitPrice'], 'number'],
            [['created_at', 'updated_at'], 'required'],
            [['UnitOfMeasure'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'RateID' => Yii::t('app', 'Rate ID'),
            'UnitOfMeasure' => Yii::t('app', 'Unit Of Measure'),
            'CR' => Yii::t('app', 'Cr'),
            'DR' => Yii::t('app', 'Dr'),
            'PassThru' => Yii::t('app', 'Pass Thru'),
            'Quantity' => Yii::t('app', 'Quantity'),
            'RateTypeID' => Yii::t('app', 'Rate Type ID'),
            'TaxGroupID' => Yii::t('app', 'Tax Group ID'),
            'UnitPrice' => Yii::t('app', 'Unit Price'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }
}
