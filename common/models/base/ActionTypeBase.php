<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "ActionType".
 *
 * @property integer $ActionTypeID
 * @property string $ActionType
 * @property string $ActionGroup
 * @property string $DebitCredit
 * @property string $AR
 * @property string $AP
 * @property integer $OrderOfAction
 * @property string $NoteShort
 * @property string $Note
 * @property string $TechNote
 * @property integer $Color
 * @property string $Icon
 * @property string $DefaultBaseDate
 * @property integer $UnitRateBased
 * @property integer $SaveAction
 * @property integer $CanPreprocess
 * @property integer $CanDeposit
 * @property string $Associate
 * @property string $ParamTemplate
 * @property integer $InventoryFactor
 * @property string $RefParam
 * @property string $UnitStatus
 * @property integer $HasContractID
 * @property string $Per
 * @property integer $Invoice
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property Action[] $actions
 */
class ActionTypeBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ActionType';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['OrderOfAction', 'Color', 'UnitRateBased', 'SaveAction', 'CanPreprocess', 'CanDeposit', 'InventoryFactor', 'HasContractID', 'Invoice', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
            [['ActionType', 'ActionGroup', 'NoteShort', 'Icon', 'DefaultBaseDate', 'Associate', 'RefParam', 'Per'], 'string', 'max' => 50],
            [['DebitCredit', 'AR', 'AP', 'UnitStatus'], 'string', 'max' => 2],
            [['Note', 'TechNote', 'ParamTemplate'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ActionTypeID' => Yii::t('app', 'Action Type ID'),
            'ActionType' => Yii::t('app', 'Action Type'),
            'ActionGroup' => Yii::t('app', 'Action Group'),
            'DebitCredit' => Yii::t('app', 'Debit Credit'),
            'AR' => Yii::t('app', 'Ar'),
            'AP' => Yii::t('app', 'Ap'),
            'OrderOfAction' => Yii::t('app', 'Order Of Action'),
            'NoteShort' => Yii::t('app', 'Note Short'),
            'Note' => Yii::t('app', 'Note'),
            'TechNote' => Yii::t('app', 'Tech Note'),
            'Color' => Yii::t('app', 'Color'),
            'Icon' => Yii::t('app', 'Icon'),
            'DefaultBaseDate' => Yii::t('app', 'Default Base Date'),
            'UnitRateBased' => Yii::t('app', 'Unit Rate Based'),
            'SaveAction' => Yii::t('app', 'Save Action'),
            'CanPreprocess' => Yii::t('app', 'Can Preprocess'),
            'CanDeposit' => Yii::t('app', 'Can Deposit'),
            'Associate' => Yii::t('app', 'Associate'),
            'ParamTemplate' => Yii::t('app', 'Param Template'),
            'InventoryFactor' => Yii::t('app', 'Inventory Factor'),
            'RefParam' => Yii::t('app', 'Ref Param'),
            'UnitStatus' => Yii::t('app', 'Unit Status'),
            'HasContractID' => Yii::t('app', 'Has Contract ID'),
            'Per' => Yii::t('app', 'Per'),
            'Invoice' => Yii::t('app', 'Invoice'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActions()
    {
        return $this->hasMany(\common\models\Action::className(), ['ActionTypeID' => 'ActionTypeID']);
    }
}
