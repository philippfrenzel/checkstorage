<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "UnitSelfStor".
 *
 * @property integer $UnitID
 * @property string $Overlock
 * @property string $WalkOrder
 * @property string $UnitOfMeasure
 * @property string $Note
 * @property string $UnitRating
 * @property string $PopupNote
 * @property double $RateDaily
 * @property double $RateMonthly
 * @property double $RateWeekly
 * @property double $UnitHeight
 * @property double $UnitLength
 * @property double $UnitWidth
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property integer $DRateID
 * @property integer $FeatureCodeID
 * @property integer $MRateID
 * @property integer $WRateID
 * @property integer $SizeCodeID
 *
 * @property FeatureCode $featureCode
 * @property Unit $unit
 */
class UnitSelfStorBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UnitSelfStor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Note', 'UnitRating', 'PopupNote'], 'string'],
            [['RateDaily', 'RateMonthly', 'RateWeekly', 'UnitHeight', 'UnitLength', 'UnitWidth'], 'number'],
            [['created_at', 'updated_at', 'DRateID', 'FeatureCodeID', 'MRateID', 'WRateID', 'SizeCodeID'], 'required'],
            [['created_at', 'updated_at', 'deleted_at', 'DRateID', 'FeatureCodeID', 'MRateID', 'WRateID', 'SizeCodeID'], 'integer'],
            [['Overlock', 'WalkOrder', 'UnitOfMeasure'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'UnitID' => Yii::t('app', 'Unit ID'),
            'Overlock' => Yii::t('app', 'Overlock'),
            'WalkOrder' => Yii::t('app', 'Walk Order'),
            'UnitOfMeasure' => Yii::t('app', 'Unit Of Measure'),
            'Note' => Yii::t('app', 'Note'),
            'UnitRating' => Yii::t('app', 'Unit Rating'),
            'PopupNote' => Yii::t('app', 'Popup Note'),
            'RateDaily' => Yii::t('app', 'Rate Daily'),
            'RateMonthly' => Yii::t('app', 'Rate Monthly'),
            'RateWeekly' => Yii::t('app', 'Rate Weekly'),
            'UnitHeight' => Yii::t('app', 'Unit Height'),
            'UnitLength' => Yii::t('app', 'Unit Length'),
            'UnitWidth' => Yii::t('app', 'Unit Width'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'DRateID' => Yii::t('app', 'Drate ID'),
            'FeatureCodeID' => Yii::t('app', 'Feature Code ID'),
            'MRateID' => Yii::t('app', 'Mrate ID'),
            'WRateID' => Yii::t('app', 'Wrate ID'),
            'SizeCodeID' => Yii::t('app', 'Size Code ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeatureCode()
    {
        return $this->hasOne(\common\models\FeatureCode::className(), ['FeatureCodeID' => 'FeatureCodeID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(\common\models\Unit::className(), ['UnitID' => 'UnitID']);
    }
}
