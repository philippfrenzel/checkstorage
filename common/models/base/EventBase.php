<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "Event".
 *
 * @property integer $EventID
 * @property integer $ActionID
 * @property string $Param
 * @property integer $UserPartyID
 * @property string $Created
 * @property string $JournalDate
 * @property double $EAmount
 * @property double $ETax
 * @property string $ESnapshot
 * @property integer $CID
 * @property string $datFrom
 * @property string $datTo
 * @property string $Description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property Action $action
 */
class EventBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ActionID', 'created_at', 'updated_at'], 'required'],
            [['ActionID', 'UserPartyID', 'CID', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['Created', 'JournalDate', 'datFrom', 'datTo'], 'safe'],
            [['EAmount', 'ETax'], 'number'],
            [['Param'], 'string', 'max' => 1000],
            [['ESnapshot', 'Description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'EventID' => Yii::t('app', 'Event ID'),
            'ActionID' => Yii::t('app', 'Action ID'),
            'Param' => Yii::t('app', 'Param'),
            'UserPartyID' => Yii::t('app', 'User Party ID'),
            'Created' => Yii::t('app', 'Created'),
            'JournalDate' => Yii::t('app', 'Journal Date'),
            'EAmount' => Yii::t('app', 'Eamount'),
            'ETax' => Yii::t('app', 'Etax'),
            'ESnapshot' => Yii::t('app', 'Esnapshot'),
            'CID' => Yii::t('app', 'Cid'),
            'datFrom' => Yii::t('app', 'Dat From'),
            'datTo' => Yii::t('app', 'Dat To'),
            'Description' => Yii::t('app', 'Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(\common\models\Action::className(), ['ActionID' => 'ActionID']);
    }
}
