<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "MarketReason".
 *
 * @property integer $MarketReasonID
 * @property string $MarketReason
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property Contract[] $contracts
 */
class MarketReasonBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MarketReason';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['MarketReason'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'MarketReasonID' => Yii::t('app', 'Market Reason ID'),
            'MarketReason' => Yii::t('app', 'Market Reason'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContracts()
    {
        return $this->hasMany(\common\models\Contract::className(), ['MarketReasonID' => 'MarketReasonID']);
    }
}
