<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "AddressType".
 *
 * @property integer $AddressTypeID
 * @property string $AddressType
 * @property integer $MailToPriority
 * @property integer $UserDefined
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property Address[] $addresses
 */
class AddressTypeBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AddressType';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['AddressType'], 'string'],
            [['MailToPriority', 'UserDefined', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['created_at', 'updated_at'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'AddressTypeID' => 'Address Type ID',
            'AddressType' => 'Address Type',
            'MailToPriority' => 'Mail To Priority',
            'UserDefined' => 'User Defined',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(\common\models\Address::className(), ['AddressTypeID' => 'AddressTypeID']);
    }
}
