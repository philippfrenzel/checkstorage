<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "Unit".
 *
 * @property integer $UnitID
 * @property string $Unit
 * @property string $StatusNow
 * @property integer $SortOrder
 * @property string $UnitNumber
 * @property string $CatalogSet
 * @property string $CatalogDesc
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property integer $SiteID
 * @property integer $LocationID
 * @property integer $OffSiteAddressID
 * @property integer $UnitTypeID
 *
 * @property Location $location
 * @property UnitType $unitType
 * @property UnitRateHistory[] $unitRateHistories
 * @property UnitSelfStor $unitSelfStor
 */
class UnitBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%Unit}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['StatusNow', 'CatalogSet', 'CatalogDesc'], 'string'],
            [['SortOrder', 'created_at', 'updated_at', 'deleted_at', 'SiteID', 'LocationID', 'OffSiteAddressID', 'UnitTypeID'], 'integer'],
            [['created_at', 'updated_at', 'SiteID', 'LocationID', 'OffSiteAddressID', 'UnitTypeID'], 'required'],
            [['Unit', 'UnitNumber'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'UnitID' => Yii::t('app', 'Unit ID'),
            'Unit' => Yii::t('app', 'Unit'),
            'StatusNow' => Yii::t('app', 'Status Now'),
            'SortOrder' => Yii::t('app', 'Sort Order'),
            'UnitNumber' => Yii::t('app', 'Unit Number'),
            'CatalogSet' => Yii::t('app', 'Catalog Set'),
            'CatalogDesc' => Yii::t('app', 'Catalog Desc'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'SiteID' => Yii::t('app', 'Site ID'),
            'LocationID' => Yii::t('app', 'Location ID'),
            'OffSiteAddressID' => Yii::t('app', 'Off Site Address ID'),
            'UnitTypeID' => Yii::t('app', 'Unit Type ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(\common\models\Location::className(), ['LocationID' => 'LocationID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitType()
    {
        return $this->hasOne(\common\models\UnitType::className(), ['UnitTypeID' => 'UnitTypeID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitRateHistories()
    {
        return $this->hasMany(\common\models\UnitRateHistory::className(), ['UnitID' => 'UnitID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitSelfStor()
    {
        return $this->hasOne(\common\models\UnitSelfStor::className(), ['UnitID' => 'UnitID']);
    }
}
