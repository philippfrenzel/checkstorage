<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "MarketSource".
 *
 * @property integer $MarketSourceID
 * @property string $MarketSource
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property Contract[] $contracts
 */
class MarketSourceBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MarketSource';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['MarketSource'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'MarketSourceID' => Yii::t('app', 'Market Source ID'),
            'MarketSource' => Yii::t('app', 'Market Source'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContracts()
    {
        return $this->hasMany(\common\models\Contract::className(), ['MarketSourceID' => 'MarketSourceID']);
    }
}
