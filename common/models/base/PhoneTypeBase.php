<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "PhoneType".
 *
 * @property integer $PhoneTypeID
 * @property string $PhoneType
 * @property integer $UserDefined
 * @property string $Icon
 * @property integer $PatternMatching
 * @property string $ConnectWord
 * @property integer $Significance
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property Phone[] $phones
 */
class PhoneTypeBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PhoneType';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UserDefined', 'PatternMatching', 'Significance', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
            [['PhoneType', 'Icon', 'ConnectWord'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'PhoneTypeID' => Yii::t('app', 'Phone Type ID'),
            'PhoneType' => Yii::t('app', 'Phone Type'),
            'UserDefined' => Yii::t('app', 'User Defined'),
            'Icon' => Yii::t('app', 'Icon'),
            'PatternMatching' => Yii::t('app', 'Pattern Matching'),
            'ConnectWord' => Yii::t('app', 'Connect Word'),
            'Significance' => Yii::t('app', 'Significance'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhones()
    {
        return $this->hasMany(\common\models\Phone::className(), ['PhoneTypeID' => 'PhoneTypeID']);
    }
}
