<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "FeatureCode".
 *
 * @property integer $FeatureCodeID
 * @property string $FeatureCodeDesc
 * @property double $PriceDeviation
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property UnitSelfStor[] $unitSelfStors
 */
class FeatureCodeBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'FeatureCode';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FeatureCodeDesc'], 'string'],
            [['PriceDeviation'], 'number'],
            [['created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'FeatureCodeID' => Yii::t('app', 'Feature Code ID'),
            'FeatureCodeDesc' => Yii::t('app', 'Feature Code Desc'),
            'PriceDeviation' => Yii::t('app', 'Price Deviation'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitSelfStors()
    {
        return $this->hasMany(\common\models\UnitSelfStor::className(), ['FeatureCodeID' => 'FeatureCodeID']);
    }
}
