<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "TaxInvoiceEvent".
 *
 * @property integer $InvoiceID
 * @property integer $EventID
 * @property string $Description
 * @property string $Unit
 * @property double $Amount
 * @property double $VAT
 * @property string $DueDate
 * @property string $VATCode
 * @property integer $ID	int	NO
 * @property integer $GLID
 * @property integer $TaxGroupID
 * @property string $JournalDate
 * @property string $EventParam
 * @property integer $UserPartyID
 * @property integer $ActionID
 * @property integer $UnitID
 * @property integer $PaymentEventID
 * @property integer $fReversed
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property TaxInvoice $invoice
 */
class TaxInvoiceEventBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TaxInvoiceEvent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['InvoiceID', 'EventID', 'GLID', 'TaxGroupID', 'UserPartyID', 'ActionID', 'UnitID', 'PaymentEventID', 'fReversed', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['Amount', 'VAT'], 'number'],
            [['DueDate', 'JournalDate'], 'safe'],
            [['created_at', 'updated_at'], 'required'],
            [['Description', 'Unit', 'VATCode', 'EventParam'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'InvoiceID' => Yii::t('app', 'Invoice ID'),
            'EventID' => Yii::t('app', 'Event ID'),
            'Description' => Yii::t('app', 'Description'),
            'Unit' => Yii::t('app', 'Unit'),
            'Amount' => Yii::t('app', 'Amount'),
            'VAT' => Yii::t('app', 'Vat'),
            'DueDate' => Yii::t('app', 'Due Date'),
            'VATCode' => Yii::t('app', 'Vatcode'),
            'ID	int	NO' => Yii::t('app', 'Id	Int	 No'),
            'GLID' => Yii::t('app', 'Glid'),
            'TaxGroupID' => Yii::t('app', 'Tax Group ID'),
            'JournalDate' => Yii::t('app', 'Journal Date'),
            'EventParam' => Yii::t('app', 'Event Param'),
            'UserPartyID' => Yii::t('app', 'User Party ID'),
            'ActionID' => Yii::t('app', 'Action ID'),
            'UnitID' => Yii::t('app', 'Unit ID'),
            'PaymentEventID' => Yii::t('app', 'Payment Event ID'),
            'fReversed' => Yii::t('app', 'F Reversed'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(\common\models\TaxInvoice::className(), ['InvoiceID' => 'InvoiceID']);
    }
}
