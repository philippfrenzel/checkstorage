<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "Address".
 *
 * @property integer $AddressID
 * @property string $Code
 * @property string $City
 * @property string $Street
 * @property integer $IsPrimary
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property integer $AddressTypeID
 * @property integer $PartyID
 * @property integer $RegionID
 *
 * @property AddressType $addressType
 * @property Party $party
 */
class AddressBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IsPrimary', 'created_at', 'updated_at', 'deleted_at', 'AddressTypeID', 'PartyID', 'RegionID'], 'integer'],
            [['created_at', 'updated_at', 'PartyID'], 'required'],
            [['Code', 'City', 'Street'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'AddressID' => 'Address ID',
            'Code' => 'Code',
            'City' => 'City',
            'Street' => 'Street',
            'IsPrimary' => 'Is Primary',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'AddressTypeID' => 'Address Type ID',
            'PartyID' => 'Party ID',
            'RegionID' => 'Region ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressType()
    {
        return $this->hasOne(\common\models\AddressType::className(), ['AddressTypeID' => 'AddressTypeID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParty()
    {
        return $this->hasOne(\common\models\Party::className(), ['PartyID' => 'PartyID']);
    }
}
