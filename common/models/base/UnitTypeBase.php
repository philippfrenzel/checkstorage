<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "UnitType".
 *
 * @property integer $UnitTypeID
 * @property integer $Icon
 * @property string $QueryName
 * @property string $TableName
 * @property string $UnitType
 * @property string $Notes
 * @property integer $SortOrder
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property Unit[] $units
 */
class UnitTypeBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UnitType';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Icon', 'SortOrder', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['QueryName', 'Notes'], 'string'],
            [['created_at', 'updated_at'], 'required'],
            [['TableName', 'UnitType'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'UnitTypeID' => Yii::t('app', 'Unit Type ID'),
            'Icon' => Yii::t('app', 'Icon'),
            'QueryName' => Yii::t('app', 'Query Name'),
            'TableName' => Yii::t('app', 'Table Name'),
            'UnitType' => Yii::t('app', 'Unit Type'),
            'Notes' => Yii::t('app', 'Notes'),
            'SortOrder' => Yii::t('app', 'Sort Order'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnits()
    {
        return $this->hasMany(\common\models\Unit::className(), ['UnitTypeID' => 'UnitTypeID']);
    }
}
