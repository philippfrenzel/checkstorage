<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "ContractType".
 *
 * @property integer $ContractTypeID
 * @property string $ContractType
 * @property string $Icon
 * @property string $Notes
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property integer $MasterRoleTypeID
 *
 * @property Contract[] $contracts
 */
class ContractTypeBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ContractType';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Notes'], 'string'],
            [['created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at', 'deleted_at', 'MasterRoleTypeID'], 'integer'],
            [['ContractType', 'Icon'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ContractTypeID' => Yii::t('app', 'Contract Type ID'),
            'ContractType' => Yii::t('app', 'Contract Type'),
            'Icon' => Yii::t('app', 'Icon'),
            'Notes' => Yii::t('app', 'Notes'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'MasterRoleTypeID' => Yii::t('app', 'Master Role Type ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContracts()
    {
        return $this->hasMany(\common\models\Contract::className(), ['ContractTypeID' => 'ContractTypeID']);
    }
}
