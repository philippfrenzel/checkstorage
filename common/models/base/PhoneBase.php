<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "Phone".
 *
 * @property integer $PhoneID
 * @property integer $PartyID
 * @property integer $PhoneTypeID
 * @property string $PhoneNumber
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property Party $party
 * @property PhoneType $phoneType
 */
class PhoneBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Phone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PartyID', 'PhoneTypeID', 'created_at', 'updated_at'], 'required'],
            [['PartyID', 'PhoneTypeID', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['PhoneNumber'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'PhoneID' => Yii::t('app', 'Phone ID'),
            'PartyID' => Yii::t('app', 'Party ID'),
            'PhoneTypeID' => Yii::t('app', 'Phone Type ID'),
            'PhoneNumber' => Yii::t('app', 'Phone Number'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParty()
    {
        return $this->hasOne(\common\models\Party::className(), ['PartyID' => 'PartyID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoneType()
    {
        return $this->hasOne(\common\models\PhoneType::className(), ['PhoneTypeID' => 'PhoneTypeID']);
    }
}
