<?php

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "Action".
 *
 * @property integer $ActionID
 * @property integer $PlanID
 * @property integer $ActionTypeID
 * @property integer $RateID
 * @property string $StartDate
 * @property string $NextDate
 * @property string $CycleLength
 * @property string $StopDate
 * @property integer $ContractID
 * @property integer $PartyID
 * @property integer $UnitID
 * @property string $XWhen
 * @property string $Param
 * @property integer $ParentActionID
 * @property integer $IsInProcessor
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property ActionType $actionType
 * @property Event[] $events
 */
class ActionBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PlanID', 'ActionTypeID', 'RateID', 'ContractID', 'PartyID', 'UnitID', 'ParentActionID', 'IsInProcessor', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['ActionTypeID', 'created_at', 'updated_at'], 'required'],
            [['StartDate', 'NextDate', 'StopDate'], 'safe'],
            [['CycleLength', 'XWhen'], 'string', 'max' => 50],
            [['Param'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ActionID' => Yii::t('app', 'Action ID'),
            'PlanID' => Yii::t('app', 'Plan ID'),
            'ActionTypeID' => Yii::t('app', 'Action Type ID'),
            'RateID' => Yii::t('app', 'Rate ID'),
            'StartDate' => Yii::t('app', 'Start Date'),
            'NextDate' => Yii::t('app', 'Next Date'),
            'CycleLength' => Yii::t('app', 'Cycle Length'),
            'StopDate' => Yii::t('app', 'Stop Date'),
            'ContractID' => Yii::t('app', 'Contract ID'),
            'PartyID' => Yii::t('app', 'Party ID'),
            'UnitID' => Yii::t('app', 'Unit ID'),
            'XWhen' => Yii::t('app', 'Xwhen'),
            'Param' => Yii::t('app', 'Param'),
            'ParentActionID' => Yii::t('app', 'Parent Action ID'),
            'IsInProcessor' => Yii::t('app', 'Is In Processor'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionType()
    {
        return $this->hasOne(\common\models\ActionType::className(), ['ActionTypeID' => 'ActionTypeID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(\common\models\Event::className(), ['ActionID' => 'ActionID']);
    }
}
