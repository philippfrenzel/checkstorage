<?php

namespace common\models;

use Yii;
use common\models\scopes\PhoneQuery;

/**
 * This is the model class for table "Phone".
 */
class Phone extends \common\models\base\PhoneBase
{

	/**
     * @inheritdoc
     * @return PolizzenQuery
     */
    public static function find()
    {
        return new PhoneQuery(get_called_class());
    }

}
