<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "UnitSelfStor".
 * @author Philipp Frenzel <philipp@frenzel.net>
 */
class UnitSelfStor extends \common\models\base\UnitSelfStorBase
{

	/**
	 * This relation will load the Units from within UnitList and then relates them ot the unit table
	 * @todo this is by now not working correct, as it's still missing to "explode" the relation or change the syntax to be with "in"
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(\common\models\Contract::className(), ['UnitList' => 'UnitNumber'])
        			->via('unit');
    }

}
