<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'medium',
        ],
    ],
    'modules'    => [
        'user' => [
            'class'        => 'dektrium\user\Module',
            'defaultRoute' => 'profile',
            'admins'       => ['admin']
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
    ]
];
