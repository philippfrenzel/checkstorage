<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * Display a single unit
 * @todo need to add the information about the related features
 */

?>

<h3><?= \Yii::t('app','Party'); ?></h3>

<div class="form-group">
<?php echo DetailView::widget([
    'model' => $model,
	    'attributes' => [
	    	'PartyName',
	    	'address.Street',
	    	'address.City',
	    ],
]);?>
</div>
