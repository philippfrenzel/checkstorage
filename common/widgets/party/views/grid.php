<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\editable\Editable;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\bootstrap\ButtonDropdown;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PolizzenserviceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$myUrl = Url::toRoute(['/polizzen/delete']);

$JSEventClick = <<<EOF

$('#testaction-btn').click(function () {
    var keys = $('#polizzen-grid-overview').yiiGridView('getSelectedRows');
    $.ajax({
        type: "POST",
        url: "$myUrl",
        data: {'id' : keys}
    })
  .done(function( msg ) {
    alert( "Die Polizzen wurden entfernt " + msg.msg );
  });
});

EOF;

$this->registerJs($JSEventClick);

?>

<div class="bgwhite">

     <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'showPageSummary' => true,
        'hover' => true,
        'floatHeader' => false,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true
        ],
        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="fa fa-archive"></i> Kunden</h3>',
        ],
        'columns' => [
            //'PartyID',
            //'DLData',
            //'DLNumber',
            //'ID1',
            //'ID2',
            //'UnitSortOrder',
            //'GrpActRef',
            /*'DLRegionAbbr'*/
            /*'Identification2'*/
            /*'Identification1'*/
            'PartyName',
            /*'Soundex'*/
            /*'Alert:ntext'*/
            /*'PartyParam'*/
            'UnitList',
            [
                'attribute' => 'ContractEffective',
                'value' => 'contract.Effective',
            ],
            [
                'attribute' => 'Street',
                'value' => 'address.Street',
            ],
            [
                'attribute' => 'City',
                'value' => 'address.City',
            ],
            [
                'attribute' => 'ZIP',
                'value' => 'address.Code',
            ],
            /*'Note:ntext'*/
            /*'Created'*/
            /*'LockTime'*/
            /*'LockUser'*/
            /*'PTD'*/
            /*'created_at'*/
            /*'updated_at'*/
            /*'deleted_at'*/
            //'partyType.PartyType',
            /*'OccupationID'*/
            [
                'class' => 'kartik\grid\ActionColumn',
                'urlCreator' => function($action, $model, $key, $index) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
                    $params[0] = '/party/' . $action;
                    return \yii\helpers\Url::toRoute($params);
                },
                'contentOptions' => ['nowrap'=>'nowrap']
            ],
        ],        
    ]); ?>

</div>
