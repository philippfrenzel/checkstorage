<?php

/**
 * The widget to display the current existing QlikView documents
 */

namespace common\widgets\party;

use Yii;
use yii\widgets\Block;
use yii\helpers\Html;
use yii\helpers\Url;

use frontend\models\PartySearch;

class DisplayWidget extends Block
{
  /**
   * @inerhit doc
   */
  public $renderInPlace = true;


  public function init()
  {
    parent::init();
    $searchModel = new PartySearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    echo $this->render('grid', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]);
    
  }

}
