<?php

/**
 * The widget to display the current existing QlikView documents
 */

namespace common\widgets\phone;

use Yii;
use yii\widgets\Block;
use yii\helpers\Html;
use yii\helpers\Url;

use frontend\models\PhoneSearch;

class DisplayWidget extends Block
{
  /**
   * @inerhit doc
   */
  public $renderInPlace = true;

  /**
   * the PartyID that is the reference for the records beeing displayed within the widget
   * @var [type]
   */
  public $PartyID = NULL;

  public function init()
  {
    parent::init();

    $searchModel  = new PhoneSearch();
    $searchModel->PartyID = $this->PartyID;
    
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    echo $this->render('grid', [
        'searchModel'  => $searchModel,
        'dataProvider' => $dataProvider,
    ]);
  }

}
