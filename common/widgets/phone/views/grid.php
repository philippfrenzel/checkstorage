<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\editable\Editable;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\bootstrap\ButtonDropdown;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PolizzenserviceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

   <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'bordered' => false,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'showPageSummary' => false,
        'hover' => true,
        'floatHeader' => false,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true
        ],
        'columns' => [
            [
                'attribute' => 'PhoneType',
                'value' => 'phoneType.PhoneType',
            ],
            [
                'attribute' => 'PhoneNumber',
                'value' => 'PhoneNumber',
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'urlCreator' => function($action, $model, $key, $index) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
                    $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                    return \yii\helpers\Url::toRoute($params);
                },
                'contentOptions' => ['nowrap'=>'nowrap']
            ],
        ],
    ]); ?>
