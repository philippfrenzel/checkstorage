<?php

/**
 * The widget to display the current existing QlikView documents
 */

namespace common\widgets\rate;

use Yii;
use yii\widgets\Block;
use yii\helpers\Html;
use yii\helpers\Url;

class DisplayRelatedWidget extends Block
{
  /**
   * @inerhit doc
   */
  public $renderInPlace = true;

  /**
   * the PartyID that is the reference for the records beeing displayed within the widget
   * @var [type]
   */
  public $model = NULL;

  public function init()
  {
    parent::init();

    echo $this->render('grid_related', [
        'model'  => $this->model,
    ]);
  }

}
