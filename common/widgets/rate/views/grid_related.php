<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\editable\Editable;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\bootstrap\ButtonDropdown;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PolizzenserviceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="bgwhite">

<table class="table table-striped">

<tr>
    <th>Von</th>
    <th>Bis</th>
    <th>Preis</th>
</tr>

<?php
    $rates = $model->unit->unitRateHistories;
    foreach($rates AS $rate):
?>
<tr>
    <td>
        <?= $rate->Start; ?>
    </td>
    <td>
        <?= $rate->Stop; ?>
    </td>
    <td>
        <?= $rate->rate->PassThru; ?>
    </td>
</tr>

<?php endforeach; ?>

</table>

</div>
