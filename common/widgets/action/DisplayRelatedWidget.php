<?php

/**
 * The widget to display the current existing QlikView documents
 */

namespace common\widgets\action;

use Yii;
use yii\widgets\Block;
use yii\helpers\Html;
use yii\helpers\Url;

use frontend\models\EventSearch;

class DisplayRelatedWidget extends Block
{
  /**
   * @inerhit doc
   */
  public $renderInPlace = true;

  /**
   * the PartyID that is the reference for the records beeing displayed within the widget
   * @var [type]
   */
  public $PartyID = NULL;

  public function init()
  {
    parent::init();

    $searchModel  = new EventSearch();
    $searchModel->PartyID = $this->PartyID;
    
    $dataProvider = $searchModel->searchRelated(Yii::$app->request->queryParams);

    echo $this->render('grid_related', [
        'searchModel'  => $searchModel,
        'dataProvider' => $dataProvider,
    ]);
  }

}
