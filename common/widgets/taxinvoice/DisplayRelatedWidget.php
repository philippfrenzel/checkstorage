<?php

/**
 * The widget to display the current existing QlikView documents
 */

namespace common\widgets\taxinvoice;

use Yii;
use yii\widgets\Block;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\TaxInvoiceSearch;

class DisplayRelatedWidget extends Block
{
  /**
   * @inerhit doc
   */
  public $renderInPlace = true;

  /**
   * the PartyID that is the reference for the records beeing displayed within the widget
   * @var [type]
   */
  public $PartyID = NULL;

  public function init()
  {
    parent::init();

    $searchModel  = new TaxInvoiceSearch();
    $searchModel->PartyID = $this->PartyID;
    
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    echo $this->render('grid_related', [
        'searchModel'  => $searchModel,
        'dataProvider' => $dataProvider,
    ]);
  }

}
