<?php

/**
 * The widget to display the current existing QlikView documents
 */

namespace common\widgets\unit;

use Yii;
use yii\widgets\Block;
use yii\helpers\Html;

class DisplaySingleWidget extends Block
{
  /**
   * @inerhit doc
   */
  public $renderInPlace = true;

  public $model = NULL;

  /**
   * renders the passed over model as a item (unitselfstor)
   * @return html of the unit self stor
   */
  public function init()
  {
    parent::init();
    
    if(!is_null($this->model))
    {
      echo $this->render('single', [
          'model'  => $this->model
      ]);
    }
    else
    {
      echo \Yii::t('app',"Unit has not been active!");
    }
  }

}
