<?php

/**
 * The widget to display the current existing QlikView documents
 */

namespace common\widgets\unit;

use Yii;
use yii\widgets\Block;
use yii\helpers\Html;
use yii\helpers\Url;

use frontend\models\UnitSelfStorSearch;

class DisplayWidget extends Block
{
  /**
   * @inerhit doc
   */
  public $renderInPlace = true;


  public function init()
  {
    parent::init();
    $searchModel  = new UnitSelfStorSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    echo $this->render('grid', [
        'searchModel'  => $searchModel,
        'dataProvider' => $dataProvider,
    ]);
  }

}
