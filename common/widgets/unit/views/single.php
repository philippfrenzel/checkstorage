<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * Display a single unit
 * @todo need to add the information about the related features
 */

?>

<h3><?= \Yii::t('app','Unit'); ?></h3>

<div class="form-group">
<?php echo DetailView::widget([
    'model' => $model,
	    'attributes' => [
	    	'unit.UnitNumber',
	    	'unit.unitType.UnitType',
	    	'UnitHeight',
	    	'UnitLength',
	    	'UnitWidth',
	    	'unit.location.Location',
	    ],
]);?>
</div>
