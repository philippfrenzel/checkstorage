<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\editable\Editable;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\bootstrap\ButtonDropdown;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PolizzenserviceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="bgwhite">

   <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'bordered' => false,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'showPageSummary' => false,
        'hover' => true,
        'floatHeader' => false,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true
        ],
        'columns' => [
            [
                'attribute' => 'UnitNumber',
                'value' => 'unit.UnitNumber',
            ],
            /*[
                'attribute'=>'UnitHeight',
                'value'=>function ($model, $index, $widget) { 
                    return Html::tag('div',$model->UnitHeight.' x '.$model->UnitLength.' x '.$model->UnitWidth .' ' . $model->UnitOfMeasure);
                },
                'format' => 'raw'
            ],*/
            'RateWeekly:decimal',
            /*[
                'attribute'=>'Location',
                'value'=>function ($model, $index, $widget) { 
                    return Html::tag('div',is_Object($model->unit->location)?$model->unit->location->Location:'-');
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \common\models\Location::getListOptions(), 
                'filterWidgetOptions'=>[
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Location'],
                'format' => 'raw'
            ],*/
            [
                'class' => 'kartik\grid\ActionColumn',
                'urlCreator' => function($action, $model, $key, $index) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
                    $params[0] = '/unit-self-stor/' . $action;
                    return \yii\helpers\Url::toRoute($params);
                },
                'contentOptions' => ['nowrap'=>'nowrap']
            ],
        ],
    ]); ?>

</div>
