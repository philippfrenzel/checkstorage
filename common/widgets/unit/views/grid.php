<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\editable\Editable;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\bootstrap\ButtonDropdown;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PolizzenserviceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*
$myUrl = Url::toRoute(['/polizzen/delete']);

$JSEventClick = <<<EOF

$('#testaction-btn').click(function () {
    var keys = $('#polizzen-grid-overview').yiiGridView('getSelectedRows');
    $.ajax({
        type: "POST",
        url: "$myUrl",
        data: {'id' : keys}
    })
  .done(function( msg ) {
    alert( "Die Polizzen wurden entfernt " + msg.msg );
  });
});

EOF;

$this->registerJs($JSEventClick);
*/

?>

<div class="bgwhite">

   <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'showPageSummary' => true,
        'hover' => true,
        'floatHeader' => false,
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true
        ],
        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="fa fa-archive"></i> Abteile</h3>',
        ],
        'columns' => [
            [
                'attribute' => 'UnitNumber',
                'value' => 'unit.UnitNumber',
            ],
            [
                'attribute'=>'UnitHeight',
                'value'=>function ($model, $index, $widget) { 
                    return Html::tag('div',$model->UnitHeight.' x '.$model->UnitLength.' x '.$model->UnitWidth .' ' . $model->UnitOfMeasure);
                },
                'format' => 'raw'
            ],
            //'UnitHeight:decimal',
            //'UnitLength:decimal',
            //'UnitWidth:decimal',
            //'UnitOfMeasure',
            //'Overlock',
            //'WalkOrder',
            //'Note:ntext',
            [
                'attribute' => 'Party',
                'value' => 'contract.party.PartyName',
            ],
            [
                'attribute' => 'StatusNow',
                'value' => 'unit.StatusNow',
            ],
            'RateWeekly:decimal',
            'Note:ntext',
            [
                'attribute' => 'UnitType',
                'value' => 'unit.unitType.UnitType',
            ],
            //'UnitRating:ntext',
            //'PopupNote:ntext',
            //'RateDaily:decimal',
            //'RateMonthly:decimal',
            /*'created_at'*/
            /*'updated_at'*/
            /*'deleted_at'*/
            /*'DRateID'*/
            /*'FeatureCodeID'*/
            /*'MRateID'*/
            /*'WRateID'*/
            /*'SizeCodeID'*/
            [
                'attribute'=>'Location',
                'value'=>function ($model, $index, $widget) { 
                    return Html::tag('div',is_Object($model->unit->location)?$model->unit->location->Location:'-');
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \common\models\Location::getListOptions(), 
                'filterWidgetOptions'=>[
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Location'],
                'format' => 'raw'
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'urlCreator' => function($action, $model, $key, $index) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
                    $params[0] = '/unit-self-stor/' . $action;
                    return \yii\helpers\Url::toRoute($params);
                },
                'contentOptions' => ['nowrap'=>'nowrap']
            ],
        ],
    ]); ?>

</div>
