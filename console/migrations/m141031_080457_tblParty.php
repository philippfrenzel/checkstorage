<?php

use yii\db\Schema;
use yii\db\Migration;

class m141031_080457_tblParty extends Migration
{
    public function up()
    {
    	switch (Yii::$app->db->driverName) {
			case 'mysql':
				$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
				break;
			case 'pgsql':
				$tableOptions = null;
				break;
			default:
				throw new RuntimeException('Your database is not supported!');
		}

		$this->createTable('{{%Party}}',[
			//the id
			'PartyID'        => Schema::TYPE_PK, //UnitID
			
			//normal fields
			'DLData' => Schema::TYPE_STRING,
			'DLNumber' => Schema::TYPE_BINARY,
			'ID1' => Schema::TYPE_BINARY,
			'ID2' => Schema::TYPE_BINARY,
			'UnitSortOrder' => Schema::TYPE_INTEGER,
			'GrpActRef' => Schema::TYPE_INTEGER,
			'DLRegionAbbr' => Schema::TYPE_STRING,
			'Identification2' => Schema::TYPE_STRING,
			'Identification1' => Schema::TYPE_STRING,
			'PartyName' => Schema::TYPE_STRING,
			'Soundex' => Schema::TYPE_STRING,
			'Alert' => Schema::TYPE_TEXT,
			'PartyParam' => Schema::TYPE_STRING.'(1000)',
			'UnitList' => Schema::TYPE_STRING,
			'Note' => Schema::TYPE_TEXT,
			'Created' => Schema::TYPE_DATETIME,
			'LockTime' => Schema::TYPE_DATETIME,
			'LockUser' => Schema::TYPE_INTEGER,
			'PTD' => Schema::TYPE_DATETIME,

	        // timestamps
			'created_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'        => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
			'PartyTypeID'     => Schema::TYPE_INTEGER . ' DEFAULT NULL', 
			'OccupationID'    => Schema::TYPE_INTEGER . ' DEFAULT NULL', 
			
		],$tableOptions);

		$this->createTable('{{%PartyType}}',[
			//the id
			'PartyTypeID'        => Schema::TYPE_PK, //UnitID
			
			//normal fields
			'PartyType'    => Schema::TYPE_TEXT,
			'UserDefined'  => Schema::TYPE_INTEGER,
	        
	        // timestamps
			'created_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'        => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
			
		],$tableOptions);

		$this->addForeignKey('FK_Party_PartyType','{{%Party}}','PartyTypeID','{{%PartyType}}','PartyTypeID');

		$this->createTable('{{%Occupation}}',[
			//the id
			'OccupationID'        => Schema::TYPE_PK, //UnitID
			
			//normal fields
			'Occupation'    	=> Schema::TYPE_TEXT,
			'UserDefined'  		=> Schema::TYPE_INTEGER,
	        
	        // timestamps
			'created_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'        => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
			
		],$tableOptions);

		$this->addForeignKey('FK_Party_Occupation','{{%Party}}','OccupationID','{{%Occupation}}','OccupationID');

		$this->createTable('{{%Address}}',[
			//the id
			'AddressID'        => Schema::TYPE_PK, //UnitID
			
			//normal fields
			'Code' 			=> Schema::TYPE_STRING,
			'City'	 		=> Schema::TYPE_STRING,
			'Street' 		=> Schema::TYPE_STRING,
			'IsPrimary' 	=> Schema::TYPE_BOOLEAN,

	        // timestamps
			'created_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'        => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
			'AddressTypeID' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'PartyID' 		=> Schema::TYPE_INTEGER . ' NOT NULL',
			'RegionID' 		=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			
		],$tableOptions);

		$this->addForeignKey('FK_Address_Party','{{%Address}}','PartyID','{{%Party}}','PartyID');

		$this->createTable('{{%AddressType}}',[
			//the id
			'AddressTypeID'        => Schema::TYPE_PK, //UnitID
			
			//normal fields
			'AddressType'    	   => Schema::TYPE_TEXT,
			'MailToPriority'	   => Schema::TYPE_INTEGER,
			'UserDefined'  		   => Schema::TYPE_INTEGER,
	        
	        // timestamps
			'created_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'        => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
			
		],$tableOptions);

		$this->addForeignKey('FK_Address_AddressType','{{%Address}}','AddressTypeID','{{%AddressType}}','AddressTypeID');

    }

    public function down()
    {   
    	//drop FKs
    	$this->dropForeignKey('FK_Party_PartyType','{{%Party}}');
    	$this->dropForeignKey('FK_Party_Occupation','{{%Party}}');
    	$this->dropForeignKey('FK_Address_Party','{{%Address}}');
    	$this->dropForeignKey('FK_Address_AddressType','{{%Address}}');

        $this->dropTable('{{%Address}}');
        $this->dropTable('{{%AddressType}}');
        $this->dropTable('{{%PartyType}}');
        $this->dropTable('{{%Occupation}}');
        $this->dropTable('{{%Party}}');
    }
}
