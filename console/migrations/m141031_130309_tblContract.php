<?php

use yii\db\Schema;
use yii\db\Migration;

class m141031_130309_tblContract extends Migration
{
    public function up()
    {
    	switch (Yii::$app->db->driverName) {
			case 'mysql':
				$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
				break;
			case 'pgsql':
				$tableOptions = null;
				break;
			default:
				throw new RuntimeException('Your database is not supported!');
		}

		$this->createTable('{{%Contract}}',[
			//the id
			'ContractID'          => Schema::TYPE_PK, //The Contract ID
			
			//normal fields
			'CycleLength' 		  => SCHEMA::TYPE_STRING.'(5)',
			'UnitSortOrder' 	  => SCHEMA::TYPE_INTEGER.'(20)',
			'ContractNumber' 	  => SCHEMA::TYPE_STRING.'(50)',
			'Param' 			  => SCHEMA::TYPE_STRING.'(255)',
			'PTSizeNeeded' 		  => SCHEMA::TYPE_STRING,
			'UnitList' 			  => SCHEMA::TYPE_TEXT,
			'PTReason' 			  => SCHEMA::TYPE_TEXT,
			'AmtDue' 			  => SCHEMA::TYPE_MONEY,
			'Balance' 		      => SCHEMA::TYPE_MONEY,
			'Deposit' 			  => SCHEMA::TYPE_MONEY,
			'Effective' 		  => SCHEMA::TYPE_DATETIME, //is the real start date of the contract
			'IsInProcessor' 	  => SCHEMA::TYPE_INTEGER,
			'LastRebuildPrepay'   => SCHEMA::TYPE_DATETIME,
			'MarketDistance' 	  => SCHEMA::TYPE_FLOAT,
			'MinimumDuration'     => SCHEMA::TYPE_DATETIME,
			'OpenCredits' 		  => SCHEMA::TYPE_MONEY,
			'Pau' 				  => SCHEMA::TYPE_DATETIME, //is the real end date for the contract duration
			'PTD' 				  => SCHEMA::TYPE_DATETIME,
			'PTDateMovein' 		  => SCHEMA::TYPE_DATETIME,
			'PTDateNeeded' 		  => SCHEMA::TYPE_DATETIME,
			'Rent' 				  => SCHEMA::TYPE_MONEY,
			'UnitIns' 			  => SCHEMA::TYPE_MONEY,
			'UnitOther' 		  => SCHEMA::TYPE_MONEY,
			'UnitRent' 			  => SCHEMA::TYPE_MONEY,
			'UnitTax' 			  => SCHEMA::TYPE_MONEY,
			'VacateDate' 	      => SCHEMA::TYPE_DATETIME,

	        // timestamps
			'created_at'          => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'          => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'          => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
			'UserID'     			=> Schema::TYPE_INTEGER . ' DEFAULT NULL', 
			'PartyID' 		 	 	=> Schema::TYPE_INTEGER . ' NOT NULL',
			'MarketReasonID' 		=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'MarketSourceID' 		=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'ContractTypeID' 		=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			
		],$tableOptions);

		$this->addForeignKey('FK_Contract_Party','{{%Contract}}','PartyID','{{%Party}}','PartyID');

		$this->createTable('{{%MarketReason}}',[
			//the id
			'MarketReasonID'          => Schema::TYPE_PK, //The MarketReason ID
			
			//normal fields
			'MarketReason' 		  	=> SCHEMA::TYPE_STRING.'(255)',

	        // timestamps
			'created_at'          => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'          => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'          => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
			
		],$tableOptions);

		$this->addForeignKey('FK_Contract_MarketReason','{{%Contract}}','MarketReasonID','{{%MarketReason}}','MarketReasonID');

		$this->createTable('{{%MarketSource}}',[
			//the id
			'MarketSourceID'      => Schema::TYPE_PK, //The MarketSource ID
			
			//normal fields
			'MarketSource' 		  => SCHEMA::TYPE_STRING . '(50)',

	        // timestamps
			'created_at'          => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'          => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'          => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
			
		],$tableOptions);

		$this->addForeignKey('FK_Contract_MarketSource','{{%Contract}}','MarketSourceID','{{%MarketSource}}','MarketSourceID');

		$this->createTable('{{%ContractType}}',[
			//the id
			'ContractTypeID'        => Schema::TYPE_PK, //The ContractType ID
			
			//normal fields
			'ContractType' 		  	=> SCHEMA::TYPE_STRING . '(50)',
			'Icon' 	 				=> SCHEMA::TYPE_STRING . '(50)',
			'Notes'  				=> SCHEMA::TYPE_TEXT,

	        // timestamps
			'created_at'            => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'            => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'            => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
			'MasterRoleTypeID' 	    => Schema::TYPE_INTEGER . ' DEFAULT NULL',

		],$tableOptions);

		$this->addForeignKey('FK_Contract_ContractType','{{%Contract}}','ContractTypeID','{{%ContractType}}','ContractTypeID');

    }

    public function down()
    {
        //first remove the fk
        $this->dropForeignKey('FK_Contract_Party','{{%Contract}}');
        $this->dropForeignKey('FK_Contract_ContractType','{{%Contract}}');
        $this->dropForeignKey('FK_Contract_MarketReason','{{%Contract}}');
        $this->dropForeignKey('FK_Contract_MarketSource','{{%Contract}}');

        $this->dropTable('{{%MarketReason}}');
        $this->dropTable('{{%MarketSource}}');
        $this->dropTable('{{%ContractType}}');
        $this->dropTable('{{%Contract}}');
    }
}
