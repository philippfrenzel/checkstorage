<?php

use yii\db\Schema;
use yii\db\Migration;

class m141116_130712_unitratehistory extends Migration
{
    public function up()
    {
    	switch (Yii::$app->db->driverName) {
			case 'mysql':
				$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
				break;
			case 'pgsql':
				$tableOptions = null;
				break;
			default:
				throw new RuntimeException('Your database is not supported!');
		}

		$this->createTable('{{%UnitRateHistory}}',[
			'UnitID' => Schema::TYPE_INTEGER . ' NOT NULL',
			'RateID' => Schema::TYPE_INTEGER . ' NOT NULL',
			'Start' => Schema::TYPE_DATETIME . ' NOT NULL',
			'Stop' => Schema::TYPE_DATETIME . ' DEFAULT NULL',
			
	        // timestamps
			'created_at'      => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'      => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'      => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys

		],$tableOptions);

		$this->addForeignKey('FK_Rate_Unit','{{%UnitRateHistory}}','UnitID','{{%Unit}}','UnitID');
		$this->addForeignKey('FK_Rate_UnitRateHistory','{{%UnitRateHistory}}','RateID','{{%Rate}}','RateID');
    }

    public function down()
    {
    	$this->dropForeignKey('FK_Rate_Unit','{{%UnitRateHistory}}');
    	$this->dropForeignKey('FK_Rate_UnitRateHistory','{{%UnitRateHistory}}');

        $this->dropTable('{{%UnitRateHistory}}');
    }
}
