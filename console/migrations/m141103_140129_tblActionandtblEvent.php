<?php

use yii\db\Schema;
use yii\db\Migration;

class m141103_140129_tblActionandtblEvent extends Migration
{
    public function up()
    {

    	switch (Yii::$app->db->driverName) {
			case 'mysql':
				$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
				break;
			case 'pgsql':
				$tableOptions = null;
				break;
			default:
				throw new RuntimeException('Your database is not supported!');
		}

		$this->createTable('{{%ActionType}}',[
			//the id
			'ActionTypeID'        => Schema::TYPE_PK, //The ContractType ID
			
			//normal fields
			'ActionType' => Schema::TYPE_STRING . '(50)',
			'ActionGroup' => Schema::TYPE_STRING . '(50)',
			'DebitCredit' => Schema::TYPE_STRING . '(2)',
			'AR' => Schema::TYPE_STRING . '(2)',
			'AP' => Schema::TYPE_STRING . '(2)',
			'OrderOfAction' => Schema::TYPE_INTEGER,
			'NoteShort' => Schema::TYPE_STRING .'(50)',
			'Note' => Schema::TYPE_STRING .'(255)',
			'TechNote' => Schema::TYPE_STRING .'(255)',
			'Color' => Schema::TYPE_INTEGER,
			'Icon' => Schema::TYPE_STRING .'(50)',
			'DefaultBaseDate' => Schema::TYPE_STRING . '(50)',
			'UnitRateBased' => Schema::TYPE_INTEGER,
			'SaveAction' => Schema::TYPE_INTEGER,
			'CanPreprocess' => Schema::TYPE_INTEGER,
			'CanDeposit' => Schema::TYPE_INTEGER,
			'Associate' => Schema::TYPE_STRING .'(50)',
			'ParamTemplate' => Schema::TYPE_STRING .'(255)',
			'InventoryFactor' => Schema::TYPE_INTEGER,
			'RefParam' => Schema::TYPE_STRING .'(50)',
			'UnitStatus' => Schema::TYPE_STRING .'(2)',
			'HasContractID' => Schema::TYPE_INTEGER,
			'Per' 	  => Schema::TYPE_STRING .'(50)',
			'Invoice' => Schema::TYPE_INTEGER,

	        // timestamps
			'created_at'            => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'            => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'            => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
			

		],$tableOptions);

		$this->createTable('{{%Action}}',[
			//the id
			'ActionID'        => Schema::TYPE_PK, //The ContractType ID
			
			//FKs
			'PlanID' 		  => Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'ActionTypeID' 	  => Schema::TYPE_INTEGER . ' NOT NULL',
			'RateID' 		  => Schema::TYPE_INTEGER . ' DEFAULT NULL',

			//normal fields
			'StartDate' 		=> Schema::TYPE_DATETIME,
			'NextDate' 			=> Schema::TYPE_DATETIME,
			'CycleLength' 		=> Schema::TYPE_STRING . '(50)',
			'StopDate' 			=> Schema::TYPE_DATETIME,
			
			//FKs
			'ContractID' 		=> Schema::TYPE_INTEGER . ' DEFAULT NULL', //mögliche fehlerursache
			'PartyID' 			=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'UnitID' 			=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			
			'XWhen' 			=> Schema::TYPE_STRING . '(50)',
			'Param' 			=> Schema::TYPE_STRING . '(1000)',
			
			//FKs
			'ParentActionID' 	=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			
			'IsInProcessor' 	=> Schema::TYPE_BOOLEAN,
			
	        // timestamps
			'created_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'        => Schema::TYPE_INTEGER . ' DEFAULT NULL',
			

		],$tableOptions);

		$this->addForeignKey('FK_Action_ActionType','{{%Action}}','ActionTypeID','{{%ActionType}}','ActionTypeID');

		$this->createTable('{{%Event}}',[
			//the id
			'EventID'        => Schema::TYPE_PK, //The ContractType ID
			
			//FKs
			'ActionID' 		  => Schema::TYPE_INTEGER . ' NOT NULL',  //error in rentplus, null is allowed ...
			
			'Param' => Schema::TYPE_STRING . '(1000)',
			'UserPartyID' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'Created' => Schema::TYPE_DATETIME,
			'JournalDate' => Schema::TYPE_DATETIME,
			'EAmount' => Schema::TYPE_FLOAT,
			'ETax' => Schema::TYPE_FLOAT,
			'ESnapshot' => Schema::TYPE_STRING . '(255)',
			'CID' => Schema::TYPE_INTEGER . ' DEFAULT NULL',	
			'datFrom' => Schema::TYPE_DATETIME,
			'datTo' => Schema::TYPE_DATETIME,
			'Description' => Schema::TYPE_STRING . '(255)',

	        // timestamps
			'created_at'            => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'            => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'            => Schema::TYPE_INTEGER . ' DEFAULT NULL',
			

		],$tableOptions);

		$this->addForeignKey('FK_Event_Action','{{%Event}}','ActionID','{{%Action}}','ActionID');

    }

    public function down()
    {
        //DROP FKs
        $this->dropForeignKey('FK_Action_ActionType','{{%Action}}');
        $this->dropForeignKey('FK_Event_Action','{{%Event}}');

        $this->dropTable('{{%ActionType}}');
        $this->dropTable('{{%Action}}');
        $this->dropTable('{{%Event}}');

    }
}
