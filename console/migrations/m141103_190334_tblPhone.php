<?php

use yii\db\Schema;
use yii\db\Migration;

class m141103_190334_tblPhone extends Migration
{
    public function up()
    {
    	switch (Yii::$app->db->driverName) {
			case 'mysql':
				$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
				break;
			case 'pgsql':
				$tableOptions = null;
				break;
			default:
				throw new RuntimeException('Your database is not supported!');
		}

		$this->createTable('{{%PhoneType}}',[
			//the id
			'PhoneTypeID'        => Schema::TYPE_PK, //The ContractType ID
			
			//normal fields
			'PhoneType' 	=> Schema::TYPE_STRING . '(50)',
			'UserDefined' 	=> Schema::TYPE_BOOLEAN,
			'Icon' 	 	 	=> Schema::TYPE_STRING .'(50)',
			'PatternMatching' => Schema::TYPE_INTEGER,	
			'ConnectWord' 	=> Schema::TYPE_STRING .'(50)',
			'Significance'  => Schema::TYPE_BOOLEAN,
			
	        // timestamps
			'created_at'    => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'    => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'    => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
			

		],$tableOptions);

		$this->createTable('{{%Phone}}',[
			//the id
			'PhoneID'         => Schema::TYPE_PK, //The ContractType ID
			
			//FKs
			'PartyID' 	  	  => Schema::TYPE_INTEGER . ' NOT NULL',
			'PhoneTypeID' 	  => Schema::TYPE_INTEGER . ' NOT NULL',

			//normal fields
			'PhoneNumber' 	  => Schema::TYPE_STRING . '(200)',
			
	        // timestamps
			'created_at'      => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'      => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'      => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
			

		],$tableOptions);

		$this->addForeignKey('FK_Phone_PhoneType','{{%Phone}}','PhoneTypeID','{{%PhoneType}}','PhoneTypeID');
		$this->addForeignKey('FK_Phone_Party','{{%Phone}}','PartyID','{{%Party}}','PartyID');

    }

    public function down()
    {
        //drop fks
    	$this->dropForeignKey('FK_Phone_PhoneType','{{%Phone}}');
    	$this->dropForeignKey('FK_Phone_Party','{{%Phone}}');

        $this->dropTable('{{%Phone}}');
        $this->dropTable('{{%PhoneType}}');
    }
}
