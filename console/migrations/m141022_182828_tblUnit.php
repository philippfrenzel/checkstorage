<?php

use yii\db\Schema;
use yii\db\Migration;

class m141022_182828_tblUnit extends Migration
{
	public function up()
	{
		switch (Yii::$app->db->driverName) {
			case 'mysql':
				$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
				break;
			case 'pgsql':
				$tableOptions = null;
				break;
			default:
				throw new RuntimeException('Your database is not supported!');
		}

		$this->createTable('{{%Unit}}',[
			//the id
			'UnitID'                => Schema::TYPE_PK, //UnitID
			
			//normal fields
			'Unit'				=> Schema::TYPE_STRING, //long unitnumber, normal 4 digets
			'StatusNow'			=> Schema::TYPE_TEXT, //current status of the unit -> @todo needs to be removed later as this gets dynamically calculated
			'SortOrder'			=> Schema::TYPE_INTEGER, //sort order
			'UnitNumber'		=> Schema::TYPE_STRING,
			'CatalogSet'		=> Schema::TYPE_TEXT,
			'CatalogDesc'		=> Schema::TYPE_TEXT,
	        
	        // timestamps
			'created_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'        => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
			'SiteID'			=> Schema::TYPE_INTEGER . ' NOT NULL', //to which site belongs this unit
			'LocationID'		=> Schema::TYPE_INTEGER . ' NOT NULL', //DONE to which location belongs this unit
			'OffSiteAddressID'	=> Schema::TYPE_INTEGER . ' NOT NULL', //if this unit is currently placed somewhere else e.g. customers place
			'UnitTypeID'        => Schema::TYPE_INTEGER . ' NOT NULL'  //DONE what type of unit is this... can be although an shop article
			
		],$tableOptions);

		$this->createTable('{{%Location}}',[
			//the id
			'LocationID'        => Schema::TYPE_PK, //UnitID
			
			//normal fields
			'Location'			=> Schema::TYPE_STRING, //long unitnumber, normal 4 digets
			'SortOrder'			=> Schema::TYPE_INTEGER, //sort order
	        
	        // timestamps
			'created_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'        => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
			'ParentID'			=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'LocationTypeID'    => Schema::TYPE_INTEGER . ' NOT NULL'  //what type of location is this... can be although an shop article
			
		],$tableOptions);

		$this->createTable('{{%UnitSelfStor}}',[
			//the id
			'UnitID'            => Schema::TYPE_PK, //UnitID
			
			//normal fields
			'Overlock' 			=> Schema::TYPE_STRING,
			'WalkOrder' 		=> Schema::TYPE_STRING,
			'UnitOfMeasure' 	=> Schema::TYPE_STRING,
			'Note' 				=> Schema::TYPE_TEXT, //shitty design @todo needs to be refactored after taking data over
			'UnitRating' 		=> Schema::TYPE_TEXT,
			'PopupNote' 		=> Schema::TYPE_TEXT, //shitty design @todo needs to be refactored after taking data over
			'RateDaily' 		=> Schema::TYPE_FLOAT, //shitty design @todo needs to be refactored after taking data over
			'RateMonthly' 		=> Schema::TYPE_FLOAT, //shitty design @todo needs to be refactored after taking data over
			'RateWeekly' 		=> Schema::TYPE_FLOAT, //shitty design @todo needs to be refactored after taking data over			
			'UnitHeight' 		=> Schema::TYPE_FLOAT,
			'UnitLength' 		=> Schema::TYPE_FLOAT,
			'UnitWidth' 		=> Schema::TYPE_FLOAT,
			
	        // timestamps
			'created_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'        => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
	        'DRateID'			=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'FeatureCodeID'		=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'MRateID'			=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'WRateID'			=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'SizeCodeID' 		=> Schema::TYPE_INTEGER . ' DEFAULT NULL'
			
		],$tableOptions);

		$this->createTable('{{%UnitType}}',[
			//the id
			'UnitTypeID'        => Schema::TYPE_PK, //UnitID
			
			//normal fields
			'Icon' 		=> Schema::TYPE_INTEGER,
			'QueryName' => Schema::TYPE_TEXT,
			'TableName' => Schema::TYPE_STRING,
			'UnitType' 	=> Schema::TYPE_STRING, 
			'Notes' 	=> Schema::TYPE_TEXT,
			'SortOrder' => Schema::TYPE_INTEGER,
	        
	        // timestamps
			'created_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'        => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
			
		],$tableOptions);


		$this->createTable('{{%FeatureCode}}',[
			//the id
			'FeatureCodeID'     => Schema::TYPE_PK, //UnitID
			
			//normal fields
			'FeatureCodeDesc' 	=> Schema::TYPE_TEXT . '(100)',
			'PriceDeviation'  	=> Schema::TYPE_FLOAT,

	        // timestamps
			'created_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'        => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
			
		],$tableOptions);

		$this->createTable('{{%Rate}}',[
			//the id
			'RateID'          => Schema::TYPE_PK, 
			
			//normal fields
			
			'UnitOfMeasure' 	=>	Schema::TYPE_STRING,
			'CR' 				=>	Schema::TYPE_INTEGER,
			'DR' 				=>	Schema::TYPE_INTEGER,
			'PassThru' 			=>	Schema::TYPE_INTEGER,
			'Quantity' 			=>	Schema::TYPE_INTEGER,
			'RateTypeID' 		=>	Schema::TYPE_INTEGER,
			'TaxGroupID' 		=>	Schema::TYPE_INTEGER,
			'UnitPrice' 		=>	Schema::TYPE_FLOAT,

	        // timestamps
			'created_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'        => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys
			
		],$tableOptions);

		//add the related fk's
        $this->addForeignKey('FK_UnitSelfStor_Unit','{{%UnitSelfStor}}','UnitID','{{%Unit}}','UnitID');
        $this->addForeignKey('FK_UnitSelfStor_FeatureCode','{{%UnitSelfStor}}','FeatureCodeID','{{%FeatureCode}}','FeatureCodeID');
        
        $this->addForeignKey('FK_Unit_UnitType','{{%Unit}}','UnitTypeID','{{%UnitType}}','UnitTypeID');

        $this->addForeignKey('FK_Unit_Location','{{%Unit}}','LocationID','{{%Location}}','LocationID');
        $this->addForeignKey('FK_Location_Location','{{%Location}}','ParentID','{{%Location}}','LocationID');

	}

	public function down()
	{
		//first remove all constraints
		$this->dropForeignKey('FK_UnitSelfStor_Unit','{{%UnitSelfStor}}');
		$this->dropForeignKey('FK_UnitSelfStor_FeatureCode','{{%UnitSelfStor}}');
		$this->dropForeignKey('FK_Unit_UnitType','{{%Unit}}');
		$this->dropForeignKey('FK_Unit_Location','{{%Unit}}');
		$this->dropForeignKey('FK_Location_Location','{{%Location}}');

		//delete the tables in inverse order
		$this->dropTable('{{%Location}}');
		$this->dropTable('{{%Rate}}');
		$this->dropTable('{{%UnitSelfStor}}');
		$this->dropTable('{{%FeatureCode}}');
		$this->dropTable('{{%UnitType}}');
		$this->dropTable('{{%Unit}}');

	}
}
