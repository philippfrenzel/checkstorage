<?php

use yii\db\Schema;
use yii\db\Migration;

class m141125_191506_tblInvoice extends Migration
{
    public function up()
    {
    	switch (Yii::$app->db->driverName) {
			case 'mysql':
				$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
				break;
			case 'pgsql':
				$tableOptions = null;
				break;
			default:
				throw new RuntimeException('Your database is not supported!');
		}

		
		$this->createTable('{{%TaxInvoice}}',[
			//the id
			'InvoiceID'        => Schema::TYPE_PK, //The ContractType ID
			
			//normal fields
			'InvoiceNumber'     => Schema::TYPE_STRING . '(50)',
			'InvoiceDate'		=> Schema::TYPE_DATETIME,
			'DueDate'			=> Schema::TYPE_DATETIME,
			'PartyID' 		 	=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'ContractID' 		=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'Note'				=> Schema::TYPE_STRING,
			'Comment'			=> Schema::TYPE_STRING,
			'Param'				=> Schema::TYPE_STRING,
			'Export'			=> Schema::TYPE_BOOLEAN,
			'InvoiceNumeric'	=> Schema::TYPE_INTEGER . ' NOT NULL',
			
	        // timestamps
			'created_at'    => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'    => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'    => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys

		],$tableOptions);

		$this->createTable('{{%TaxInvoiceEvent}}',[
			//normal fields
			
			'InvoiceID' 	=> Schema::TYPE_INTEGER,
			'EventID' 		=> Schema::TYPE_INTEGER . ' DEFAULT NULL', //@todo this should not be allowed to be NULL
			'Description' 	=> Schema::TYPE_STRING,
			'Unit' 			=> Schema::TYPE_STRING,
			'Amount' 		=> Schema::TYPE_FLOAT,
			'VAT' 			=> Schema::TYPE_FLOAT,
			'DueDate' 		=> Schema::TYPE_DATETIME,
			'VATCode' 		=> Schema::TYPE_STRING,
			'ID	int	NO' 	=> Schema::TYPE_PK, //this seems to be the pk
			'GLID' 			=> Schema::TYPE_INTEGER . ' DEFAULT NULL',//@todo this should not be allowed to be NULL
			'TaxGroupID' 	=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'JournalDate' 	=> Schema::TYPE_DATETIME,
			'EventParam' 	=> Schema::TYPE_STRING,
			'UserPartyID' 	=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'ActionID' 		=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'UnitID' 		=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'PaymentEventID' 	=> Schema::TYPE_INTEGER . ' DEFAULT NULL',
			'fReversed' 	=> Schema::TYPE_BOOLEAN,

	        // timestamps
			'created_at'    => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'    => Schema::TYPE_INTEGER . ' NOT NULL',
			'deleted_at'    => Schema::TYPE_INTEGER . ' DEFAULT NULL',
	        
	        //Foreign Keys

		],$tableOptions);

		$this->addForeignKey('FK_TaxInvoiceEvent_TaxInvoice','{{%TaxInvoiceEvent}}','InvoiceID','{{%TaxInvoice}}','InvoiceID');
    }

    public function down()
    {
    	$this->dropForeignKey('FK_TaxInvoiceEvent_TaxInvoice','{{%TaxInvoiceEvent}}');

        $this->dropTable('{{%TaxInvoiceEvent}}');
        $this->dropTable('{{%TaxInvoice}}');
    }
}
