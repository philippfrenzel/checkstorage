<?php

namespace backend\controllers;

use common\models\AddressType;
use backend\models\AddressTypeSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * AddressTypeController implements the CRUD actions for AddressType model.
 */
class AddressTypeController extends Controller
{
	/**
	 * Lists all AddressType models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new AddressTypeSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single AddressType model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($AddressTypeID)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($AddressTypeID),
		]);
	}

	/**
	 * Creates a new AddressType model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new AddressType;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing AddressType model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($AddressTypeID)
	{
		$model = $this->findModel($AddressTypeID);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing AddressType model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($AddressTypeID)
	{
		$this->findModel($AddressTypeID)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the AddressType model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return AddressType the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($AddressTypeID)
	{
		if (($model = AddressType::findOne($AddressTypeID)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
