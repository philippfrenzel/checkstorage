<?php

namespace backend\controllers;

use common\models\Action;
use backend\models\ActionSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * ActionController implements the CRUD actions for Action model.
 */
class ActionController extends Controller
{
	/**
	 * Lists all Action models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ActionSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single Action model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($ActionID)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($ActionID),
		]);
	}

	/**
	 * Creates a new Action model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Action;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing Action model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($ActionID)
	{
		$model = $this->findModel($ActionID);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Action model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($ActionID)
	{
		$this->findModel($ActionID)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the Action model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Action the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($ActionID)
	{
		if (($model = Action::findOne($ActionID)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
