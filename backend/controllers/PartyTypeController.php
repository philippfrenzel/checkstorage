<?php

namespace backend\controllers;

use common\models\PartyType;
use backend\models\PartyTypeSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * PartyTypeController implements the CRUD actions for PartyType model.
 */
class PartyTypeController extends Controller
{
	/**
	 * Lists all PartyType models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new PartyTypeSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single PartyType model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($PartyTypeID)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($PartyTypeID),
		]);
	}

	/**
	 * Creates a new PartyType model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new PartyType;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing PartyType model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($PartyTypeID)
	{
		$model = $this->findModel($PartyTypeID);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing PartyType model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($PartyTypeID)
	{
		$this->findModel($PartyTypeID)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the PartyType model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return PartyType the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($PartyTypeID)
	{
		if (($model = PartyType::findOne($PartyTypeID)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
