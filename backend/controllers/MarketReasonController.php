<?php

namespace backend\controllers;

use common\models\MarketReason;
use backend\models\MarketReasonSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * MarketReasonController implements the CRUD actions for MarketReason model.
 */
class MarketReasonController extends Controller
{
	/**
	 * Lists all MarketReason models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new MarketReasonSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single MarketReason model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($MarketReasonID)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($MarketReasonID),
		]);
	}

	/**
	 * Creates a new MarketReason model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new MarketReason;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing MarketReason model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($MarketReasonID)
	{
		$model = $this->findModel($MarketReasonID);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing MarketReason model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($MarketReasonID)
	{
		$this->findModel($MarketReasonID)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the MarketReason model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return MarketReason the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($MarketReasonID)
	{
		if (($model = MarketReason::findOne($MarketReasonID)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
