<?php

namespace backend\controllers;

use common\models\MarketSource;
use backend\models\MarketSourceSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * MarketSourceController implements the CRUD actions for MarketSource model.
 */
class MarketSourceController extends Controller
{
	/**
	 * Lists all MarketSource models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new MarketSourceSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single MarketSource model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($MarketSourceID)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($MarketSourceID),
		]);
	}

	/**
	 * Creates a new MarketSource model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new MarketSource;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing MarketSource model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($MarketSourceID)
	{
		$model = $this->findModel($MarketSourceID);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing MarketSource model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($MarketSourceID)
	{
		$this->findModel($MarketSourceID)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the MarketSource model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return MarketSource the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($MarketSourceID)
	{
		if (($model = MarketSource::findOne($MarketSourceID)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
