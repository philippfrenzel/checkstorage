<?php

namespace backend\controllers;

use common\models\ActionType;
use backend\models\ActionTypeSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * ActionTypeController implements the CRUD actions for ActionType model.
 */
class ActionTypeController extends Controller
{
	/**
	 * Lists all ActionType models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ActionTypeSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single ActionType model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($ActionTypeID)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($ActionTypeID),
		]);
	}

	/**
	 * Creates a new ActionType model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new ActionType;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing ActionType model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($ActionTypeID)
	{
		$model = $this->findModel($ActionTypeID);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing ActionType model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($ActionTypeID)
	{
		$this->findModel($ActionTypeID)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the ActionType model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return ActionType the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($ActionTypeID)
	{
		if (($model = ActionType::findOne($ActionTypeID)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
