<?php

namespace backend\controllers;

use common\models\ContractType;
use backend\models\ContractTypeSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * ContractTypeController implements the CRUD actions for ContractType model.
 */
class ContractTypeController extends Controller
{
	/**
	 * Lists all ContractType models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ContractTypeSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single ContractType model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($ContractTypeID)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($ContractTypeID),
		]);
	}

	/**
	 * Creates a new ContractType model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new ContractType;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing ContractType model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($ContractTypeID)
	{
		$model = $this->findModel($ContractTypeID);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing ContractType model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($ContractTypeID)
	{
		$this->findModel($ContractTypeID)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the ContractType model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return ContractType the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($ContractTypeID)
	{
		if (($model = ContractType::findOne($ContractTypeID)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
