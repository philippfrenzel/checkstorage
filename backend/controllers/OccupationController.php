<?php

namespace backend\controllers;

use common\models\Occupation;
use backend\models\OccupationSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * OccupationController implements the CRUD actions for Occupation model.
 */
class OccupationController extends Controller
{
	/**
	 * Lists all Occupation models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new OccupationSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single Occupation model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($OccupationID)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($OccupationID),
		]);
	}

	/**
	 * Creates a new Occupation model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Occupation;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing Occupation model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($OccupationID)
	{
		$model = $this->findModel($OccupationID);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Occupation model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($OccupationID)
	{
		$this->findModel($OccupationID)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the Occupation model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Occupation the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($OccupationID)
	{
		if (($model = Occupation::findOne($OccupationID)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
