<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ContractType;

/**
 * ContractTypeSearch represents the model behind the search form about ContractType.
 */
class ContractTypeSearch extends Model
{
	public $ContractTypeID;
	public $ContractType;
	public $Icon;
	public $Notes;
	public $created_at;
	public $updated_at;
	public $deleted_at;
	public $MasterRoleTypeID;

	public function rules()
	{
		return [
			[['ContractTypeID', 'created_at', 'updated_at', 'deleted_at', 'MasterRoleTypeID'], 'integer'],
			[['ContractType', 'Icon', 'Notes'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'ContractTypeID' => 'Contract Type ID',
			'ContractType' => 'Contract Type',
			'Icon' => 'Icon',
			'Notes' => 'Notes',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
			'MasterRoleTypeID' => 'Master Role Type ID',
		];
	}

	public function search($params)
	{
		$query = ContractType::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'ContractTypeID' => $this->ContractTypeID,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'MasterRoleTypeID' => $this->MasterRoleTypeID,
        ]);

		$query->andFilterWhere(['like', 'ContractType', $this->ContractType])
            ->andFilterWhere(['like', 'Icon', $this->Icon])
            ->andFilterWhere(['like', 'Notes', $this->Notes]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
