<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Action;

/**
 * ActionSearch represents the model behind the search form about Action.
 */
class ActionSearch extends Model
{
	public $ActionID;
	public $PlanID;
	public $ActionTypeID;
	public $RateID;
	public $StartDate;
	public $NextDate;
	public $CycleLength;
	public $StopDate;
	public $ContractID;
	public $PartyID;
	public $UnitID;
	public $XWhen;
	public $Param;
	public $ParentActionID;
	public $IsInProcessor;
	public $created_at;
	public $updated_at;
	public $deleted_at;

	public function rules()
	{
		return [
			[['ActionID', 'PlanID', 'ActionTypeID', 'RateID', 'ContractID', 'PartyID', 'UnitID', 'ParentActionID', 'IsInProcessor', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
			[['StartDate', 'NextDate', 'CycleLength', 'StopDate', 'XWhen', 'Param'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'ActionID' => 'Action ID',
			'PlanID' => 'Plan ID',
			'ActionTypeID' => 'Action Type ID',
			'RateID' => 'Rate ID',
			'StartDate' => 'Start Date',
			'NextDate' => 'Next Date',
			'CycleLength' => 'Cycle Length',
			'StopDate' => 'Stop Date',
			'ContractID' => 'Contract ID',
			'PartyID' => 'Party ID',
			'UnitID' => 'Unit ID',
			'XWhen' => 'Xwhen',
			'Param' => 'Param',
			'ParentActionID' => 'Parent Action ID',
			'IsInProcessor' => 'Is In Processor',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
		];
	}

	public function search($params)
	{
		$query = Action::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'ActionID' => $this->ActionID,
            'PlanID' => $this->PlanID,
            'ActionTypeID' => $this->ActionTypeID,
            'RateID' => $this->RateID,
            'StartDate' => $this->StartDate,
            'NextDate' => $this->NextDate,
            'StopDate' => $this->StopDate,
            'ContractID' => $this->ContractID,
            'PartyID' => $this->PartyID,
            'UnitID' => $this->UnitID,
            'ParentActionID' => $this->ParentActionID,
            'IsInProcessor' => $this->IsInProcessor,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

		$query->andFilterWhere(['like', 'CycleLength', $this->CycleLength])
            ->andFilterWhere(['like', 'XWhen', $this->XWhen])
            ->andFilterWhere(['like', 'Param', $this->Param]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
