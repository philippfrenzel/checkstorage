<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MarketSource;

/**
 * MarketSourceSearch represents the model behind the search form about MarketSource.
 */
class MarketSourceSearch extends Model
{
	public $MarketSourceID;
	public $MarketSource;
	public $created_at;
	public $updated_at;
	public $deleted_at;

	public function rules()
	{
		return [
			[['MarketSourceID', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
			[['MarketSource'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'MarketSourceID' => 'Market Source ID',
			'MarketSource' => 'Market Source',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
		];
	}

	public function search($params)
	{
		$query = MarketSource::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'MarketSourceID' => $this->MarketSourceID,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

		$query->andFilterWhere(['like', 'MarketSource', $this->MarketSource]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
