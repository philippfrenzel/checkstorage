<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AddressType;

/**
 * AddressTypeSearch represents the model behind the search form about AddressType.
 */
class AddressTypeSearch extends Model
{
	public $AddressTypeID;
	public $AddressType;
	public $MailToPriority;
	public $UserDefined;
	public $created_at;
	public $updated_at;
	public $deleted_at;

	public function rules()
	{
		return [
			[['AddressTypeID', 'MailToPriority', 'UserDefined', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
			[['AddressType'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'AddressTypeID' => 'Address Type ID',
			'AddressType' => 'Address Type',
			'MailToPriority' => 'Mail To Priority',
			'UserDefined' => 'User Defined',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
		];
	}

	public function search($params)
	{
		$query = AddressType::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'AddressTypeID' => $this->AddressTypeID,
            'MailToPriority' => $this->MailToPriority,
            'UserDefined' => $this->UserDefined,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

		$query->andFilterWhere(['like', 'AddressType', $this->AddressType]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
