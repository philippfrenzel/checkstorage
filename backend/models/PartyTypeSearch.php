<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PartyType;

/**
 * PartyTypeSearch represents the model behind the search form about PartyType.
 */
class PartyTypeSearch extends Model
{
	public $PartyTypeID;
	public $PartyType;
	public $UserDefined;
	public $created_at;
	public $updated_at;
	public $deleted_at;

	public function rules()
	{
		return [
			[['PartyTypeID', 'UserDefined', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
			[['PartyType'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'PartyTypeID' => 'Party Type ID',
			'PartyType' => 'Party Type',
			'UserDefined' => 'User Defined',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
		];
	}

	public function search($params)
	{
		$query = PartyType::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'PartyTypeID' => $this->PartyTypeID,
            'UserDefined' => $this->UserDefined,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

		$query->andFilterWhere(['like', 'PartyType', $this->PartyType]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
