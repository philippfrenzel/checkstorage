<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MarketReason;

/**
 * MarketReasonSearch represents the model behind the search form about MarketReason.
 */
class MarketReasonSearch extends Model
{
	public $MarketReasonID;
	public $MarketReason;
	public $created_at;
	public $updated_at;
	public $deleted_at;

	public function rules()
	{
		return [
			[['MarketReasonID', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
			[['MarketReason'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'MarketReasonID' => 'Market Reason ID',
			'MarketReason' => 'Market Reason',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
		];
	}

	public function search($params)
	{
		$query = MarketReason::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'MarketReasonID' => $this->MarketReasonID,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

		$query->andFilterWhere(['like', 'MarketReason', $this->MarketReason]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
