<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ActionType;

/**
 * ActionTypeSearch represents the model behind the search form about ActionType.
 */
class ActionTypeSearch extends Model
{
	public $ActionTypeID;
	public $ActionType;
	public $ActionGroup;
	public $DebitCredit;
	public $AR;
	public $AP;
	public $OrderOfAction;
	public $NoteShort;
	public $Note;
	public $TechNote;
	public $Color;
	public $Icon;
	public $DefaultBaseDate;
	public $UnitRateBased;
	public $SaveAction;
	public $CanPreprocess;
	public $CanDeposit;
	public $Associate;
	public $ParamTemplate;
	public $InventoryFactor;
	public $RefParam;
	public $UnitStatus;
	public $HasContractID;
	public $Per;
	public $Invoice;
	public $created_at;
	public $updated_at;
	public $deleted_at;

	public function rules()
	{
		return [
			[['ActionTypeID', 'OrderOfAction', 'Color', 'UnitRateBased', 'SaveAction', 'CanPreprocess', 'CanDeposit', 'InventoryFactor', 'HasContractID', 'Invoice', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
			[['ActionType', 'ActionGroup', 'DebitCredit', 'AR', 'AP', 'NoteShort', 'Note', 'TechNote', 'Icon', 'DefaultBaseDate', 'Associate', 'ParamTemplate', 'RefParam', 'UnitStatus', 'Per'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'ActionTypeID' => 'Action Type ID',
			'ActionType' => 'Action Type',
			'ActionGroup' => 'Action Group',
			'DebitCredit' => 'Debit Credit',
			'AR' => 'Ar',
			'AP' => 'Ap',
			'OrderOfAction' => 'Order Of Action',
			'NoteShort' => 'Note Short',
			'Note' => 'Note',
			'TechNote' => 'Tech Note',
			'Color' => 'Color',
			'Icon' => 'Icon',
			'DefaultBaseDate' => 'Default Base Date',
			'UnitRateBased' => 'Unit Rate Based',
			'SaveAction' => 'Save Action',
			'CanPreprocess' => 'Can Preprocess',
			'CanDeposit' => 'Can Deposit',
			'Associate' => 'Associate',
			'ParamTemplate' => 'Param Template',
			'InventoryFactor' => 'Inventory Factor',
			'RefParam' => 'Ref Param',
			'UnitStatus' => 'Unit Status',
			'HasContractID' => 'Has Contract ID',
			'Per' => 'Per',
			'Invoice' => 'Invoice',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
		];
	}

	public function search($params)
	{
		$query = ActionType::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'ActionTypeID' => $this->ActionTypeID,
            'OrderOfAction' => $this->OrderOfAction,
            'Color' => $this->Color,
            'UnitRateBased' => $this->UnitRateBased,
            'SaveAction' => $this->SaveAction,
            'CanPreprocess' => $this->CanPreprocess,
            'CanDeposit' => $this->CanDeposit,
            'InventoryFactor' => $this->InventoryFactor,
            'HasContractID' => $this->HasContractID,
            'Invoice' => $this->Invoice,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

		$query->andFilterWhere(['like', 'ActionType', $this->ActionType])
            ->andFilterWhere(['like', 'ActionGroup', $this->ActionGroup])
            ->andFilterWhere(['like', 'DebitCredit', $this->DebitCredit])
            ->andFilterWhere(['like', 'AR', $this->AR])
            ->andFilterWhere(['like', 'AP', $this->AP])
            ->andFilterWhere(['like', 'NoteShort', $this->NoteShort])
            ->andFilterWhere(['like', 'Note', $this->Note])
            ->andFilterWhere(['like', 'TechNote', $this->TechNote])
            ->andFilterWhere(['like', 'Icon', $this->Icon])
            ->andFilterWhere(['like', 'DefaultBaseDate', $this->DefaultBaseDate])
            ->andFilterWhere(['like', 'Associate', $this->Associate])
            ->andFilterWhere(['like', 'ParamTemplate', $this->ParamTemplate])
            ->andFilterWhere(['like', 'RefParam', $this->RefParam])
            ->andFilterWhere(['like', 'UnitStatus', $this->UnitStatus])
            ->andFilterWhere(['like', 'Per', $this->Per]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
