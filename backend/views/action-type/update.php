<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\ActionType $model
 */

$this->title = 'Action Type Update ' . $model->ActionTypeID . '';
$this->params['breadcrumbs'][] = ['label' => 'Action Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->ActionTypeID, 'url' => ['view', 'ActionTypeID' => $model->ActionTypeID]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="action-type-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'ActionTypeID' => $model->ActionTypeID], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
