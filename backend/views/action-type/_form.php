<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\ActionType $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="action-type-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            
			<?= $form->field($model, 'OrderOfAction')->textInput() ?>
			<?= $form->field($model, 'Color')->textInput() ?>
			<?= $form->field($model, 'UnitRateBased')->textInput() ?>
			<?= $form->field($model, 'SaveAction')->textInput() ?>
			<?= $form->field($model, 'CanPreprocess')->textInput() ?>
			<?= $form->field($model, 'CanDeposit')->textInput() ?>
			<?= $form->field($model, 'InventoryFactor')->textInput() ?>
			<?= $form->field($model, 'HasContractID')->textInput() ?>
			<?= $form->field($model, 'Invoice')->textInput() ?>
			<?= $form->field($model, 'created_at')->textInput() ?>
			<?= $form->field($model, 'updated_at')->textInput() ?>
			<?= $form->field($model, 'deleted_at')->textInput() ?>
			<?= $form->field($model, 'ActionType')->textInput(['maxlength' => 50]) ?>
			<?= $form->field($model, 'ActionGroup')->textInput(['maxlength' => 50]) ?>
			<?= $form->field($model, 'NoteShort')->textInput(['maxlength' => 50]) ?>
			<?= $form->field($model, 'Icon')->textInput(['maxlength' => 50]) ?>
			<?= $form->field($model, 'DefaultBaseDate')->textInput(['maxlength' => 50]) ?>
			<?= $form->field($model, 'Associate')->textInput(['maxlength' => 50]) ?>
			<?= $form->field($model, 'RefParam')->textInput(['maxlength' => 50]) ?>
			<?= $form->field($model, 'Per')->textInput(['maxlength' => 50]) ?>
			<?= $form->field($model, 'DebitCredit')->textInput(['maxlength' => 2]) ?>
			<?= $form->field($model, 'AR')->textInput(['maxlength' => 2]) ?>
			<?= $form->field($model, 'AP')->textInput(['maxlength' => 2]) ?>
			<?= $form->field($model, 'UnitStatus')->textInput(['maxlength' => 2]) ?>
			<?= $form->field($model, 'Note')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'TechNote')->textInput(['maxlength' => 255]) ?>
			<?= $form->field($model, 'ParamTemplate')->textInput(['maxlength' => 255]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    \yii\bootstrap\Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ [
    'label'   => 'ActionType',
    'content' => $this->blocks['main'],
    'active'  => true,
], ]
                 ]
    );
    ?>
        <hr/>

        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? 'Create' : 'Save'), ['class' => $model->isNewRecord ?
        'btn btn-primary' : 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
