<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var backend\models\ActionTypeSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="action-type-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'ActionTypeID') ?>

		<?= $form->field($model, 'ActionType') ?>

		<?= $form->field($model, 'ActionGroup') ?>

		<?= $form->field($model, 'DebitCredit') ?>

		<?= $form->field($model, 'AR') ?>

		<?php // echo $form->field($model, 'AP') ?>

		<?php // echo $form->field($model, 'OrderOfAction') ?>

		<?php // echo $form->field($model, 'NoteShort') ?>

		<?php // echo $form->field($model, 'Note') ?>

		<?php // echo $form->field($model, 'TechNote') ?>

		<?php // echo $form->field($model, 'Color') ?>

		<?php // echo $form->field($model, 'Icon') ?>

		<?php // echo $form->field($model, 'DefaultBaseDate') ?>

		<?php // echo $form->field($model, 'UnitRateBased') ?>

		<?php // echo $form->field($model, 'SaveAction') ?>

		<?php // echo $form->field($model, 'CanPreprocess') ?>

		<?php // echo $form->field($model, 'CanDeposit') ?>

		<?php // echo $form->field($model, 'Associate') ?>

		<?php // echo $form->field($model, 'ParamTemplate') ?>

		<?php // echo $form->field($model, 'InventoryFactor') ?>

		<?php // echo $form->field($model, 'RefParam') ?>

		<?php // echo $form->field($model, 'UnitStatus') ?>

		<?php // echo $form->field($model, 'HasContractID') ?>

		<?php // echo $form->field($model, 'Per') ?>

		<?php // echo $form->field($model, 'Invoice') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

		<?php // echo $form->field($model, 'deleted_at') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
