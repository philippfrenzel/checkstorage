<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var common\models\ActionType $model
*/

$this->title = 'Action Type View ' . $model->ActionTypeID . '';
$this->params['breadcrumbs'][] = ['label' => 'Action Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->ActionTypeID, 'url' => ['view', 'ActionTypeID' => $model->ActionTypeID]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="action-type-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'ActionTypeID' => $model->ActionTypeID],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Action Type', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->ActionTypeID ?>    </h3>


    <?php $this->beginBlock('common\models\ActionType'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    			'ActionTypeID',
			'ActionType',
			'ActionGroup',
			'DebitCredit',
			'AR',
			'AP',
			'OrderOfAction',
			'NoteShort',
			'Note',
			'TechNote',
			'Color',
			'Icon',
			'DefaultBaseDate',
			'UnitRateBased',
			'SaveAction',
			'CanPreprocess',
			'CanDeposit',
			'Associate',
			'ParamTemplate',
			'InventoryFactor',
			'RefParam',
			'UnitStatus',
			'HasContractID',
			'Per',
			'Invoice',
			'created_at',
			'updated_at',
			'deleted_at',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'ActionTypeID' => $model->ActionTypeID],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
<?php $this->beginBlock('Actions'); ?>
<p class='pull-right'>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-list"></span> List All Actions',
            ['action/index'],
            ['class'=>'btn text-muted btn-xs']
        ) ?>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-plus"></span> New Action',
            ['action/create', 'Action'=>['ActionTypeID'=>$model->ActionTypeID]],
            ['class'=>'btn btn-success btn-xs']
        ) ?>
</p><div class='clearfix'></div>
<?php $this->endBlock() ?>


    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> ActionType',
    'content' => $this->blocks['common\models\ActionType'],
    'active'  => true,
],[
    'label'   => '<small><span class="glyphicon glyphicon-paperclip"></span> Actions</small>',
    'content' => $this->blocks['Actions'],
    'active'  => false,
], ]
                 ]
    );
    ?></div>
