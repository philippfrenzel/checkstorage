<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\MarketReason $model
 */

$this->title = 'Market Reason Update ' . $model->MarketReasonID . '';
$this->params['breadcrumbs'][] = ['label' => 'Market Reasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->MarketReasonID, 'url' => ['view', 'MarketReasonID' => $model->MarketReasonID]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="market-reason-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'MarketReasonID' => $model->MarketReasonID], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
