<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var backend\models\EventSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="event-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'EventID') ?>

		<?= $form->field($model, 'ActionID') ?>

		<?= $form->field($model, 'Param') ?>

		<?= $form->field($model, 'UserPartyID') ?>

		<?= $form->field($model, 'Created') ?>

		<?php // echo $form->field($model, 'JournalDate') ?>

		<?php // echo $form->field($model, 'EAmount') ?>

		<?php // echo $form->field($model, 'ETax') ?>

		<?php // echo $form->field($model, 'ESnapshot') ?>

		<?php // echo $form->field($model, 'CID') ?>

		<?php // echo $form->field($model, 'datFrom') ?>

		<?php // echo $form->field($model, 'datTo') ?>

		<?php // echo $form->field($model, 'Description') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

		<?php // echo $form->field($model, 'deleted_at') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
