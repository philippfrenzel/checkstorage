<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var common\models\MarketSource $model
*/

$this->title = 'Market Source View ' . $model->MarketSourceID . '';
$this->params['breadcrumbs'][] = ['label' => 'Market Sources', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->MarketSourceID, 'url' => ['view', 'MarketSourceID' => $model->MarketSourceID]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="market-source-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'MarketSourceID' => $model->MarketSourceID],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Market Source', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->MarketSourceID ?>    </h3>


    <?php $this->beginBlock('common\models\MarketSource'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    			'MarketSourceID',
			'MarketSource',
			'created_at',
			'updated_at',
			'deleted_at',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'MarketSourceID' => $model->MarketSourceID],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
<?php $this->beginBlock('Contracts'); ?>
<p class='pull-right'>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-list"></span> List All Contracts',
            ['contract/index'],
            ['class'=>'btn text-muted btn-xs']
        ) ?>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-plus"></span> New Contract',
            ['contract/create', 'Contract'=>['MarketSourceID'=>$model->MarketSourceID]],
            ['class'=>'btn btn-success btn-xs']
        ) ?>
</p><div class='clearfix'></div>
<?php $this->endBlock() ?>


    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> MarketSource',
    'content' => $this->blocks['common\models\MarketSource'],
    'active'  => true,
],[
    'label'   => '<small><span class="glyphicon glyphicon-paperclip"></span> Contracts</small>',
    'content' => $this->blocks['Contracts'],
    'active'  => false,
], ]
                 ]
    );
    ?></div>
