<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\MarketSource $model
 */

$this->title = 'Market Source Update ' . $model->MarketSourceID . '';
$this->params['breadcrumbs'][] = ['label' => 'Market Sources', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->MarketSourceID, 'url' => ['view', 'MarketSourceID' => $model->MarketSourceID]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="market-source-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'MarketSourceID' => $model->MarketSourceID], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
