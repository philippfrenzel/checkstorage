<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\ContractType $model
 */

$this->title = 'Contract Type Update ' . $model->ContractTypeID . '';
$this->params['breadcrumbs'][] = ['label' => 'Contract Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->ContractTypeID, 'url' => ['view', 'ContractTypeID' => $model->ContractTypeID]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="contract-type-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'ContractTypeID' => $model->ContractTypeID], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
