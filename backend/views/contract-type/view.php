<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var common\models\ContractType $model
*/

$this->title = 'Contract Type View ' . $model->ContractTypeID . '';
$this->params['breadcrumbs'][] = ['label' => 'Contract Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->ContractTypeID, 'url' => ['view', 'ContractTypeID' => $model->ContractTypeID]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="contract-type-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'ContractTypeID' => $model->ContractTypeID],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Contract Type', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->ContractTypeID ?>    </h3>


    <?php $this->beginBlock('common\models\ContractType'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    			'ContractTypeID',
			'ContractType',
			'Icon',
			'Notes:ntext',
			'created_at',
			'updated_at',
			'deleted_at',
			'MasterRoleTypeID',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'ContractTypeID' => $model->ContractTypeID],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
<?php $this->beginBlock('Contracts'); ?>
<p class='pull-right'>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-list"></span> List All Contracts',
            ['contract/index'],
            ['class'=>'btn text-muted btn-xs']
        ) ?>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-plus"></span> New Contract',
            ['contract/create', 'Contract'=>['ContractTypeID'=>$model->ContractTypeID]],
            ['class'=>'btn btn-success btn-xs']
        ) ?>
</p><div class='clearfix'></div>
<?php $this->endBlock() ?>


    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> ContractType',
    'content' => $this->blocks['common\models\ContractType'],
    'active'  => true,
],[
    'label'   => '<small><span class="glyphicon glyphicon-paperclip"></span> Contracts</small>',
    'content' => $this->blocks['Contracts'],
    'active'  => false,
], ]
                 ]
    );
    ?></div>
