<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\Action $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="action-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            
			<?= $form->field($model, 'PlanID')->textInput() ?>
			<?= $form->field($model, 'ActionTypeID')->textInput() ?>
			<?= $form->field($model, 'RateID')->textInput() ?>
			<?= $form->field($model, 'ContractID')->textInput() ?>
			<?= $form->field($model, 'PartyID')->textInput() ?>
			<?= $form->field($model, 'UnitID')->textInput() ?>
			<?= $form->field($model, 'ParentActionID')->textInput() ?>
			<?= $form->field($model, 'IsInProcessor')->textInput() ?>
			<?= $form->field($model, 'created_at')->textInput() ?>
			<?= $form->field($model, 'updated_at')->textInput() ?>
			<?= $form->field($model, 'deleted_at')->textInput() ?>
			<?= $form->field($model, 'StartDate')->textInput() ?>
			<?= $form->field($model, 'NextDate')->textInput() ?>
			<?= $form->field($model, 'StopDate')->textInput() ?>
			<?= $form->field($model, 'CycleLength')->textInput(['maxlength' => 50]) ?>
			<?= $form->field($model, 'XWhen')->textInput(['maxlength' => 50]) ?>
			<?= $form->field($model, 'Param')->textInput(['maxlength' => 1000]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    \yii\bootstrap\Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ [
    'label'   => 'Action',
    'content' => $this->blocks['main'],
    'active'  => true,
], ]
                 ]
    );
    ?>
        <hr/>

        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? 'Create' : 'Save'), ['class' => $model->isNewRecord ?
        'btn btn-primary' : 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
