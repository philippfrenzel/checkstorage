<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var backend\models\ActionSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="action-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'ActionID') ?>

		<?= $form->field($model, 'PlanID') ?>

		<?= $form->field($model, 'ActionTypeID') ?>

		<?= $form->field($model, 'RateID') ?>

		<?= $form->field($model, 'StartDate') ?>

		<?php // echo $form->field($model, 'NextDate') ?>

		<?php // echo $form->field($model, 'CycleLength') ?>

		<?php // echo $form->field($model, 'StopDate') ?>

		<?php // echo $form->field($model, 'ContractID') ?>

		<?php // echo $form->field($model, 'PartyID') ?>

		<?php // echo $form->field($model, 'UnitID') ?>

		<?php // echo $form->field($model, 'XWhen') ?>

		<?php // echo $form->field($model, 'Param') ?>

		<?php // echo $form->field($model, 'ParentActionID') ?>

		<?php // echo $form->field($model, 'IsInProcessor') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

		<?php // echo $form->field($model, 'deleted_at') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
