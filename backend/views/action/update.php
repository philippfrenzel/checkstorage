<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Action $model
 */

$this->title = 'Action Update ' . $model->ActionID . '';
$this->params['breadcrumbs'][] = ['label' => 'Actions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->ActionID, 'url' => ['view', 'ActionID' => $model->ActionID]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="action-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'ActionID' => $model->ActionID], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
