<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var common\models\Action $model
*/

$this->title = 'Action View ' . $model->ActionID . '';
$this->params['breadcrumbs'][] = ['label' => 'Actions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->ActionID, 'url' => ['view', 'ActionID' => $model->ActionID]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="action-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'ActionID' => $model->ActionID],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Action', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->ActionID ?>    </h3>


    <?php $this->beginBlock('common\models\Action'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    			'ActionID',
			'PlanID',
			'ActionTypeID',
			'RateID',
			'StartDate',
			'NextDate',
			'CycleLength',
			'StopDate',
			'ContractID',
			'PartyID',
			'UnitID',
			'XWhen',
			'Param',
			'ParentActionID',
			'IsInProcessor',
			'created_at',
			'updated_at',
			'deleted_at',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'ActionID' => $model->ActionID],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
<?php $this->beginBlock('Events'); ?>
<p class='pull-right'>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-list"></span> List All Events',
            ['event/index'],
            ['class'=>'btn text-muted btn-xs']
        ) ?>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-plus"></span> New Event',
            ['event/create', 'Event'=>['ActionID'=>$model->ActionID]],
            ['class'=>'btn btn-success btn-xs']
        ) ?>
</p><div class='clearfix'></div>
<?php $this->endBlock() ?>


    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> Action',
    'content' => $this->blocks['common\models\Action'],
    'active'  => true,
],[
    'label'   => '<small><span class="glyphicon glyphicon-paperclip"></span> Events</small>',
    'content' => $this->blocks['Events'],
    'active'  => false,
], ]
                 ]
    );
    ?></div>
