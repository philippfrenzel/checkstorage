<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\PartyType $model
 */

$this->title = 'Party Type Update ' . $model->PartyTypeID . '';
$this->params['breadcrumbs'][] = ['label' => 'Party Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->PartyTypeID, 'url' => ['view', 'PartyTypeID' => $model->PartyTypeID]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="party-type-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'PartyTypeID' => $model->PartyTypeID], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
