<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var common\models\PartyType $model
*/

$this->title = 'Party Type View ' . $model->PartyTypeID . '';
$this->params['breadcrumbs'][] = ['label' => 'Party Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->PartyTypeID, 'url' => ['view', 'PartyTypeID' => $model->PartyTypeID]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="party-type-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'PartyTypeID' => $model->PartyTypeID],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Party Type', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->PartyTypeID ?>    </h3>


    <?php $this->beginBlock('common\models\PartyType'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    			'PartyTypeID',
			'PartyType:ntext',
			'UserDefined',
			'created_at',
			'updated_at',
			'deleted_at',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'PartyTypeID' => $model->PartyTypeID],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
<?php $this->beginBlock('Parties'); ?>
<p class='pull-right'>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-list"></span> List All Parties',
            ['party/index'],
            ['class'=>'btn text-muted btn-xs']
        ) ?>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-plus"></span> New Party',
            ['party/create', 'Party'=>['PartyTypeID'=>$model->PartyTypeID]],
            ['class'=>'btn btn-success btn-xs']
        ) ?>
</p><div class='clearfix'></div>
<?php $this->endBlock() ?>


    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> PartyType',
    'content' => $this->blocks['common\models\PartyType'],
    'active'  => true,
],[
    'label'   => '<small><span class="glyphicon glyphicon-paperclip"></span> Parties</small>',
    'content' => $this->blocks['Parties'],
    'active'  => false,
], ]
                 ]
    );
    ?></div>
